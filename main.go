package main

import (
	"cms/controller"
	_ "cms/docs"
	"cms/router"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	httpSwagger "github.com/swaggo/http-swagger"
)

// @title			CMS
// @version		1.0
// @description	CMS API文件
// @license.url	http://www.apache.org/licenses/LICENSE-2.0.html
// @host			localhost:30030
func main() {
	// Initialize database connection
	if err := controller.Init(); err != nil {
		log.Fatal("Error initializing database: ", err)
	}

	// redis init
	err := controller.InitClient()
	if err != nil {
		fmt.Println(err)
		return
	}

	// Initialize router
	r := mux.NewRouter()

	router.Init(r)

	// swagger
	r.PathPrefix("/").Handler(httpSwagger.WrapHandler)

	// log紀錄
	r.Use(controller.LoggMakerMiddleware)
	r.Use(controller.SetResHeaderJson)

	// Server port :30050
	fmt.Println("======================================================")
	fmt.Printf(" Server/swagger doc started at http://localhost%s\n", ":30050")
	fmt.Printf(" socket-io started at http://localhost%s\n", ":30050")
	fmt.Println("測試人員帳號密碼皆為 dev_account")
	fmt.Println("======================================================")

	// Socket server
	go controller.SocketServer()

	log.Fatal(http.ListenAndServe(":30050", r))

}
