package controller

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

// GetTestdata 測試驗證資料
// GetApp godoc
// @Summary 用header帶入登入時拿到的token 並拿到一筆測試資料
// @Description "用token 拿到資料"
// @Tags GetTestdata
// @Param Authorization header string true "Insert your 登入 token" default(<Add access token here>)
// @Param Content-Type header string true "default Content-Type" default(application/json)
// @Produce json
// @Success 200 {object} controller.GetTestdataSuccessResExample "Request finish!"
// @Failure	201 {object} controller.GetTestdataFailResExample "Other error! Please Check Response.msg!"
// @Router /get_test_data [post]
func GetManagerTestdata(w http.ResponseWriter, r *http.Request) {

	// response json init
	successApiResponse := ApiResInit{}
	failApiResponse := ApiFailResInit{}

	// req.Method 判斷
	if r.Method == http.MethodGet {

		failApiResponse.Ret = 201
		failApiResponse.Msg = "request type error!"
		failApiResponse.Data = nil

		responseJSON, _ := json.Marshal(failApiResponse)

		// 寫入回應內容
		_, err := w.Write(responseJSON)
		if err != nil {
			return
		}

	} else {

		data := map[string]string{
			"date": "20230116",
			"id":   "10"}

		dataJSON, _ := json.Marshal(data)

		// 產生response
		successApiResponse.Ret = 200
		successApiResponse.Data = json.RawMessage(dataJSON)
		successApiResponse.Msg = ""

		responseJSON, _ := json.Marshal(successApiResponse)

		// 設置回應狀態碼和回應內容類型
		w.WriteHeader(http.StatusOK)

		// 寫入回應內容
		_, err := w.Write(responseJSON)
		if err != nil {
			return
		}
	}
}

// PerfectLogin
// GetApp godoc
// @Summary 登入驗證密碼並獲取token
// @Description "登入成功會給予token"
// @Tags PerfectLogin
// @Content-Type application/json
// @Produce json
// @Param request body controller.PerfectLoginRequestExample true "JSON檔案內容"
// @Success 200 {object} controller.PerfectLoginSuccess "Request finish!"
// @Failure	201 {object} controller.PerfectLoginFail "Other error! Please Check Response.msg!"
// @Router /PerfectLogin [post]
func ManagerPerfectLogin(w http.ResponseWriter, r *http.Request) {

	var loginReq LoginReq

	err := json.NewDecoder(r.Body).Decode(&loginReq)

	// response json init
	successApiResponse := ApiResInit{}
	failApiResponse := ApiFailResInit{}

	if r.Method != http.MethodPost || err != nil {

		failApiResponse.Ret = 201
		failApiResponse.Msg = "request type error!"
		failApiResponse.Data = nil

		responseJSON, _ := json.Marshal(failApiResponse)

		// 寫入回應內容
		_, err := w.Write(responseJSON)
		if err != nil {
			return
		}

	} else {

		account := loginReq.Account
		pwd := loginReq.Pwd

		// 撈出驗證密碼
		id, accountPwd, _ := GetPwdByManagerAccount(account)
		// hash後結果
		pwdHash := passwordhash(account, pwd)
		permissionId, _ := GetPermissionByManagerAccount(account)
		banPermissionId, _ := GetBanPermissionByManagerAccount(account)
		log.Println(permissionId)
		// req密碼與db內比對 若是正確 生成新 token + 有效時間
		if pwdHash == accountPwd {

			// create JWT token 有效期：一天
			token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
				"id":              id,
				"permissionId":    permissionId,
				"banPermissionId": banPermissionId,
				"exp":             time.Now().Add(time.Second * 1800).Unix(),
			})

			// 用JwtKey yaml 內 JwtKey 簽名
			JwtKey := "CMSeyJhbGciOi"
			ByteKey := []byte(JwtKey)
			tokenString, err := token.SignedString(ByteKey)

			//撈出資料庫中帳號資料
			manager, _ := GetManagerAccountInfo(account)

			//寫入redis
			err = WriteTokenInRedis(id, tokenString)
			fmt.Print("Insert to redis! \n")

			if err != nil {

				errMsg := fmt.Sprintf("Redis insert Fail! %s \n", err)

				failApiResponse.Ret = 201
				failApiResponse.Msg = errMsg
				failApiResponse.Data = nil

				responseJSON, _ := json.Marshal(failApiResponse)

				// 寫入回應內容
				_, err := w.Write(responseJSON)
				if err != nil {
					return
				}
			}

			// token 寫入 response json
			data := map[string]string{
				"id":        strconv.Itoa(int(manager.ID)),
				"nick_name": manager.NickName,
				"language":  manager.Language,
				"account":   account,
				"token":     tokenString}

			dataJSON, _ := json.Marshal(data)

			// 產生response
			successApiResponse.Ret = 200
			successApiResponse.Data = json.RawMessage(dataJSON)
			successApiResponse.Msg = ""

			responseJSON, _ := json.Marshal(successApiResponse)

			// 更新進入db
			id := strconv.Itoa(int(manager.ID))
			err = UpdateManagerAuthToken(id, tokenString)

			// 回傳response

			_, err = w.Write(responseJSON)
			if err != nil {
				return
			}

		} else {

			// 密碼錯誤
			failApiResponse.Ret = 201
			failApiResponse.Msg = "Invalid account or password!"
			failApiResponse.Data = nil

			responseJSON, _ := json.Marshal(failApiResponse)

			// 寫入回應內容
			_, err := w.Write(responseJSON)
			if err != nil {
				return
			}
		}
	}

}
