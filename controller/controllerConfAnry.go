package controller

import (
	"github.com/jinzhu/gorm"
)

const (
	//for payment
	goldMerId = "MC0123456789"
	goldKey   = "369d4621fddf90c2ee2b4f49"
	easyAppId = "987654321"
	easyKey   = "EF5D849DC9DE22D8AB254536A3B366B0"
)

type theuser struct {
	Id             int     `json:"id"`
	Account        string  `json:"account" example:"dsdd2"`
	NickName       string  `json:"nickName" gorm:"column:nickName" example:"6dda"`
	Email          string  `json:"email" example:"6dfwe@gmail.com"`
	Pwd            string  `json:"pwd" example:"adj2dji2"`
	Phone          string  `json:"phone" example:"0902837208"`
	CreateDatetime string  `json:"createDatetime" gorm:"column:createDatetime"`
	Icon           *string `json:"icon" example:"https://fjhiod.jpg"`
	UserStory      *string `json:"userStory" gorm:"column:userStory"`
	Coin           int     `json:"coin" example:"0"`
	VipType        int     `json:"vipType" gorm:"column:vipType" example:"0"`
	AuthStatus     int     `json:"authStatus" gorm:"column:authStatus" example:"2"`
	LoginDatetime  *string `json:"loginDatetime" gorm:"column:loginDatetime"`
	LoginDayscount int     `json:"loginDayscount" gorm:"column:loginDayscount" example:"0"`
	LoginCountry   int     `json:"loginCountry" gorm:"column:loginCountry"`
	Birthday       *string `json:"birthday"`
	Country        int     `json:"country"`
	Sex            int     `json:"sex" example:"0"`
	Language       int     `json:"language"`
	RoleId         int     `json:"roleId" gorm:"column:roleId"`
	AuthToken      *string `json:"authToken" gorm:"column:authToken"`
}

type theuserError struct {
	Id             int     `json:"id"`
	Account        string  `json:"account" example:"ERROR"`
	NickName       string  `json:"nickName" gorm:"column:nickName" example:"accountERROR"`
	Email          string  `json:"email" example:"emailERROR"`
	Pwd            string  `json:"pwd" example:"ERROR"`
	Phone          string  `json:"phone" example:"phoneERROR"`
	CreateDatetime string  `json:"createDatetime" gorm:"column:createDatetime"`
	Icon           *string `json:"icon" example:"iconERROR"`
	UserStory      *string `json:"userStory" gorm:"column:userStory"`
	Coin           int     `json:"coin"`
	VipType        int     `json:"vipType" gorm:"column:vipType"`
	AuthStatus     int     `json:"authStatus" gorm:"column:authStatus"`
	LoginDatetime  *string `json:"loginDatetime" gorm:"column:loginDatetime" example:"ERROR"`
	LoginDayscount int     `json:"loginDayscount" gorm:"column:loginDayscount" example:"0"`
	LoginCountry   int     `json:"loginCountry" gorm:"column:loginCountry"`
	Birthday       *string `json:"birthday"`
	Country        int     `json:"country"`
	Sex            int     `json:"sex"`
	Language       int     `json:"language"`
	RoleId         int     `json:"roleId" gorm:"column:roleId"`
	AuthToken      *string `json:"authToken" gorm:"column:authToken,omitempty"`
}
type themanager struct {
	Id             int     `json:"id"`
	Account        string  `json:"account" example:"j6fdt"`
	NickName       string  `json:"nickName" gorm:"column:nickName" example:"huig8g"`
	Email          string  `json:"email" example:"gjih@gmail.com"`
	Pwd            string  `json:"pwd" example:"98fghf59"`
	Phone          string  `json:"phone" example:"0907568467"`
	CreateDatetime string  `json:"createDatetime" gorm:"column:createDatetime"`
	AuthStatus     int     `json:"authStatus" gorm:"column:authStatus" example:"2"`
	LoginDatetime  *string `json:"loginDatetime" gorm:"column:loginDatetime"`
	LoginCountry   int     `json:"loginCountry" gorm:"column:loginCountry"`
	Country        int     `json:"country"`
	Language       int     `json:"language"`
	AuthToken      *string `json:"authToken" gorm:"column:authToken"`
}

type themanagerError struct {
	Id             int     `json:"id"`
	Account        string  `json:"account" example:"ERROR"`
	NickName       string  `json:"nickName" gorm:"column:nickName" example:"accountERROR"`
	Email          string  `json:"email" example:"emailERROR"`
	Pwd            string  `json:"pwd" example:"ERROR"`
	Phone          string  `json:"phone" example:"phoneERROR"`
	CreateDatetime string  `json:"createDatetime" gorm:"column:createDatetime"`
	AuthStatus     int     `json:"authStatus" gorm:"column:authStatus"`
	LoginDatetime  *string `json:"loginDatetime" gorm:"column:loginDatetime" example:"ERROR"`
	LoginCountry   int     `json:"loginCountry" gorm:"column:loginCountry"`
	Country        int     `json:"country"`
	Sex            int     `json:"sex"`
	Language       int     `json:"language"`
	AuthToken      *string `json:"authToken" gorm:"column:authToken"`
}
type role struct {
	Id       int     `json:"id"`
	RoleName string  `json:"roleName" gorm:"column:roleName" example:"therole"`
	Note     *string `json:"note"`
	Enable   int     `json:"enable" example:"1"`
}
type roleError struct {
	Id       int     `json:"id"`
	RoleName string  `json:"roleName" gorm:"column:roleName" example:"roleERROR"`
	Note     *string `json:"note"`
	Enable   int     `json:"enable" example:"1"`
}
type roleManager struct {
	Id       int    `json:"id"`
	RoleName string `json:"roleName" gorm:"column:roleName"`
	Account  string `json:"account"`
	Enable   int    `json:"enable"`
}
type roleManagerId struct {
	Id        int `json:"id"`
	RoleId    int `json:"roleId" gorm:"column:roleId"`
	ManagerId int `json:"managerId" gorm:"column:managerId"`
	Enable    int `json:"enable"`
}

type permission struct {
	Id           int    `json:"id" example:"11"`
	Permission   string `json:"permission" example:"goPermission"`
	Level        int    `json:"level" example:"2"`
	Sort         int    `json:"sort" example:"2"`
	ParentId     int    `json:"parentId" gorm:"column:parentId" example:"4"`
	LinkFunction string `json:"linkFunction" gorm:"column:linkFunction" example:"GoPermission"`
	Enable       int    `json:"enable" example:"1"`
}

type permissionError struct {
	Id           int    `json:"id"`
	Permission   string `json:"permission" example:"permissionERROR"`
	Level        int    `json:"level"`
	Sort         int    `json:"sort"`
	ParentId     int    `json:"parentId" gorm:"column:parentId"`
	LinkFunction string `json:"linkFunction" gorm:"column:linkFunction"`
	Enable       int    `json:"enable" example:"1"`
}

type rolePermission struct {
	Id         int     `json:"id"`
	RoleName   string  `json:"roleName" gorm:"column:roleName" example:"therole"`
	Note       *string `json:"note"`
	Permission string  `json:"permission" example:"goPermission"`
	Enable     int     `json:"enable"`
}
type rolePermissionId struct {
	Id           int `json:"id"`
	RoleId       int `json:"roleId" gorm:"column:roleId"`
	PermissionId int `json:"permissionId" gorm:"column:permissionId"`
	Enable       int `json:"enable" example:"1"`
}
type banManagersPermission struct {
	Id           int `json:"id"`
	PermissionId int `json:"permissionId" gorm:"column:permissionId"`
	ManagerId    int `json:"managerId" gorm:"column:managerId"`
	Enable       int `json:"enable" example:"1"`
}
type permissionBanList struct {
	Id         int    `json:"id"`
	Permission string `json:"permission"`
	Account    string `json:"account"`
	Enable     int    `json:"enable" example:"1"`
}

type userq struct {
	Id             int     `json:"id"`
	Account        string  `json:"account"`
	NickName       string  `json:"nickName" gorm:"column:nickName"`
	Email          string  `json:"email"`
	Phone          string  `json:"phone"`
	CreateDatetime string  `json:"createdDatetime" gorm:"column:createdDatetime"`
	Icon           *string `json:"icon"`
	UserStory      *string `json:"userStory" gorm:"column:userStory"`
	Coin           int     `json:"coin"`
	VipType        int     `json:"vipType" gorm:"column:vipType"`
	AuthStatus     int     `json:"authStatus" gorm:"column:authStatus"`
	LoginDatetime  *string `json:"loginDatetime" gorm:"column:loginDatetime"`
	LoginDayscount int     `json:"loginDayscount" gorm:"column:loginDayscount"`
	LoginCountry   int     `json:"loginCountry" gorm:"column:loginCountry"`
	Birthday       *string `json:"birthday"`
	Country        int     `json:"country"`
	Sex            int     `json:"sex"`
	Language       int     `json:"language"`
	RoleId         int     `json:"roleId"`
}

type manaq struct {
	Id             int     `json:"id"`
	Account        string  `json:"account"`
	NickName       string  `json:"nickName" gorm:"column:nickName"`
	Email          string  `json:"email"`
	Phone          string  `json:"phone"`
	CreateDatetime string  `json:"createDatetime" gorm:"column:createDatetime"`
	AuthStatus     int     `json:"authStatus" gorm:"column:authStatus"`
	LoginDatetime  *string `json:"loginDatetime" gorm:"column:loginDatetime"`
	LoginCountry   int     `json:"loginCountry" gorm:"column:loginCountry"`
	Country        int     `json:"country"`
	Language       int     `json:"language"`
}

type ApiResponseCorrect struct {
	ResultCode    int         `json:"ret"`
	ResultMessage interface{} `json:"data"`
	Message       string      `json:"msg"`
}
type DataInfo struct {
	Code int         `json:"code,omitempty"`
	Msg  string      `json:"msg,omitempty"`
	Info interface{} `json:"info,omitempty"`
}

type ManagerInsertRedis struct {
	Id    string `json:"id"`
	Token string `json:"token"`
}

// Users Users db.users 欄位
type Managers struct {
	gorm.Model `json:"id"`
	Account    string `json:"account" gorm:"column:account"`
	Pwd        string `json:"pwd" gorm:"column:pwd"`
	AuthToken  string `json:"authToken" gorm:"column:authToken"`
	NickName   string `json:"nickName" gorm:"column:nickName"`
	Language   string `json:"language" gorm:"column:language"`
}

type Permissions struct {
	Id int `json:"id" gorm:"column:id"`
}

type PayChannel struct {
	Id          int    `json:"id" example:"1"`
	ChannelName string `json:"channelName" gorm:"column:channelName" example:"golden"`
	PayCode     string `json:"payCode" gorm:"column:payCode" example:""`
	PlatformId  int    `json:"platformId" gorm:"column:platformId" example:"1001"`
	PayType     int    `json:"payType" gorm:"column:payType" example:"1001"`
	CountryCode int    `json:"countryCode" gorm:"column:countryCode" example:"86"`
	Currency    int    `json:"currency" example:"86"`
	Amounts     string `json:"amounts" example:"50"`
	Note        string `json:"note" example:"note"`
	CreateTime  string `json:"createTime" gorm:"column:createTime" example:"2023-03-09 10:00:00"`
	UpdateTime  string `json:"updateTime" gorm:"column:updateTime" example:"2023-03-19 10:02:00"`
	Enable      int    `json:"enable" example:"1"`
}

type PayChannelError struct {
	Id          int    `json:"id"`
	ChannelName string `json:"channelName" gorm:"column:channelName" example:"ERROR"`
	PayCode     string `json:"payCode" gorm:"column:payCode" example:"ERROR"`
	PlatformId  int    `json:"platformId" gorm:"column:platformId"`
	PayType     int    `json:"payType" gorm:"column:payType"`
	CountryCode int    `json:"countryCode" gorm:"column:countryCode"`
	Currency    int    `json:"currency"`
	Amounts     string `json:"amounts" example:"amountsERROR"`
	Note        string `json:"note"`
	CreateTime  string `json:"createTime" gorm:"column:createTime"`
	UpdateTime  string `json:"updateTime" gorm:"column:updateTime"`
	Enable      int    `json:"enable"`
}
type Platformdatatype struct {
	Id          int    `json:"id" example:"1"`
	Name        string `json:"name" gorm:"column:name" example:"金牌代收"`
	Code        string `json:"code" gorm:"column:code" example:"golden"`
	UrlCall     string `json:"urlCall" gorm:"column:urlCall" example:"http://apay.kj176.com:6789/api/golden/charge"`
	UrlQuery    string `json:"urlQuery" gorm:"column:urlQuery" example:"http://apay.kj176.com:6789/api/golden/query"`
	UrlCallBack string `json:"urlCallBack" gorm:"column:urlCallBack" example:"http://wss.richclub.tv:30050/CallBackGolden"`
	Note        string `json:"note"`
	CreateTime  string `json:"createTime" gorm:"column:createTime" example:"2023-03-09 10:00:00"`
	UpdateTime  string `json:"updateTime" gorm:"column:updateTime" example:"2023-03-19 10:02:00"`
	Enable      int    `json:"enable" example:"1"`
}
type PlatformdatatypeError struct {
	Id          int    `json:"id"`
	Name        string `json:"name" gorm:"column:name" example:"ERROR"`
	Code        string `json:"code" gorm:"column:code" example:"ERROR"`
	UrlCall     string `json:"urlCall" gorm:"column:urlCall" example:"urlCallERROR"`
	UrlQuery    string `json:"urlQuery" gorm:"column:urlQuery" example:"urlQueryERROR"`
	UrlCallBack string `json:"urlCallBack" gorm:"column:urlCallBack" example:"urlCallBackERROR"`
	Note        string `json:"note"`
	CreateTime  string `json:"createTime" gorm:"column:createTime"`
	UpdateTime  string `json:"updateTime" gorm:"column:updateTime"`
	Enable      int    `json:"enable"`
}
type postdatatype struct {
	MerId      string  `json:"merId" gorm:"column:merId"`
	MerOrderNo string  `json:"merOrderNo" gorm:"column:merOrderNo"`
	OrderAmt   float64 `json:"orderAmt" gorm:"column:prderAmt"`
	OrderCode  string  `json:"orderCode" gorm:"column:orderCode"`
	NotifyURL  string  `json:"notifyURL" gorm:"column:notifyURL"`
	JumpURL    string  `json:"jumpURL" gorm:"column:jumpURL"`
	Sign       string  `json:"sign" gorm:"column:sign"`
}
type payRecords_channelcode struct {
	OrderNo     string  `json:"orderNo" gorm:"column:orderNo"`
	Uid         int     `json:"uid" gorm:"column:uid"`
	OrderAction int     `json:"orderAction" gorm:"column:orderAction"`
	Currency    int     `json:"currency" gorm:"column:currency"`
	Amount      float64 `json:"amount" gorm:"column:amount"`
	CreateTime  string  `json:"createTime" gorm:"column:createTime"`
	CreateData  string  `json:"createData" gorm:"column:createData"`
	OrderStage  int     `json:"orderStage" gorm:"column:orderStage"`
	OrderStatus int     `json:"orderStatus" gorm:"column:orderStatus"`
}

type UserCoinRecoderDataType struct {
	Id        int     `json:"id" gorm:"column:id"`
	Uid       int     `json:"uid" gorm:"column:uid"`
	Type      int     `json:"type" gorm:"column:type"`
	Action    int     `json:"action" gorm:"column:action"`
	OrderNo   string  `json:"orderNo" gorm:"column:orderNo"`
	Coin      int     `json:"coin" gorm:"column:coin"`
	Amount    float64 `json:"amount" gorm:"column:amount"`
	AfterCoin int     `json:"afterCoin" gorm:"column:afterCoin"`
	Time      string  `json:"time" gorm:"column:time"`
}

type BankAccountType struct {
	Id          int    `json:"id" example:"2"`
	UserId      int    `json:"userId" gorm:"column:userId" example:"1"`
	AccountBank string `json:"accountBank" gorm:"column:accountBank" example:"SS銀行"`
	AccountName string `json:"accountName" gorm:"column:accountName" example:"jack"`
	Account     string `json:"account" example:joke`
	Default     int    `json:"default" example:"1"`
	Enable      int    `json:"enable" example:"1"`
}

type BankAccountTypeError struct {
	Id          int    `json:"id"`
	UserId      int    `json:"userId" gorm:"column:userId"`
	AccountBank string `json:"accountBank" gorm:"column:accountBank" example:"ERROR"`
	AccountName string `json:"accountName" gorm:"column:accountName" example:"ERROR"`
	Account     string `json:"account" example:"ERROR"`
	Default     int    `json:"default"`
	Enable      int    `json:"enable"`
}
type ordernotype struct {
	OrderNo string `json:"urlCall" gorm:"column:urlCall"`
}

type amountsstring struct {
	Currency int    `json:"currency" gorm:"column:currency"`
	Amounts  string `json:"amounts" gorm:"column:amounts"`
}
type payoutpostdatatype struct {
	AppId      string  `json:"appId" gorm:"column:appId"`
	AppOrderNo string  `json:"appOrderNo" gorm:"column:appOrderNo"`
	OrderAmt   float64 `json:"orderAmt" gorm:"column:prderAmt"`
	PayId      string  `json:"payId" gorm:"column:payId"`
	AccNo      string  `json:"accNo" gorm:"column:accNo"`
	AccName    string  `json:"accName" gorm:"column:accName"`
	BankName   string  `json:"bankName" gorm:"column:bankName"`
	BankCode   string  `json:"bankCode" gorm:"column:bankCode"`
	NotifyURL  string  `json:"notifyURL" gorm:"column:notifyURL"`
	Sign       string  `json:"sign" gorm:"column:sign"`
}

type PayWithdrawRuleType struct {
	CountryCode int    `json:"countryCode" gorm:"column:countryCode" example:"86"`
	BetAmount   int    `json:"betAmount" gorm:"column:betAmount" example:"20000"`
	Duration    int    `json:"duration" gorm:"column:duration" example:"30"`
	Note        string `json:"note" example:"泰國"`
	CreateTime  string `json:"createTime" gorm:"column:createTime" example:"2022-09-29 06:30:33"`
	UpdateTime  string `json:"updateTime" gorm:"column:updateTime" example:"2022-11-16 08:18:00"`
	Enable      int    `json:"enable" example:"1"`
}
type PayWithdrawRuleTypeError struct {
	CountryCode int    `json:"countryCode" gorm:"column:countryCode"`
	BetAmount   int    `json:"betAmount" gorm:"column:betAmount"`
	Duration    int    `json:"duration" gorm:"column:duration"`
	Note        string `json:"note"`
	CreateTime  string `json:"createTime" gorm:"column:createTime"`
	UpdateTime  string `json:"updateTime" gorm:"column:updateTime"`
	Enable      int    `json:"enable"`
}

type AgencyPayOutOrderRecall struct {
	Code    string `json:"code" gorm:"column:code"`
	Msg     string `json:"msg" gorm:"column:msg"`
	OrderNo string `json:"orderNo" gorm:"column:orderNo"`
	AccNo   string `json:"accNo" gorm:"column:accNo"`
	Amt     string `json:"amt" gorm:"column:amt"`
}

type payoutRecords_channelcode struct {
	OrderNo     string  `json:"orderNo" gorm:"column:orderNo"`
	Uid         int     `json:"uid" gorm:"column:uid"`
	AccountBank string  `json:"accountBank" gorm:"column:accountBank"`
	AccountName string  `json:"accountName" gorm:"column:accountName"`
	Account     string  `json:"account" gorm:"column:account"`
	OrderAction int     `json:"orderAction" gorm:"column:orderAction"`
	Currency    int     `json:"currency" gorm:"column:currency"`
	Amount      float64 `json:"amount" gorm:"column:amount"`
	CreateTime  string  `json:"createTime" gorm:"column:createTime"`
	CreateData  string  `json:"createData" gorm:"column:createData"`
	OrderStage  int     `json:"orderStage" gorm:"column:orderStage"`
	OrderStatus int     `json:"orderStatus" gorm:"column:orderStatus"`
}

type userPayChannel struct {
	Id          int    `json:"id"`
	ChannelName string `json:"channelName" gorm:"column:channelName"`
	Amounts     string `json:"amounts"`
}

type userOrderType struct {
	Id        int    `json:"id"`
	UserId    int    `json:"userId" gorm:"column:userId"`
	ProductId int    `json:"productId" gorm:"column:productId"`
	Amount    int    `json:"amount" gorm:"column:amount"`
	Time      string `json:"time" gorm:"column:time"`
	Status    int    `json:"status" gorm:"column:status"`
}

type productType struct {
	Id            int    `json:"id"`
	Name          string `json:"name"`
	Desc          string `json:"desc"`
	Price         int    `json:"price"`
	WatchDuration int    `json:"watchDuration" gorm:"column:watchDuration"`
}

type videoAuthRecordType struct {
	Id         int    `json:"id"`
	UserId     int    `json:"userId" gorm:"column:userId"`
	ProductId  int    `json:"productId" gorm:"column:productId"`
	OrderTime  string `json:"orderTime" gorm:"column:orderTime"`
	ExpireTime string `json:"expireTime" gorm:"column:expireTime"`
	Status     int    `json:"status" gorm:"column:status"`
}
