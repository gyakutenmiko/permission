package controller

import (
	"regexp"
	"strconv"
	"time"

	//"github.com/forgoer/openssl"

	"database/sql"
	"io/ioutil"

	"github.com/bitly/go-simplejson"

	_ "github.com/go-sql-driver/mysql" //avoid golang mark mysql

	"log"
	"net/http"
)

// QueryManager
// GetApp QueryManager
// @Summary QueryManager
// @Description "ShowManagers"
// @Tags Manager
// @Content-Type application/json
// @Produce json
// @Param account header string false "account"
// @Param nickname header string false "nickname"
// @Success 200 {object} controller.manaq "OK"
// @Failure  201 {object} controller.manaq "ERROR"
// @Router /QueryManager [post]
func QueryManager(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	account := getRespBodyJson.Get("account").MustString()
	nickname := getRespBodyJson.Get("nickName").MustString()

	var retjs []manaq

	err := DB.Table("managers").Select("id,account,nickName,email,phone,createDatetime,authStatus,IFNULL(loginDatetime,'') as loginDatetime,loginCountry,country,language").Where("account LIKE '%" + account + "%' AND nickName LIKE '%" + nickname + "%'").Scan(&retjs).Error
	if err != nil {
		log.Println(err)
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response) //回傳
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: retjs}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response) //回傳
}

// NewManager
// GetApp NewManager
// @Summary NewManager
// @Description "AddNewManagers"
// @Tags Manager
// @Content-Type application/json
// @Produce json
// @Param account header string true "account"
// @Param nickname header string true "nickname"
// @Param email header string true "email"
// @Param pwd header string true "pwd"
// @Param phone header string true "phone"
// @Param loginCountry header int true "loginCountry"
// @Param country header int true "country"
// @Param language header int true "language"
// @Success 200 {object} controller.themanager "OK"
// @Failure  201 {object} controller.themanagerError "ERROR"
// @Router /NewManager [post]
func NewManager(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	account := getRespBodyJson.Get("account").MustString()
	nickname := getRespBodyJson.Get("nickName").MustString()
	email := getRespBodyJson.Get("email").MustString()
	pwd := getRespBodyJson.Get("pwd").MustString()
	phone := getRespBodyJson.Get("phone").MustString()
	logincountry := getRespBodyJson.Get("loginCountry").MustString()
	country := getRespBodyJson.Get("country").MustString()
	language := getRespBodyJson.Get("language").MustString()
	lg, err := strconv.Atoi(logincountry)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "loginCountryERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	co, err := strconv.Atoi(country)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "countryERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	la, err := strconv.Atoi(language)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "languageERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	timeutc := time.Date(time.Now().Year(), time.Now().Month(), time.Now().Day(), time.Now().Hour(), time.Now().Minute(), time.Now().Second(), 0, time.Local).UTC().Format("2006-01-02 15:04:05")

	// var execString string
	tx, err := DBGlobal.Begin()
	defer func(tx *sql.Tx) {
		err := tx.Commit()
		if err != nil {

		}
	}(tx)
	if err != nil {
		log.Println(err)
		return
	}
	//判斷account是否含特殊符號
	match, _ := regexp.MatchString("\\W", account)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "accountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if account == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "accountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷nick_name是否含特殊符號
	match, _ = regexp.MatchString("\\W", nickname)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "nickNameERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if nickname == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "nickNameERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷email是否照格式
	match, _ = regexp.MatchString("(\\w)[@](\\w+)(.com)", email)
	if match != true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "emailERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷password是否照格式
	err = VerifyPasswordRule(pwd, 6, 14)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "passwordERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷phone是否含數字以外的符號
	match, _ = regexp.MatchString("\\D", phone)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "phoneERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if phone == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "phoneERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}

	last := passwordhash(account, pwd)

	mana := themanager{
		Account:        account,
		NickName:       nickname,
		Email:          email,
		Pwd:            last,
		Phone:          phone,
		CreateDatetime: timeutc,
		AuthStatus:     2,
		LoginCountry:   lg,
		Country:        co,
		Language:       la,
	}
	if err = DB.Table("managers").Create(&mana).Error; err != nil {
		log.Println(err, "INSERT managers SET account=?,nickName=?,email=?,pwd=?,phone=?,createDatetime=utc_timestamp,loginCountry=?,country=?,language=?", account, nickname, email, last, phone, logincountry, country, language)
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}

func ManagerRegisterEmailCheck(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	account := getRespBodyJson.Get("account").MustString()
	email := getRespBodyJson.Get("email").MustString()
	phone := getRespBodyJson.Get("phone").MustString()
	registertime := time.Now().Local()
	losttime := registertime.Add(time.Minute * 3)

	//判斷account是否含特殊符號
	match, _ := regexp.MatchString("\\W", account)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "accountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if account == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "accountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷email是否照格式
	match, _ = regexp.MatchString("(\\w)[@](\\w+)(.com)", email)
	if match != true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "emailERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷phone是否含數字以外的符號
	match, _ = regexp.MatchString("\\D", phone)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "phoneERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if phone == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "phoneERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}

	type accounttype struct {
		Account string
	}
	var accountcheck accounttype

	err := DB.Table("managers").Select("account").Where("email = ? AND phone = ?", email, phone).Scan(&accountcheck).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "accountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if accountcheck.Account != account {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "accountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	code := registerHash(account, email, phone)
	err = redisClient.Set(account, code, time.Minute*3).Err()
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	send("anry@richclub.live", "<html><body><h1>"+account+"客戶您好:<br> 由於您於"+registertime.Format("2006/01/02 15:04")+"在CMS使用註冊功能,基於安全理由,註冊碼將於<br>"+losttime.Format("2006/01/02 15:04")+"失效,以下是您的註冊連結<br>http://172.0.0.1:30030/RegisterCheck?account="+account+"&code="+code+"<br>若有任何問題,歡迎至CMS客戶中心詢問或直接撥打專線02-xxx-xxx-xx會有專人為您服務</h1></body></html>")
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}
func ManagerRegisterCheck(writer http.ResponseWriter, request *http.Request) {
	account := request.URL.Query().Get("account")
	code := request.URL.Query().Get("code")
	var codecheck string
	type accounttype struct {
		Account string
	}
	var accountcheck accounttype

	err := DB.Table("managers").Select("account").Where("account = ?", account).Scan(&accountcheck).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "accountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if accountcheck.Account != account {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "accountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	codecheck, _ = redisClient.Get(account).Result()
	if codecheck != code {
		remail := ManagerRestartRegisterMail(account)
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "codeERROR remail " + strconv.FormatBool(remail)}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	err = DB.Table("managers").Where("account = ?", account).Update("authStatus", 1).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}

func ManagerRestartRegisterMail(account string) bool {
	registertime := time.Now().Local()
	losttime := registertime.Add(time.Minute * 3)
	type accounttype struct {
		Account string
		Email   string
		Phone   string
	}
	var accountcheck accounttype
	err := DB.Table("managers").Select("account,email,phone").Where("account = ?", account).Scan(&accountcheck).Error
	if err != nil {
		return false
	}
	if accountcheck.Account != account {
		return false
	}
	code := registerHash(account, accountcheck.Email, accountcheck.Phone)
	err = redisClient.Set(account, code, time.Minute*3).Err()
	if err != nil {
		return false
	}
	send("anry@richclub.live", "<html><body><h1>"+account+"客戶您好:<br> 由於您於"+registertime.Format("2006/01/02 15:04")+"在CMS使用註冊功能,基於安全理由,註冊碼將於<br>"+losttime.Format("2006/01/02 15:04")+"失效,以下是您的註冊連結<br>http://172.0.0.1:30030/RegisterCheck?account="+account+"&code="+code+"<br>若有任何問題,歡迎至CMS客戶中心詢問或直接撥打專線02-xxx-xxx-xx會有專人為您服務</h1></body></html>")
	return true
}

// ModifyManager
// GetApp ModifyManager
// @Summary ModifyManager
// @Description "ModifyManager"
// @Tags Manager
// @Content-Type application/json
// @Produce json
// @Param id header string true "id"
// @Param nickname header string true "nickname"
// @Param email header string true "email"
// @Param pwd header string true "pwd"
// @Param phone header string true "phone"
// @Param authStatus header int true "authStatus"
// @Param country header int true "country"
// @Param language header int true "language"
// @Success 200 {object} controller.themanager "OK"
// @Failure  201 {object} controller.themanagerError "ERROR"
// @Router /ModifyManager [post]
func ModifyManager(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	id := getRespBodyJson.Get("id").MustString()
	nickname := getRespBodyJson.Get("nickName").MustString()
	email := getRespBodyJson.Get("email").MustString()
	pwdold := getRespBodyJson.Get("pwdold").MustString()
	pwd := getRespBodyJson.Get("pwd").MustString()
	phone := getRespBodyJson.Get("phone").MustString()
	authstatus := getRespBodyJson.Get("authStatus").MustString()
	country := getRespBodyJson.Get("country").MustString()
	language := getRespBodyJson.Get("language").MustString()
	aut, err := strconv.Atoi(authstatus)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "authStatusERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	cou, err := strconv.Atoi(country)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "countryERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	la, err := strconv.Atoi(language)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "languageERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷nick_name是否含特殊符號
	match, _ := regexp.MatchString("\\W", nickname)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "nicknameERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if nickname == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "nickNameERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷email是否照格式
	match, _ = regexp.MatchString("(\\w)[@](\\w+)(.com)", email)
	if match != true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "emailERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷phone是否含數字以外的符號
	match, _ = regexp.MatchString("\\D", phone)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "phoneERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if phone == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "phoneERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷auth_status是否照格式
	match, _ = regexp.MatchString("[^0-2]|[0-2]{2,}", authstatus)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "authStatusERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if authstatus == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "authStatusERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}

	//判斷是否打算更新密碼
	if pwdold != "" {
		//判斷password是否照格式
		err = VerifyPasswordRule(pwd, 6, 14)
		if err != nil {
			response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "passwordERROR"}
			ResponseWithJson(writer, http.StatusOK, response)
			return
		}

		type accounttype struct {
			Account string
		}
		var account accounttype
		err = DB.Table("managers").Select("account").Where("id = ?", id).Scan(&account).Error
		if err != nil {
			response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "idERROR"}
			ResponseWithJson(writer, http.StatusOK, response)
			return
		}
		if account.Account == "" {
			response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "idERROR"}
			ResponseWithJson(writer, http.StatusOK, response)
			return
		}

		lastpasswordold := passwordhash(account.Account, pwdold)

		var accountcheck accounttype
		err = DB.Table("managers").Select("account").Where("pwd = ?", lastpasswordold).Scan(&accountcheck).Error
		if err != nil {
			response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "oldPasswordERROR"}
			ResponseWithJson(writer, http.StatusOK, response)
			return
		}
		if accountcheck.Account == "" {
			response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "oldPasswordERROR"}
			ResponseWithJson(writer, http.StatusOK, response)
			return
		}

		lastpassword := passwordhash(account.Account, pwd)

		if err = DB.Table("managers").Where("id = ?", id).Updates(theuser{NickName: nickname, Email: email, Pwd: lastpassword, Phone: phone, AuthStatus: aut, Country: cou, Language: la}).Error; err != nil {
			log.Println("UPDATE users SET nickName=?,email=?,pwd=?,phone=?,icon=?,userStory=?,coin=?,vipType=?,authStatus=?,country=?,sex=?,language=? WHERE id =?", nickname, email, pwd, phone, authstatus, country, language, id)
			response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
			ResponseWithJson(writer, http.StatusOK, response)
			return
		}
		response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//若沒打算更密則依序更新其他
	if err = DB.Table("managers").Where("id = ?", id).Updates(theuser{NickName: nickname, Email: email, Phone: phone, AuthStatus: aut, Country: cou, Language: la}).Error; err != nil {
		log.Println("UPDATE users SET nickName=?,email=?,phone=?,authStatus=?,country=?,,language=? WHERE id =?", nickname, email, phone, authstatus, country, language, id)
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}

func ManagerEmailCheck(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	account := getRespBodyJson.Get("account").MustString()
	email := getRespBodyJson.Get("email").MustString()
	forgettime := time.Now().Local()
	losttime := forgettime.Add(time.Minute * 3)

	//判斷account是否含特殊符號
	match, _ := regexp.MatchString("\\W", account)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "accountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if account == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "accountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷email是否照格式
	match, _ = regexp.MatchString("(\\w)[@](\\w+)(.com)", email)
	if match != true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "emailERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	type accounttype struct {
		Account string
	}
	var accountcheck accounttype

	err := DB.Table("managers").Select("account").Where("email = ?", email).Scan(&accountcheck).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: err.Error(), Info: ""}, Message: "emailERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if accountcheck.Account != account {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: err.Error(), Info: ""}, Message: "emailERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	authcode := GetAuthCode(6)
	authcoderune := []rune(authcode)
	err = redisClient.Set(account, authcode, time.Minute*3).Err()
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: err.Error(), Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	send("anry@richclub.live", "<html><body><h1>"+account+"客戶您好:<br> 由於您於"+forgettime.Format("2006/01/02 15:04")+"在CMS使用忘記密碼功能,基於安全理由,驗證碼將於<br>"+losttime.Format("2006/01/02 15:04")+"失效,以下是您的驗證碼<br><P style='display: inline-block; border-radius: 12px; background: #73AD21; width: 60px; height: 60px; font-size: 45px; text-align: center; margin: 5px; '>"+string(authcoderune[0])+"</P><P style='display: inline-block; border-radius: 12px; background: #73AD21; width: 60px; height: 60px; font-size: 45px; text-align: center; margin: 5px; '>"+string(authcoderune[1])+"</P><P style='display: inline-block; border-radius: 12px; background: #73AD21; width: 60px; height: 60px; font-size: 45px; text-align: center; margin: 5px; '>"+string(authcoderune[2])+"</P><P style='display: inline-block; border-radius: 12px; background: #73AD21; width: 60px; height: 60px; font-size: 45px; text-align: center; margin: 5px; '>"+string(authcoderune[3])+"</P><P style='display: inline-block; border-radius: 12px; background: #73AD21; width: 60px; height: 60px; font-size: 45px; text-align: center; margin: 5px; '>"+string(authcoderune[4])+"</P><P style='display: inline-block; border-radius: 12px; background: #73AD21; width: 60px; height: 60px; font-size: 45px; text-align: center; margin: 5px; '>"+string(authcoderune[5])+"</P><br>若有任何問題,歡迎至CMS客戶中心詢問或直接撥打專線02-xxx-xxx-xx會有專人為您服務</h1></body></html>")
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "" + email, Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}
func ManagerAuthcodePasswordCheck(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	account := getRespBodyJson.Get("account").MustString()
	authcode := getRespBodyJson.Get("authcode").MustString()
	pwd := getRespBodyJson.Get("pwd").MustString()

	//判斷account是否含特殊符號
	match, _ := regexp.MatchString("\\W", account)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "accountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if account == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "accountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	var authcodecheck string
	authcodecheck, _ = redisClient.Get(account).Result()
	if authcodecheck != authcode {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "authcodeERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	err := VerifyPasswordRule(pwd, 6, 14)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "passwordERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}

	lastpassword := passwordhash(account, pwd)

	if err = DB.Table("managers").Where("account = ?", account).Update("pwd", lastpassword).Error; err != nil {
		log.Println("UPDATE managers SET pwd=? WHERE account =?", pwd, account)
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}

// QueryRole
// GetApp QueryRole
// @Summary QueryRole
// @Description "ShowTheRoles"
// @Tags Role
// @Content-Type application/json
// @Produce json
// @Param roleName header string false "roleName"
// @Success 200 {object} controller.role "OK"
// @Failure  201 {object} controller.role "ERROR"
// @Router /QueryRole [post]
func QueryRole(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	rolename := getRespBodyJson.Get("roleName").MustString()

	var retjs []role

	err := DB.Table("managerRoles").Select("id, roleName , note , enable").Where("roleName LIKE '%" + rolename + "%'").Scan(&retjs).Error
	if err != nil {
		log.Println(err)
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: retjs}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response) //回傳
}

// NewRole
// GetApp NewRole
// @Summary NewRole
// @Description "AddNewRole"
// @Tags Role
// @Content-Type application/json
// @Produce json
// @Param roleName header string true "roleName"
// @Param note header string false "note"
// @Success 200 {object} controller.role "OK"
// @Failure  201 {object} controller.roleError "ERROR"
// @Router /NewRole [post]
func NewRole(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	rolename := getRespBodyJson.Get("roleName").MustString()
	note := getRespBodyJson.Get("note").MustString()
	//判斷rolename是否含特殊符號
	match, err := regexp.MatchString("\\W", rolename)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "roleNameERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if rolename == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "roleNameERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}

	newrole := role{
		RoleName: rolename,
		Note:     &note,
		Enable:   1,
	}
	if err = DB.Table("managerRoles").Create(&newrole).Error; err != nil {
		log.Println(err, "INSERT managerRoles SET roleName=?,note=?", rolename, note)
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}

// ModifyRole
// GetApp ModifyRole
// @Summary ModifyRole
// @Description "ModifyRole"
// @Tags Role
// @Content-Type application/json
// @Produce json
// @Param roleName header string true "roleName"
// @Param note header string false "note"
// @Success 200 {object} controller.role "OK"
// @Failure  201 {object} controller.roleError "ERROR"
// @Router /ModifyRole [post]
func ModifyRole(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	id := getRespBodyJson.Get("id").MustString()
	rolename := getRespBodyJson.Get("roleName").MustString()
	note := getRespBodyJson.Get("note").MustString()
	enable := getRespBodyJson.Get("enable").MustString()
	//判斷rolename是否含特殊符號
	match, err := regexp.MatchString("\\W", rolename)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "rolenameERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if rolename == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "roleNameERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}

	//判斷enable是否照格式
	match, err = regexp.MatchString("[^01]|[01]{2,}", enable)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "enableERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if enable == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "enableERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	ena, err := strconv.Atoi(enable)
	if err = DB.Table("managerRoles").Where("id = ?", id).Updates(role{RoleName: rolename, Note: &note, Enable: ena}).Error; err != nil {
		log.Println("UPDATE managerRoles SET roleName = ?,note = ? WHERE roleId = ?", rolename, note, id)
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}

// QueryManagerRole
// GetApp QueryManagerRole
// @Summary QueryManagerRole
// @Description "ShowMembersRole"
// @Tags RoleMember
// @Content-Type application/json
// @Produce json
// @Param role header string false "role"
// @Param account header string false "account"
// @Success 200 {object} controller.roleManager "OK"
// @Failure  201 {object} controller.roleManager "ERROR"
// @Router /QueryManagerRole [post]
func QueryManagerRole(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	role := getRespBodyJson.Get("role").MustString()
	account := getRespBodyJson.Get("account").MustString()
	var retjs []roleManager

	err := DB.Table("rolesManagersList A3").Select("A3.id, A1.account , A2.roleName , A3.enable").Joins("JOIN managers A1 ON A1.id = A3.managerId").Joins("JOIN managerRoles A2 ON A2.id = A3.roleId").Where("A1.account LIKE '%" + account + "%' AND A2.roleName LIKE '%" + role + "%'").Scan(&retjs).Error
	if err != nil {
		log.Println(err)
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: retjs}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response) //回傳
}

// NewManagerRole
// GetApp NewManagerRole
// @Summary NewManagerRole
// @Description "SetTheRoleToMember"
// @Tags RoleMember
// @Content-Type application/json
// @Produce json
// @Param roleId header int true "roleId"
// @Param managerId header int true "managerId"
// @Success 200 {object} controller.roleManagerId "OK"
// @Failure  201 {object} controller.roleManagerId "ERROR"
// @Router /NewManagerRole [post]
func NewManagerRole(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	roleid := getRespBodyJson.Get("roleId").MustString()
	managerid := getRespBodyJson.Get("managerId").MustString()
	rid, err := strconv.Atoi(roleid)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "roleIdERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	mid, err := strconv.Atoi(managerid)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "managerIdERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	mana := roleManagerId{
		RoleId:    rid,
		ManagerId: mid,
		Enable:    1,
	}
	if err = DB.Table("rolesManagersList").Create(&mana).Error; err != nil {
		log.Println(err, "INSERT rolesManagersList SET roleId=?,managerId=?", roleid, managerid)
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}

// OnOffManagerRole
// GetApp OnOffManagerRole
// @Summary OnOffManagerRole
// @Description "OnOffTheRoleForManager"
// @Tags RoleMember
// @Content-Type application/json
// @Produce json
// @Param roleId header int true "roleId"
// @Param managerId header int true "managerId"
// @Success 200 {object} controller.roleManagerId "OK"
// @Failure  201 {object} controller.roleManagerId "ERROR"
// @Router /OnOffManagerRole [post]
func OnOffManagerRole(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	roleid := getRespBodyJson.Get("roleId").MustString()
	managerid := getRespBodyJson.Get("managerId").MustString()
	enable := getRespBodyJson.Get("enable").MustString()
	tx, err := DBGlobal.Begin()
	defer func(tx *sql.Tx) {
		err := tx.Commit()
		if err != nil {

		}
	}(tx)
	if err != nil {
		log.Println(err)
		return
	}

	//判斷enable是否照格式
	match, err := regexp.MatchString("[^01]|[01]{2,}", enable)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "enableERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if enable == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "enableERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}

	if err = DB.Table("rolesManagersList").Where("roleId = ? AND managerId = ?", roleid, managerid).Update("enable", enable).Error; err != nil {
		log.Println("UPDATE rolesManagersList SET enable = 0 WHERE roleId = ? AND managerId = ?", roleid, managerid)
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}

// QueryPermission
// GetApp QueryPermission
// @Summary QueryPermission
// @Description "ShowThePremissions"
// @Tags Premission
// @Content-Type application/json
// @Produce json
// @Param permission header string false "permission"
// @Success 200 {object} controller.permission "OK"
// @Failure  201 {object} controller.permission "ERROR"
// @Router /QueryPermission [post]
func QueryPermission(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	thepermission := getRespBodyJson.Get("permission").MustString()
	tx, err := DBGlobal.Begin()
	defer func(tx *sql.Tx) {
		err := tx.Commit()
		if err != nil {

		}
	}(tx)
	if err != nil {
		log.Println(err)
		return
	}
	var retjs []permission

	err = DB.Table("managerPermissions").Select("id, permission, level, sort, parentId, IFNULL(linkFunction,'') as linkFuction").Where("permission LIKE '%" + thepermission + "%'").Scan(&retjs).Error
	if err != nil {
		log.Println(err)
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: retjs}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response) //回傳
}

// NewPermission
// GetApp NewPermission
// @Summary NewPermission
// @Description "AddNewPermission"
// @Tags Premission
// @Content-Type application/json
// @Produce json
// @Param permission header string true "permission"
// @Param parent_id header int true "parent_id"
// @Param linkFunction header string false "linkFuction"
// @Success 200 {object} controller.permission "OK"
// @Failure  201 {object} controller.permissionError "ERROR"
// @Router /NewPermission [post]
func NewPermission(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	thepermission := getRespBodyJson.Get("permission").MustString()
	parentid := getRespBodyJson.Get("parentId").MustString()
	link_function := getRespBodyJson.Get("linkFunction").MustString()
	par, err := strconv.Atoi(parentid)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "parentIdERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	var parentlinkcheck permission
	err = DB.Table("managerPermissions").Select("linkFunction").Where("parentId = ?", parentid).Scan(&parentlinkcheck).Error
	if parentlinkcheck.LinkFunction != "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "linkFunctionERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}

	//判斷permission是否含特殊符號
	match, err := regexp.MatchString("\\W", thepermission)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "permissionERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if thepermission == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "permissionERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	type typelevel struct {
		Level int
	}
	type typesort struct {
		Count int
	}
	var count int64

	var level typelevel
	var sort typesort
	if par < 0 {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "parentIdERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if par > 0 {
		err = DB.Table("managerPermissions").Select("level").Where("id = ?", parentid).Scan(&level).Error
		if err != nil {
			log.Println(err)
			response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "parentIdERROR"}
			ResponseWithJson(writer, http.StatusOK, response)
			return
		}
		level.Level += 1
	} else {
		level.Level = 1
	}
	err = DB.Table("managerPermissions").Where("parentId = ?", parentid).Count(&count).Error
	sort.Count = int(count) + 1
	newpermission := permission{
		Permission:   thepermission,
		Level:        level.Level,
		Sort:         sort.Count,
		ParentId:     par,
		LinkFunction: link_function,
		Enable:       1,
	}
	if err = DB.Table("managerPermissions").Create(&newpermission).Error; err != nil {
		log.Println(err, "INSERT managerPermissions SET permission=?,level=?,sort=?,parentId=?,linkFunction=?", thepermission, level, sort, parentid, link_function)
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}
func ModifyPermission(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	id := getRespBodyJson.Get("id").MustString()
	thepermission := getRespBodyJson.Get("permission").MustString()
	parentid := getRespBodyJson.Get("parentId").MustString()
	link_function := getRespBodyJson.Get("linkFunction").MustString()
	enable := getRespBodyJson.Get("enable").MustString()
	idint, err := strconv.Atoi(id)
	par, err := strconv.Atoi(parentid)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "parentIdERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}

	//判斷permission是否含特殊符號
	match, err := regexp.MatchString("\\W", thepermission)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "permissionERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if thepermission == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "permissionERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	type typelevel struct {
		Level int
	}
	type typesort struct {
		Sort int
	}
	var count int64

	var level typelevel
	var sort typesort
	if par < 0 {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "parentIdERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if par > 0 {
		err = DB.Table("managerPermissions").Select("level").Where("id = ?", parentid).Scan(&level).Error
		if err != nil {
			log.Println(err)
			response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "parentIdERROR"}
			ResponseWithJson(writer, http.StatusOK, response)
			return
		}
		level.Level += 1
	} else {
		level.Level = 1
	}
	err = DB.Table("managerPermissions").Where("parentId = ?", parentid).Count(&count).Error
	sort.Sort = int(count) + 1

	var sortold typesort
	type typeparent struct {
		Id int
	}
	var parentidold typeparent
	var linkold permission
	var countold int64
	//存取舊位置
	err = DB.Table("managerPermissions").Select("parentId").Where("id = ?", idint).Scan(&parentidold).Error
	err = DB.Table("managerPermissions").Select("sort").Where("id = ?", idint).Scan(&sortold).Error
	if parentidold.Id == par {
		sort.Sort = sortold.Sort
	}
	err = DB.Table("managerPermissions").Select("linkFunction").Where("id = ?", idint).Scan(&linkold).Error
	if linkold.LinkFunction == "" {
		link_function = ""
	} else {
		if link_function == "" {
			response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "linkfuncERROR"}
			ResponseWithJson(writer, http.StatusOK, response)
			return
		}
	}
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "sortoldERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	err = DB.Table("managerPermissions").Select("parentId").Where("id = ?", idint).Scan(&parentidold).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "parentidoldoldERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	err = DB.Table("managerPermissions").Where("parentId = ?", parentidold.Id).Count(&countold).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "countERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷enable是否照格式
	match, err = regexp.MatchString("[^01]|[01]{2,}", enable)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "enableERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if enable == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "enableERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	ena, err := strconv.Atoi(enable)
	if err = DB.Table("managerPermissions").Where("id = ?", idint).Updates(map[string]interface{}{"permission": thepermission, "level": level.Level, "sort": sort.Sort, "parentId": par, "linkFunction": link_function, "enable": ena}).Error; err != nil {
		log.Println(err, "Update managerPermissions SET permission=?,level=?,sort=?,parentId=?,linkFunction=? WHERE id = ?", thepermission, level, sort, parentid, link_function, id)
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	} else {
		if countold > 1 {
			for i := sortold.Sort; i < int(countold); i++ {
				err = DB.Table("managerPermissions").Where("parentid = ? AND sort = ?", parentidold.Id, i+1).Update("sort", i).Error
			}
		}
		OkOrError := changeTheChildrenLevelForPermission(idint, level.Level)
		if OkOrError == false {
			response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
			ResponseWithJson(writer, http.StatusOK, response)
			return
		}
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}
func changeTheChildrenLevelForPermission(parentid int, parentlevel int) bool {
	var count int64
	var linkcheck permission
	err := DB.Table("managerPermissions").Where("parentId = ?", parentid).Count(&count).Error
	err = DB.Table("managerPermissions").Where("parentId = ?", parentid).Update("level", parentlevel+1).Error
	for i := 0; i < int(count); i++ {
		if err = DB.Table("managerPermissions").Select("id,level,linkFunction").Where("parentId = ? AND sort = ?", parentid, i+1).Scan(&linkcheck).Error; err != nil {
			return false
		}
		if linkcheck.LinkFunction == "" {
			OkOrError := changeTheChildrenLevelForPermission(linkcheck.Id, linkcheck.Level)
			if OkOrError == false {
				return false
			}
		}
	}
	return true
}

func QueryRolePermission(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	role := getRespBodyJson.Get("role").MustString()
	permission := getRespBodyJson.Get("permission").MustString()
	var retjs []rolePermission

	err := DB.Table("rolesPermissionsList A1").Select("A1.id, A2.roleName, A2.note , A3.permission , A1.enable").Joins("JOIN managerRoles A2 ON A2.id = A1.roleId").Joins("JOIN managerPermissions A3 ON A3.id = A1.permissionId").Where("A2.roleName LIKE '%" + role + "%' AND A3.permission LIKE '%" + permission + "%'").Scan(&retjs).Error
	if err != nil {
		log.Println(err)
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: retjs}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response) //回傳
}

// NewRolePermission
// GetApp NewRolePermission
// @Summary NewRolePermission
// @Description "SetPermissionToRole"
// @Tags RolePermission
// @Content-Type application/json
// @Produce json
// @Param roleId header int true "roleId"
// @Param permissionId header int true "permissionId"
// @Success 200 {object} controller.rolePermissionId "OK"
// @Failure  201 {object} controller.rolePermissionId "ERROR"
// @Router /NewRolePermission [post]
func NewRolePermission(writer http.ResponseWriter, request *http.Request) {
	var rope rolePermissionId
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	roleid := getRespBodyJson.Get("roleId").MustString()
	permissionid := getRespBodyJson.Get("permissionId").MustString()
	rid, err := strconv.Atoi(roleid)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "roleIdERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	pid, err := strconv.Atoi(permissionid)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "permissionIdERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}

	var count int64
	type idtype struct {
		Id int
	}
	var exisitcheck int64
	err = DB.Table("rolesPermissionsList").Where("roleId =" + roleid + " AND permissionId = " + permissionid).Count(&exisitcheck).Error
	if exisitcheck > 0 {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR:Data is exisit"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	err = DB.Table("managerPermissions").Where("parentId = ?", permissionid).Count(&count).Error
	rope = rolePermissionId{
		RoleId:       rid,
		PermissionId: pid,
		Enable:       1,
	}
	if err = DB.Table("rolesPermissionsList").Create(&rope).Error; err != nil {
		log.Println(err, "INSERT rolesPermissionsList SET roleId=?,permissionId=?", roleid, permissionid)
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	} else {
		if count > 0 {

			OkOrError := putChildPermissionToRole(pid, rid)
			if OkOrError == false {
				response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
				ResponseWithJson(writer, http.StatusOK, response)
				return
			}
		}
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}
func putChildPermissionToRole(parid int, rid int) bool {
	var count int64
	type idtype struct {
		Id int
	}
	err := DB.Table("managerPermissions").Where("parentId = ?", parid).Count(&count).Error
	if count > 0 {
		var childpermissionid []idtype
		var linkcheck permission
		var exisitcheck int64
		err = DB.Table("managerPermissions").Select("id").Where("parentId = ?", parid).Scan(&childpermissionid).Error
		for i := 0; i < int(count); i++ {
			err = DB.Table("rolesPermissionsList").Where("roleId =" + strconv.Itoa(rid) + " AND permissionId = " + strconv.Itoa(childpermissionid[i].Id)).Count(&exisitcheck).Error
			if exisitcheck == 0 {
				var rope = rolePermissionId{
					RoleId:       rid,
					PermissionId: childpermissionid[i].Id,
					Enable:       1,
				}
				if err = DB.Table("rolesPermissionsList").Create(&rope).Error; err != nil {
					log.Println(err, "INSERT rolesPermissionsList SET roleId=?,permissionId=?", strconv.Itoa(rid), strconv.Itoa(childpermissionid[i].Id))
					return false
				}
				err = DB.Table("managerPermissions").Select("linkFunction").Where("id = ?", strconv.Itoa(childpermissionid[i].Id)).Scan(&linkcheck).Error
				if linkcheck.LinkFunction == "" {
					OkOrError := putChildPermissionToRole(childpermissionid[i].Id, rid)
					if OkOrError == false {
						return OkOrError
					}
				}
			}
		}
	}
	return true
}

// OnOffRolePermission
// GetApp OnOffRolePermission
// @Summary OnOffRolePermission
// @Description "OnOffThePermissionForRole"
// @Tags RolePermission
// @Content-Type application/json
// @Produce json
// @Param roleId header int true "roleId"
// @Param permissionId header int true "permissionId"
// @Param enable header int true "enable"
// @Success 200 {object} controller.rolePermissionId "OK"
// @Failure  201 {object} controller.rolePermissionId "ERROR"
// @Router /OnOffRolePermission [post]
func OnOffRolePermission(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	roleid := getRespBodyJson.Get("roleId").MustString()
	permissionid := getRespBodyJson.Get("permissionId").MustString()
	enable := getRespBodyJson.Get("enable").MustString()
	rol, err := strconv.Atoi(roleid)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "roleIdERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	per, err := strconv.Atoi(permissionid)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "permissionIdERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	ena, err := strconv.Atoi(enable)
	if err != nil {
		log.Println(err)
		return
	}
	//判斷enable是否照格式
	match, err := regexp.MatchString("[^01]|[01]{2,}", enable)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "enableERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if enable == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "enableERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}

	var count int64
	err = DB.Table("managerPermissions").Where("parentId = " + permissionid).Count(&count).Error

	if err = DB.Table("rolesPermissionsList").Where("roleId = ? AND permissionId = ?", rol, per).Update("enable", ena).Error; err != nil {
		log.Println("UPDATE rolesPermissionsList SET enable = ? WHERE roleId = ? AND permissionId = ?", ena, roleid, permissionid)
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	} else {
		if count > 0 {
			OkOrError := onOffChildPermissionToRole(per, rol, ena)
			if OkOrError == false {
				response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
				ResponseWithJson(writer, http.StatusOK, response)
				return
			}
		}
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}

func onOffChildPermissionToRole(parid int, rid int, enable int) bool {
	var count int64
	type idtype struct {
		Id int
	}
	err := DB.Table("managerPermissions").Where("parentId = ?", parid).Count(&count).Error
	if count > 0 {
		var childpermissionid []idtype
		var linkcheck permission
		var exisitcheck int64
		err = DB.Table("managerPermissions").Select("id").Where("parentId = ?", parid).Scan(&childpermissionid).Error
		for i := 0; i < int(count); i++ {
			err = DB.Table("rolesPermissionsList").Where("roleId = ? AND permissionId = ?", rid, childpermissionid[i].Id).Count(&exisitcheck).Error
			if exisitcheck != 0 {
				if err = DB.Table("rolesPermissionsList").Where("roleId = ? AND permissionId = ?", rid, childpermissionid[i].Id).Update("enable", enable).Error; err != nil {
					log.Println(err, "UPDATE rolesPermissionsList SET enable = ? WHERE roleId = ? AND permissionId = ?", enable, rid, childpermissionid[i].Id)
					return false
				}
				err = DB.Table("managerPermissions").Select("linkFunction").Where("id = " + strconv.Itoa(childpermissionid[i].Id)).Scan(&linkcheck).Error
				if linkcheck.LinkFunction == "" {
					OkOrError := onOffChildPermissionToRole(childpermissionid[i].Id, rid, enable)
					if OkOrError == false {
						return OkOrError
					}
				}
			}
		}
	}
	return true
}

// QueryBanPermission
// GetApp QueryBanPermission
// @Summary QueryBanPermission
// @Description "ShowBanManagerPermissions"
// @Tags BanPermission
// @Content-Type application/json
// @Produce json
// @Param permission header string false "permission"
// @Param account header string false "account"
// @Success 200 {object} controller.permissionBanList "OK"
// @Failure  201 {object} controller.permissionBanList "ERROR"
// @Router /QueryBanPermission [post]
func QueryBanPermission(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	permission := getRespBodyJson.Get("permission").MustString()
	account := getRespBodyJson.Get("account").MustString()
	var retjs []permissionBanList

	err := DB.Table("managersPermissionsBan A3").Select("A3.id, A1.permission,A2.account , A3.enable").Joins("JOIN managerPermissions A1 ON A1.id = A3.permissionId").Joins("JOIN managers A2 ON A2.id = A3.managerId").Where("A1.permission LIKE '%" + permission + "%' AND A2.roleName LIKE '%" + account + "%'").Scan(&retjs).Error
	if err != nil {
		log.Println(err)
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: retjs}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response) //回傳
}

// NewBanPermission
// GetApp NewBanPermission
// @Summary NewBanPermission
// @Description "BanTheManagersPermission"
// @Tags BanPermission
// @Content-Type application/json
// @Produce json
// @Param permissionId header int true "permissionId"
// @Param managerId header int true "managerId"
// @Success 200 {object} controller.banManagersPermission "OK"
// @Failure  201 {object} controller.banManagersPermission "ERROR"
// @Router /NewBanPermission [post]
func NewBanPermission(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	permissionid := getRespBodyJson.Get("permissionId").MustString()
	managerid := getRespBodyJson.Get("managerId").MustString()
	perid, err := strconv.Atoi(permissionid)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "permissionIdERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	manid, err := strconv.Atoi(managerid)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "managerIdERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	var exisitcheck int64
	err = DB.Table("permissionsManagerBan").Where("managerId =" + managerid + " AND permissionId = " + permissionid).Count(&exisitcheck).Error
	if exisitcheck > 0 {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR:Data is exisit"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	var count int64
	err = DB.Table("managerPermissions").Where("parentId = ?", perid).Count(&count).Error
	newbanpermission := banManagersPermission{
		PermissionId: perid,
		ManagerId:    manid,
		Enable:       1,
	}
	if err = DB.Table("permissionsManagerBan").Create(&newbanpermission).Error; err != nil {
		log.Println(err, "INSERT managerPermissions SET permissionId=?,managerId=?", permissionid, managerid)
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	} else {
		if int(count) > 0 {

			OkOrError := banChildPermissionForManager(perid, manid)
			if OkOrError == false {
				response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
				ResponseWithJson(writer, http.StatusOK, response)
				return
			}
		}
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}

func banChildPermissionForManager(parid int, mid int) bool {
	var count int64
	type idtype struct {
		Id int
	}
	err := DB.Table("managerPermissions").Where("parentId = ?", parid).Count(&count).Error
	if count > 0 {
		var childpermissionid []idtype
		var linkcheck permission
		var exisitcheck int64
		err = DB.Table("managerPermissions").Select("id").Where("parentId = ?", parid).Scan(&childpermissionid).Error
		for i := 0; i < int(count); i++ {
			err = DB.Table("permissionsManagerBan").Where("managerId = ? AND permissionId = ?", mid, childpermissionid[i].Id).Count(&exisitcheck).Error
			if exisitcheck == 0 {
				var rope = banManagersPermission{
					ManagerId:    mid,
					PermissionId: childpermissionid[i].Id,
					Enable:       1,
				}
				if err = DB.Table("permissionsManagerBan").Create(&rope).Error; err != nil {
					log.Println(err, "INSERT permissionsManagerBan SET managerId=?,permissionId=?", mid, childpermissionid[i].Id)
					return false
				}
				err = DB.Table("managerPermissions").Select("linkFunction").Where("id = ?", childpermissionid[i].Id).Scan(&linkcheck).Error
				if linkcheck.LinkFunction == "" {
					OkOrError := banChildPermissionForManager(childpermissionid[i].Id, mid)
					if OkOrError == false {
						return OkOrError
					}
				}
			}
		}
	}
	return true
}

// OnOffBanPermission
// GetApp OnOffBanPermission
// @Summary BanPermissionOnOff
// @Description "OnOffTheBanList"
// @Tags BanPermission
// @Content-Type application/json
// @Produce json
// @Param permissionId header int true "permissionId"
// @Param managerId header int true "managerId"
// @Param enable header int true "enable"
// @Success 200 {object} controller.banManagersPermission "OK"
// @Failure  201 {object} controller.banManagersPermission "ERROR"
// @Router /BanPermissionOnOff [post]
func OnOffBanPermission(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	permissionid := getRespBodyJson.Get("permissionId").MustString()
	managerid := getRespBodyJson.Get("managerId").MustString()
	enable := getRespBodyJson.Get("enable").MustString()
	perid, err := strconv.Atoi(permissionid)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "permissionIdERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	manid, err := strconv.Atoi(managerid)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "managerIdERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	tx, err := DBGlobal.Begin()
	defer func(tx *sql.Tx) {
		err := tx.Commit()
		if err != nil {

		}
	}(tx)
	if err != nil {
		log.Println(err)
		return
	}
	//判斷enable是否照格式
	match, err := regexp.MatchString("[^01]|[01]{2,}", enable)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "enableERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if enable == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "enableERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	ena, err := strconv.Atoi(enable)

	if err = DB.Table("permissionsManagerBan").Where("permissionId = ? AND managerId = ?", perid, manid).Update("enable", ena).Error; err != nil {
		log.Println("UPDATE permissionsManagerBan SET enable = ? WHERE permissionId = ? AND managerId = ?", ena, permissionid, managerid)
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	} else {
		OkOrError := onOffBanChildPermissionForManager(perid, manid, ena)
		if OkOrError == false {
			response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
			ResponseWithJson(writer, http.StatusOK, response)
			return
		}
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}

func onOffBanChildPermissionForManager(parid int, mid int, enable int) bool {
	var count int64
	type idtype struct {
		Id int
	}
	err := DB.Table("managerPermissions").Where("parentId = ?", parid).Count(&count).Error
	if count > 0 {
		var childpermissionid []idtype
		var linkcheck permission
		var exisitcheck int64
		err = DB.Table("managerPermissions").Select("id").Where("parentId = ?", parid).Scan(&childpermissionid).Error
		for i := 0; i < int(count); i++ {
			err = DB.Table("permissionsManagerBan").Where("permissionId = ? AND managerId = ? ", childpermissionid[i].Id, mid).Count(&exisitcheck).Error
			if exisitcheck != 0 {
				if err = DB.Table("permissionsManagerBan").Where("permissionId = ? AND managerId = ? ", childpermissionid[i].Id, mid).Update("enable", enable).Error; err != nil {
					return false
				}
				err = DB.Table("managerPermissions").Select("linkFunction").Where("id = ?", childpermissionid[i].Id).Scan(&linkcheck).Error
				if linkcheck.LinkFunction == "" {
					OkOrError := onOffBanChildPermissionForManager(childpermissionid[i].Id, mid, enable)
					if OkOrError == false {
						return OkOrError
					}
				}
			}
		}
	}
	return true
}
