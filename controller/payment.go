package controller

import (
	"bytes"
	"crypto/md5"
	"encoding/json"
	"fmt"

	//"go/types"
	"io"
	"io/ioutil"
	"net/http"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"sync"

	//"strings"
	"encoding/hex"
	"time"

	"github.com/bitly/go-simplejson"
	"github.com/jinzhu/gorm"
)

var wg sync.WaitGroup

// QueryPayInChannels
// GetApp QueryPayInChannels
// @Summary QueryPayInChannels
// @Description "QueryPayInChannels"
// @Tags Payment
// @Content-Type application/json
// @Produce json
// @Param userId header int false "userId"
// @Success 200 {object} controller.userPayChannel "OK"
// @Failure  201 {object} controller.userPayChannel "ERROR"
// @Router /QueryPayInChannels [post]
func QueryPayInChannels(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	userid := getRespBodyJson.Get("userId").MustString()
	uid, err := strconv.Atoi(userid)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "userIDERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	var channels []userPayChannel
	err = DB.Table("payChannels A2").Select("A2.id,A2.channelName,A2.amounts").Joins("JOIN users A1 ON A1.loginCountry = A2.countryCode").Where("A1.id = ?", uid).Scan(&channels).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: channels}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}

// CallPayIn
// GetApp CallPayIn
// @Summary CallPayIn
// @Description "入金下單"
// @Tags Payment
// @Content-Type application/json
// @Produce json
// @Param userId header int true "userId"
// @Param channelId formData int true "channelId"
// @Param amounts formData float64 true "amounts"
// @Success 200 string string "payurl"
// @Router /CallPayIn [post]
func CallPayIn(writer http.ResponseWriter, request *http.Request) {
	createTime := time.Now().UTC().Format("2006-01-02 15:04:05")
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	thecreatedata, _ := json.Marshal(getRespBodyJson)
	userid := getRespBodyJson.Get("userId").MustString()
	channelid := getRespBodyJson.Get("channelId").MustString()
	amounts := getRespBodyJson.Get("amounts").MustString()
	uid, err := strconv.Atoi(userid)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "userIDERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	var usercheck userq
	_ = DB.Table("users").Select("id").Where("id = " + userid).Scan(&usercheck).Error
	if usercheck.Id != uid {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "userIDERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	cid, err := strconv.Atoi(channelid)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "channelIdERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	var channelcheck PayChannel
	_ = DB.Table("payChannels").Select("id").Where("id = " + channelid).Scan(&channelcheck).Error
	if channelcheck.Id != cid {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "channelIdERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	amt, err := strconv.ParseFloat(amounts, 64)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "amountsERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if amounts == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "amountsERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	amounts = strconv.FormatFloat(amt, 'f', 2, 64)
	timeutc := strings.Replace(time.Now().UTC().Format("060102150405.000"), ".", "", 1)
	var amountscheck amountsstring
	err = DB.Table("payChannels A2").Select("A2.currency,A2.amounts").Joins("JOIN users A1 ON A1.loginCountry = A2.countryCode").Where("A1.id = ? AND A2.id = ?", uid, cid).Scan(&amountscheck).Error
	amountcheckstrings := strings.Split(amountscheck.Amounts, ",")
	var amountOK bool
	for _, amountcheck := range amountcheckstrings {
		if amounts == amountcheck {
			amountOK = true
			break
		}
	}
	if amountOK != true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR:The amount doesn't enable"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	var paydata PayChannel
	err = DB.Table("payChannels A2").Select("A2.payCode,A2.payType").Joins("JOIN users A1 ON A1.loginCountry = A2.countryCode").Where("A1.id = ? AND A2.id = ?", uid, cid).Scan(&paydata).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "payDataERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	var urlset Platformdatatype
	err = DB.Table("payChannels A2").Select("A3.code,A3.urlCall,A3.urlCallBack").Joins("JOIN users A1 ON A1.loginCountry = A2.countryCode").Joins("JOIN payPlatforms A3 ON A2.platformId = A3.id").Where("A1.id = ? AND A2.id = ?", uid, cid).Scan(&urlset).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	payrecord := "payRecords_" + urlset.Code
	theUserId := fmt.Sprintf("%06d", uid)
	theChannelId := fmt.Sprintf("%04d", cid)
	var orderno ordernotype
	orderno.OrderNo = theUserId + theChannelId + timeutc

	var keyset = map[string]string{
		"MerId":      goldMerId,
		"MerOrderNo": orderno.OrderNo,
		"OrderAmt":   amounts,
		"OrderCode":  paydata.PayCode,
		"NotifyURL":  urlset.UrlCallBack,
		"JumpURL":    "",
	}
	thekeyset, _ := json.Marshal(keyset)
	thekey := sortthekey(thekeyset)
	thekey += "&key=" + goldKey

	payrecordcode := payRecords_channelcode{
		OrderNo:     orderno.OrderNo,
		Uid:         uid,
		OrderAction: paydata.PayType,
		Currency:    amountscheck.Currency,
		Amount:      amt,
		CreateTime:  createTime,
		CreateData:  string(thecreatedata),
		OrderStage:  1,
		OrderStatus: 0,
	}
	err = DB.Table(payrecord).Create(&payrecordcode).Error

	h := md5.New()
	io.WriteString(h, thekey)
	thesign := hex.EncodeToString(h.Sum(nil))
	thesign = strings.ToUpper(thesign)
	fmt.Println(thesign)
	data := postdatatype{
		MerId:      goldMerId,
		MerOrderNo: orderno.OrderNo,
		OrderAmt:   amt,
		OrderCode:  paydata.PayCode,
		NotifyURL:  urlset.UrlCallBack,
		JumpURL:    "",
		Sign:       thesign,
	}
	jsondata, _ := json.Marshal(data)
	resp, err := http.Post(urlset.UrlCall, "application/json", bytes.NewBuffer(jsondata))
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "respERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	calloutTime := time.Now().UTC().Format("2006-01-02 15:04:05.000")
	err = DB.Table(payrecord).Where("orderNo = ?", payrecordcode.OrderNo).Update("calloutTime", calloutTime).Error
	err = DB.Table(payrecord).Where("orderNo = ?", payrecordcode.OrderNo).Update("calloutData", string(jsondata)).Error
	err = DB.Table(payrecord).Where("orderNo = ?", payrecordcode.OrderNo).Update("orderStage", 2).Error
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "bodyERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	paygetRespBodyJson, _ := simplejson.NewJson(body)
	thereturndata, _ := json.Marshal(paygetRespBodyJson)
	payurl := paygetRespBodyJson.Get("data").Get("payUrl").MustString()
	returnTime := time.Now().UTC().Format("2006-01-02 15:04:05")
	err = DB.Table(payrecord).Where("orderNo = ?", payrecordcode.OrderNo).Update("returnTime", returnTime).Error
	err = DB.Table(payrecord).Where("orderNo = ?", payrecordcode.OrderNo).Update("returnData", string(thereturndata)).Error
	err = DB.Table(payrecord).Where("orderNo = ?", payrecordcode.OrderNo).Update("orderStage", 3).Error
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: payurl}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
	paynotifyTime := time.Now().UTC().Format("2006-01-02 15:04:05")
	err = DB.Table(payrecord).Where("orderNo = ?", payrecordcode.OrderNo).Update("paynotifyTime", paynotifyTime).Error
	err = DB.Table(payrecord).Where("orderNo = ?", payrecordcode.OrderNo).Update("orderStage", 4).Error
	err = DB.Table(payrecord).Where("orderNo = ?", payrecordcode.OrderNo).Update("orderStatus", 1).Error
}

// CallBackGolden
// GetApp CallBackGolden
// @Summary CallBackGolden
// @Description "入金回調"
// @Tags Payment
// @Content-Type application/json
// @Produce json
// @Param merOrderNo header string true "merOrderNo"
// @Param orderNo formData int true "orderNo"
// @Param orderTime formData string true "orderTime"
// @Param merId formData string true "merId"
// @Param orderAmt formData float64 true "orderAmt"
// @Param payAmt formData float64 true "payAmt"
// @Param orderStatus formData string true "orderStatus"
// @Param sign formData string true "sign"
// @Success 200 string string "SUCCESS"
// @Failure  201 string string "ERROR"
// @Router /CallBackGolden [post]
func CallBackGolden(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	thecallbackdata, _ := json.Marshal(getRespBodyJson)
	merorderno := getRespBodyJson.Get("merOrderNo").MustString()
	orderno := getRespBodyJson.Get("orderNo").MustString()
	ordertime := getRespBodyJson.Get("orderTime").MustString()
	merid := getRespBodyJson.Get("merId").MustString()
	orderamt := getRespBodyJson.Get("orderAmt").MustString()
	payamt := getRespBodyJson.Get("payAmt").MustString()
	orderstatus := getRespBodyJson.Get("orderStatus").MustString()
	sign := getRespBodyJson.Get("sign").MustString()
	var payrecordcheck payRecords_channelcode
	_ = DB.Table("payRecords_golden").Select("orderNo,orderStatus").Where("orderNo = ?", merorderno).Scan(&payrecordcheck)
	if payrecordcheck.OrderNo == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "merOrderNoERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if payrecordcheck.OrderStatus >= 3 {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "The order is over"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	callbackTime := time.Now().UTC().Format("2006-01-02 15:04:05")
	_ = DB.Table("payRecords_golden").Where("orderNo = ?", merorderno).Update("callbackTime", callbackTime).Error
	_ = DB.Table("payRecords_golden").Where("orderNo = ?", merorderno).Update("callbackData", string(thecallbackdata)).Error
	_ = DB.Table("payRecords_golden").Where("orderNo = ?", merorderno).Update("orderStage", 5).Error
	_ = DB.Table("payRecords_golden").Where("orderNo = ?", merorderno).Update("orderStatus", 2).Error
	oamt, err := strconv.ParseFloat(orderamt, 64)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "orderAmtERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	_ = DB.Table("payRecords_golden").Select("amount").Where("orderNo = ?", merorderno).Scan(&payrecordcheck).Error
	if oamt != payrecordcheck.Amount {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "orderAmtERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	pamt, err := strconv.Atoi(payamt)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "payAmtERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	fmt.Println(pamt)
	if pamt < 0 {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "payAmt smaller than 0"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if orderno == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "orderNoERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	match, _ := regexp.MatchString("\\D", ordertime)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "orderTimeERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if ordertime == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "orderTimeERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if merid != "MC0123456789" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "merIdERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if orderstatus != "00" && orderstatus != "01" && orderstatus != "02" && orderstatus != "99" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "orderStatusERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	thekey := "merId=" + merid + "&merOrderNo=" + merorderno + "&orderAmt=" + orderamt + "&orderNo=" + orderno + "&orderStatus=" + orderstatus + "&orderTime=" + ordertime + "&payAmt=" + payamt + "&key=369d4621fddf90c2ee2b4f49"
	h := md5.New()
	io.WriteString(h, thekey)
	thesign := hex.EncodeToString(h.Sum(nil))
	thesign = strings.ToUpper(thesign)
	if thesign != sign {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: sign}, Message: "signERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	settileTime := time.Now().UTC().Format("2006-01-02 15:04:05")
	_ = DB.Table("payRecords_golden").Where("orderNo = ?", merorderno).Update("settleTime", settileTime).Error
	_ = DB.Table("payRecords_golden").Where("orderNo = ?", merorderno).Update("orderStage", 6).Error
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "SUCCESS"}
	ResponseWithJson(writer, http.StatusOK, response)
	if orderstatus != "00" {
		recieveTime := time.Now().UTC().Format("2006-01-02 15:04:05")
		finishTime := time.Now().UTC().Format("2006-01-02 15:04:05")
		_ = DB.Table("payRecords_golden").Where("orderNo = ?", merorderno).Update("recieveTime", recieveTime).Error
		_ = DB.Table("payRecords_golden").Where("orderNo = ?", merorderno).Update("finishTime", finishTime).Error
		_ = DB.Table("payRecords_golden").Where("orderNo = ?", merorderno).Update("orderStage", 8).Error
		_ = DB.Table("payRecords_golden").Where("orderNo = ?", merorderno).Update("orderStatus", 3).Error
	} else {
		recieveTime := time.Now().UTC().Format("2006-01-02 15:04:05")
		_ = DB.Table("payRecords_golden").Where("orderNo = ?", merorderno).Update("recieveTime", recieveTime).Error
		_ = DB.Table("payRecords_golden").Where("orderNo = ?", merorderno).Update("orderStage", 7).Error
		var userdata UserCoinRecoderDataType
		_ = DB.Table("payRecords_golden").Select("uid").Where("orderNo = ?", merorderno).Scan(&userdata).Error
		OKorError := RecordUserCoin(userdata.Uid, merorderno)
		if OKorError == true {
			finishTime := time.Now().UTC().Format("2006-01-02 15:04:05")
			_ = DB.Table("payRecords_golden").Where("orderNo = ?", merorderno).Update("finishTime", finishTime).Error
			_ = DB.Table("payRecords_golden").Where("orderNo = ?", merorderno).Update("orderStage", 8).Error
			_ = DB.Table("payRecords_golden").Where("orderNo = ?", merorderno).Update("orderStatus", 3).Error
		} else {
			_ = DB.Table("payRecords_golden").Where("orderNo = ?", merorderno).Update("orderStatus", -1).Error
		}
	}
}

func RecordUserCoin(userid int, orderno string) bool {
	cid, _ := strconv.Atoi(orderno[6:10])
	var paydata PayChannel
	err := DB.Table("payChannels A2").Select("A2.payType").Joins("JOIN users A1 ON A1.loginCountry = A2.countryCode").Where("A1.id = ? AND A2.id = ?", userid, cid).Scan(&paydata).Error
	if err != nil {
		return false
	}
	var platformdata Platformdatatype
	err = DB.Table("payChannels A2").Select("A3.code").Joins("JOIN users A1 ON A1.loginCountry = A2.countryCode").Joins("JOIN payPlatforms A3 ON A2.platformId = A3.id").Where("A1.id = ? AND A2.id = ?", userid, cid).Scan(&platformdata).Error
	if err != nil {
		return false
	}
	var recorddata payRecords_channelcode
	payrecord := "payRecords_" + platformdata.Code
	err = DB.Table(payrecord).Select("orderAction,currency,amount").Where("orderNo = ?", orderno).Scan(&recorddata).Error
	if err != nil {
		return false
	}
	currencypaytogetrate := 1
	if recorddata.OrderAction == 1001 {
		currencypaytogetrate *= 1
	} else if recorddata.OrderAction == 1002 {
		currencypaytogetrate *= -1
	}
	plusamount := int(recorddata.Amount) * currencypaytogetrate
	tx := DB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	if err := tx.Error; err != nil {
		return false
	}

	user := DB.Table("users")

	err = tx.Table("users").Set("gorm:query_option", "FOR UPDATE").First(&user, "id = ?", userid).Error
	if err != nil {
		tx.Rollback()
		return false
	}

	err = tx.Table("users").Where("id = ?", userid).Update("coin", gorm.Expr("coin + ?", plusamount)).Error
	if err != nil {
		tx.Rollback()
		return false
	}
	var usercoin theuser
	err = tx.Table("users").Select("coin").Where("id = ?", userid).Scan(&usercoin).Error
	if err != nil {
		return false
	}
	usercoin.Coin -= plusamount
	thetime := time.Now().UTC().Format("2006-01-02 15:04:05")
	usercoinrecoderdata := UserCoinRecoderDataType{
		Uid:       userid,
		Type:      paydata.PayType,
		Action:    recorddata.OrderAction,
		OrderNo:   orderno,
		Coin:      usercoin.Coin,
		Amount:    recorddata.Amount,
		AfterCoin: usercoin.Coin + plusamount,
		Time:      thetime,
	}
	err = tx.Table("userCoinRecords").Create(&usercoinrecoderdata).Error
	if err != nil {
		tx.Rollback()
		return false
	}
	err = tx.Commit().Error
	if err != nil {
		tx.Rollback()
		return false
	}

	return true
}

// QueryPayChannels
// GetApp QueryPayChannels
// @Summary QueryPayChannels
// @Description "QueryPayChannels"
// @Tags Payment
// @Content-Type application/json
// @Produce json
// @Param userId path int false "userId"
// @Success 200 {object} controller.PayChannel "OK"
// @Failure  201 {object} controller.PayChannel "ERROR"
// @Router /QueryPayInChannels [post]
func QueryPayChannel(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	channelname := getRespBodyJson.Get("channelName").MustString()

	var retjs []PayChannel

	err := DB.Table("payChannels").Where("channelName LIKE '%" + channelname + "%'").Scan(&retjs).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response) //回傳
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: retjs}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response) //回傳
}

// NewPayChannel
// GetApp NewPayChannel
// @Summary NewPayChannel
// @Description "NewPayChannel"
// @Tags Payment
// @Content-Type application/json
// @Produce json
// @Param channelName header string true "channelName"
// @Param payCode formData string true "payCode"
// @Param platformId formData int true "platformId"
// @Param payType formData int true "payType"
// @Param countryCode formData int true "countryCode"
// @Param currency formData int true "currency"
// @Param amounts formData string false "amounts"
// @Param note formData string false "note"
// @Param enable formData int false "enable"
// @Success 200 {object} controller.PayChannel "OK"
// @Failure  201 {object} controller.PayChannelError "ERROR"
// @Router /QueryPayInChannels [post]
func NewPayChannel(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	channelname := getRespBodyJson.Get("channelName").MustString()
	paycode := getRespBodyJson.Get("payCode").MustString()
	platformid := getRespBodyJson.Get("platformId").MustString()
	paytype := getRespBodyJson.Get("payType").MustString()
	countrycode := getRespBodyJson.Get("countryCode").MustString()
	currency := getRespBodyJson.Get("currency").MustString()
	amounts := getRespBodyJson.Get("amounts").MustString()
	note := getRespBodyJson.Get("note").MustString()
	enable := getRespBodyJson.Get("enable").MustString()
	pmid, err := strconv.Atoi(platformid)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "platformIdERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	pty, err := strconv.Atoi(paytype)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "payTypeERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	cocod, err := strconv.Atoi(countrycode)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "countryCodeERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	cur, err := strconv.Atoi(currency)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "currencyERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	amountcheckstrings := strings.Split(amounts, ",")
	for _, amountcheck := range amountcheckstrings {
		_, err := strconv.Atoi(amountcheck)
		if err != nil {
			response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "amountsERROR"}
			ResponseWithJson(writer, http.StatusOK, response)
			return
		}
	}
	//判斷enable是否照格式
	match, _ := regexp.MatchString("[^01]|[01]{2,}", enable)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "enableERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	ena, _ := strconv.Atoi(enable)
	newchannel := PayChannel{
		ChannelName: channelname,
		PayCode:     paycode,
		PlatformId:  pmid,
		PayType:     pty,
		CountryCode: cocod,
		Currency:    cur,
		Amounts:     amounts,
		Note:        note,
		CreateTime:  time.Now().UTC().Format("2006-01-02 15:04:05"),
		UpdateTime:  time.Now().UTC().Format("2006-01-02 15:04:05"),
		Enable:      ena,
	}
	err = DB.Table("payChannels").Create(&newchannel).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response) //回傳
}

// ModifyPayChannel
// GetApp ModifyPayChannel
// @Summary ModifyPayChannel
// @Description "ModifyPayChannel"
// @Tags Payment
// @Content-Type application/json
// @Produce json
// @Param id header int true "id"
// @Param channelName formData string true "channelName"
// @Param payCode formData string true "payCode"
// @Param platformId formData int true "platformId"
// @Param payType formData int true "payType"
// @Param countryCode formData int true "countryCode"
// @Param currency formData int true "currency"
// @Param amounts formData string false "amounts"
// @Param note formData string false "note"
// @Param enable formData int false "enable"
// @Success 200 {object} controller.PayChannel "OK"
// @Failure  201 {object} controller.PayChannelError "ERROR"
// @Router /QueryPayInChannels [post]
func ModifyPayChannel(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	id := getRespBodyJson.Get("id").MustString()
	channelname := getRespBodyJson.Get("channelName").MustString()
	paycode := getRespBodyJson.Get("payCode").MustString()
	platformid := getRespBodyJson.Get("platformId").MustString()
	paytype := getRespBodyJson.Get("payType").MustString()
	countrycode := getRespBodyJson.Get("countryCode").MustString()
	currency := getRespBodyJson.Get("currency").MustString()
	amounts := getRespBodyJson.Get("amounts").MustString()
	note := getRespBodyJson.Get("note").MustString()
	enable := getRespBodyJson.Get("enable").MustString()
	pmid, err := strconv.Atoi(platformid)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "platformIdERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	pty, err := strconv.Atoi(paytype)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "payTypeERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	cocod, err := strconv.Atoi(countrycode)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "countryCodeERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	cur, err := strconv.Atoi(currency)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "currencyERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	amountcheckstrings := strings.Split(amounts, ",")
	for _, amountcheck := range amountcheckstrings {
		_, err := strconv.Atoi(amountcheck)
		if err != nil {
			response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "amountsERROR"}
			ResponseWithJson(writer, http.StatusOK, response)
			return
		}
	}
	//判斷enable是否照格式
	match, err := regexp.MatchString("[^01]|[01]{2,}", enable)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "enableERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if enable == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "enableERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	ena, _ := strconv.Atoi(enable)

	err = DB.Table("payChannels").Where("id = ?", id).Updates(PayChannel{ChannelName: channelname, PayCode: paycode, PlatformId: pmid, PayType: pty, CountryCode: cocod, Currency: cur, Amounts: amounts, Note: note, UpdateTime: time.Now().UTC().Format("2006-01-02 15:04:05"), Enable: ena}).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response) //回傳
}

// QueryPayPlatform
// GetApp QueryPayPlatform
// @Summary QueryPayPlatform
// @Description "QueryPayPlatform"
// @Tags Payment
// @Content-Type application/json
// @Produce json
// @Param name path string false "name"
// @Success 200 {object} controller.Platformdatatype "OK"
// @Failure  201 {object} controller.PlatformdatatypeError "ERROR"
// @Router /QueryPayPlatform [post]
func QueryPayPlatform(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	name := getRespBodyJson.Get("name").MustString()

	var retjs []Platformdatatype

	err := DB.Table("payPlatforms").Where("name LIKE '%" + name + "%'").Scan(&retjs).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response) //回傳
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: retjs}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response) //回傳
}

// NewPayPlatform
// GetApp NewPayPlatform
// @Summary NewPayPlatform
// @Description "NewPayPlatform"
// @Tags Payment
// @Content-Type application/json
// @Produce json
// @Param name header string true "name"
// @Param code formData string true "code"
// @Param urlCall formData string true "urlCall"
// @Param urlQuery formData string true "urlQuery"
// @Param urlCallBack formData string true "urlCallBack"
// @Param note formData string false "note"
// @Param enable formData string false "enable"
// @Success 200 {object} controller.Platformdatatype "OK"
// @Failure  201 {object} controller.PlatformdatatypeError "ERROR"
// @Router /NewPayPlatform [post]
func NewPayPlatform(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	name := getRespBodyJson.Get("name").MustString()
	code := getRespBodyJson.Get("code").MustString()
	urlcall := getRespBodyJson.Get("urlCall").MustString()
	urlquery := getRespBodyJson.Get("urlQuery").MustString()
	urlcallback := getRespBodyJson.Get("urlCallBack").MustString()
	note := getRespBodyJson.Get("note").MustString()
	enable := getRespBodyJson.Get("enable").MustString()
	//判斷urlcall是否照格式
	match, _ := regexp.MatchString("https://(\\w+)", urlcall)
	if match != true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "urlCallERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷urlquery是否照格式
	match, _ = regexp.MatchString("https://(\\w+)", urlquery)
	if match != true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "urlQueryERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷urlcallback是否照格式
	match, _ = regexp.MatchString("https://(\\w+)", urlcallback)
	if match != true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "urlCallBackERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷enable是否照格式
	match, err := regexp.MatchString("[^01]|[01]{2,}", enable)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "enableERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	ena, _ := strconv.Atoi(enable)

	newplatform := Platformdatatype{
		Name:        name,
		Code:        code,
		UrlCall:     urlcall,
		UrlQuery:    urlquery,
		UrlCallBack: urlcallback,
		Note:        note,
		CreateTime:  time.Now().UTC().Format("2006-01-02 15:04:05"),
		Enable:      ena,
	}
	err = DB.Table("payPlatforms").Create(&newplatform).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response) //回傳
}

// ModifyPayPlatform
// GetApp ModifyPayPlatform
// @Summary ModifyPayPlatform
// @Description "ModifyPayPlatform"
// @Tags Payment
// @Content-Type application/json
// @Produce json
// @Param id header int true "id"
// @Param name formData string true "name"
// @Param code formData string true "code"
// @Param urlCall formData string true "urlCall"
// @Param urlQuery formData string true "urlQuery"
// @Param urlCallBack formData string true "urlCallBack"
// @Param note formData string false "note"
// @Param enable formData int false "enable"
// @Success 200 {object} controller.Platformdatatype "OK"
// @Failure  201 {object} controller.PlatformdatatypeError "ERROR"
// @Router /ModifyPayPlatform [post]
func ModifyPayPlatform(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	id := getRespBodyJson.Get("id").MustString()
	name := getRespBodyJson.Get("name").MustString()
	code := getRespBodyJson.Get("code").MustString()
	urlcall := getRespBodyJson.Get("urlCall").MustString()
	urlquery := getRespBodyJson.Get("urlQuery").MustString()
	urlcallback := getRespBodyJson.Get("urlCallBack").MustString()
	note := getRespBodyJson.Get("note").MustString()
	enable := getRespBodyJson.Get("enable").MustString()
	//判斷urlcall是否照格式
	match, _ := regexp.MatchString("https://(\\w+)", urlcall)
	if match != true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "urlCallERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷urlquery是否照格式
	match, _ = regexp.MatchString("https://(\\w+)", urlquery)
	if match != true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "urlQueryERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷urlcallback是否照格式
	match, _ = regexp.MatchString("https://(\\w+)", urlcallback)
	if match != true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "urlCallBackERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷enable是否照格式
	match, _ = regexp.MatchString("[^01]|[01]{2,}", enable)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "enableERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if enable == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "enableERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	ena, _ := strconv.Atoi(enable)

	err := DB.Table("payPlatforms").Where("id = ?", id).Updates(Platformdatatype{Name: name, Code: code, UrlCall: urlcall, UrlQuery: urlquery, UrlCallBack: urlcallback, Note: note, UpdateTime: time.Now().UTC().Format("2006-01-02 15:04:05"), Enable: ena})
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response) //回傳
}

// QueryPayOutAccount
// GetApp QueryPayOutAccount
// @Summary QueryPayOutAccount
// @Description "QueryPayOutAccount"
// @Tags Payment
// @Content-Type application/json
// @Produce json
// @Param userId path int true "userId"
// @Success 200 {object} controller.BankAccountType "OK"
// @Failure  201 {object} controller.BankAccountTypeError "ERROR"
// @Router /QueryPayOutAccount [post]
func QueryPayOutAccount(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	userid := getRespBodyJson.Get("userId").MustString()

	var bankaccounts []BankAccountType

	err := DB.Table("userBankAccounts").Where("userId = ?", userid).Scan(&bankaccounts).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: bankaccounts}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response) //回傳
}

// NewPayOutAccount
// GetApp NewPayOutAccount
// @Summary NewPayOutAccount
// @Description "NewPayOutAccount"
// @Tags Payment
// @Content-Type application/json
// @Produce json
// @Param userId header int true "userId"
// @Param accountBank formData string true "accountBank"
// @Param accountName formData string true "accountName"
// @Param account formData string true "account"
// @Param default formData string false "default"
// @Success 200 {object} controller.BankAccountType "OK"
// @Failure  201 {object} controller.BankAccountTypeError "ERROR"
// @Router /NewPayOutAccount [post]
func NewPayOutAccount(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	userid := getRespBodyJson.Get("userId").MustString()
	accountbank := getRespBodyJson.Get("accountBank").MustString()
	accountname := getRespBodyJson.Get("accountName").MustString()
	account := getRespBodyJson.Get("account").MustString()
	defaultt := getRespBodyJson.Get("default").MustString()
	uid, err := strconv.Atoi(userid)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "userIDERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷enable是否照格式
	match, _ := regexp.MatchString("[^01]|[01]{2,}", defaultt)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "enableERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if defaultt == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "enableERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	def, _ := strconv.Atoi(defaultt)
	newaccount := BankAccountType{
		UserId:      uid,
		AccountBank: accountbank,
		AccountName: accountname,
		Account:     account,
		Default:     def,
	}
	err = DB.Table("userBankAccounts").Create(&newaccount).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response) //回傳
}

// ModifyPayOutAccount
// GetApp ModifyPayOutAccount
// @Summary ModifyPayOutAccount
// @Description "ModifyPayOutAccount"
// @Tags Payment
// @Content-Type application/json
// @Produce json
// @Param id header int true "id"
// @Param userId formData int true "userId"
// @Param accountBank formData string true "accountBank"
// @Param accountName formData string true "accountName"
// @Param account formData string true "account"
// @Param default formData string false "default"
// @Success 200 {object} controller.BankAccountType "OK"
// @Failure  201 {object} controller.BankAccountTypeError "ERROR"
// @Router /ModifyPayOutAccount [post]
func ModifyPayOutAccount(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	id := getRespBodyJson.Get("id").MustString()
	userid := getRespBodyJson.Get("userId").MustString()
	accountbank := getRespBodyJson.Get("accountBank").MustString()
	accountname := getRespBodyJson.Get("accountName").MustString()
	account := getRespBodyJson.Get("account").MustString()
	defaultt := getRespBodyJson.Get("default").MustString()
	uid, err := strconv.Atoi(userid)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "userIDERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷enable是否照格式
	match, _ := regexp.MatchString("[^01]|[01]{2,}", defaultt)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "enableERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if defaultt == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "enableERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	def, _ := strconv.Atoi(defaultt)
	err = DB.Table("userBankAccounts").Where("id = ?", id).Updates(BankAccountType{UserId: uid, AccountBank: accountbank, AccountName: accountname, Account: account, Default: def}).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response) //回傳
}

// DeletePayOutAccount
// GetApp DeletePayOutAccount
// @Summary DeletePayOutAccount
// @Description "DeletePayOutAccount"
// @Tags Payment
// @Content-Type application/json
// @Produce json
// @Param id header int true "id"
// @Success 200 string string "OK"
// @Failure  201 string string "ERROR"
// @Router /DeletePayOutAccount [post]
func DeletePayOutAccount(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	id := getRespBodyJson.Get("id").MustString()
	idi, err := strconv.Atoi(id)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "userIDERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	err = DB.Table("userBankAccounts").Where("id = ?", idi).Update("enable", 0).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response) //回傳
}

// QueryPayOutChannel
// GetApp QueryPayOutChannel
// @Summary QueryPayOutChannel
// @Description "QueryPayOutChannel"
// @Tags Payment
// @Content-Type application/json
// @Produce json
// @Param userId path int true "userId"
// @Success 200 {object} controller.PayChannel "OK"
// @Failure  201 {object} controller.PayChannelError "ERROR"
// @Router /QueryPayOutChannel [post]
func QueryPayOutChannel(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	userid := getRespBodyJson.Get("userId").MustString()
	var payoutchannels []PayChannel

	err := DB.Table("payChannels A2").Select("A2.id,A2.channelName,A2.payCode,A2.platformId,A2.payType,A2.countryCode,A2.currency,A2.amounts,A2.note,A2.createTime,A2.updateTime,A2.enable").Joins("JOIN users A1 ON A1.loginCountry = A2.countryCode").Where("A1.id = ?", userid).Scan(&payoutchannels).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: payoutchannels}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}

// CallPayOut
// GetApp CallPayOut
// @Summary CallPayOut
// @Description "出金下單"
// @Tags Payment
// @Content-Type application/json
// @Produce json
// @Param userId header int true "userId"
// @Param amounts formData float64 true "amounts"
// @Param accountId formData int true "accountId"
// @Success 200 string string "OK"
// @Failure  201 string string "ERROR"
// @Router /CallPayOut [post]
func CallPayOut(writer http.ResponseWriter, request *http.Request) {
	createTime := time.Now().UTC().Format("2006-01-02 15:04:05")
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	thecreatedata, _ := json.Marshal(getRespBodyJson)
	userid := getRespBodyJson.Get("userId").MustString()
	amounts := getRespBodyJson.Get("amounts").MustString()
	accountid := getRespBodyJson.Get("accountId").MustString()
	uid, err := strconv.Atoi(userid)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "userIdERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	var usercheck userq
	_ = DB.Table("users").Select("id,coin").Where("id = " + userid).Scan(&usercheck).Error
	if usercheck.Id != uid {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "userIdERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	checkbetamount := payWithdrawCheck(uid)
	if checkbetamount == false {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "betAmountFail"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	amt, err := strconv.ParseFloat(amounts, 64)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "amountsERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if amounts == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "amountsERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if usercheck.Coin < int(amt) {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "Your coin doesn't enough!"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	amounts = strconv.FormatFloat(amt, 'f', 2, 64)
	acc, err := strconv.Atoi(accountid)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "accountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	var accountcheck BankAccountType
	_ = DB.Table("userBankAccounts").Where("id = ?", accountid).Scan(&accountcheck).Error
	if accountcheck.Id != acc {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "accountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if accountcheck.UserId != uid {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "accountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	timeutc := strings.Replace(time.Now().UTC().Format("060102150405.000"), ".", "", 1)
	plateformid := 2001
	var channeldata PayChannel
	err = DB.Table("payChannels A2").Select("A2.id,A2.payCode,A2.payType,A2.currency").Joins("JOIN users A1 ON A1.loginCountry = A2.countryCode").Joins("JOIN payPlatforms A3 ON A2.platformId = A3.id").Where("A1.id = ? AND A3.id = ?", uid, plateformid).Scan(&channeldata).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "channelERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	var plateformdata Platformdatatype
	err = DB.Table("payPlatforms").Select("code,urlCall,urlCallBack").Where("id = ?", plateformid).Scan(&plateformdata).Error
	payrecord := "payRecords_" + plateformdata.Code
	theUserId := fmt.Sprintf("%06d", uid)
	theChannelId := fmt.Sprintf("%04d", channeldata.Id)
	var orderno ordernotype
	orderno.OrderNo = theUserId + theChannelId + timeutc
	fmt.Println(orderno.OrderNo)
	var keyset = map[string]string{
		"appId":      easyAppId,
		"appOrderNo": orderno.OrderNo,
		"orderAmt":   amounts,
		"payId":      "401",
		"accNo":      accountcheck.Account,
		"accName":    accountcheck.AccountName,
		"bankName":   accountcheck.AccountBank,
	}
	thekeyset, _ := json.Marshal(keyset)
	thekey := sortthekey(thekeyset)
	thekey += "&key=" + easyKey
	fmt.Println(thekey)

	payrecordcode := payoutRecords_channelcode{
		OrderNo:     orderno.OrderNo,
		Uid:         uid,
		AccountBank: accountcheck.AccountBank,
		AccountName: accountcheck.AccountName,
		Account:     accountcheck.Account,
		OrderAction: channeldata.PayType,
		Currency:    channeldata.Currency,
		Amount:      amt,
		CreateTime:  createTime,
		CreateData:  string(thecreatedata),
		OrderStage:  1,
		OrderStatus: 0,
	}
	err = DB.Table(payrecord).Create(&payrecordcode).Error
	h := md5.New()
	io.WriteString(h, thekey)
	thesign := hex.EncodeToString(h.Sum(nil))
	thesign = strings.ToUpper(thesign)
	fmt.Println(thesign)
	data := payoutpostdatatype{
		AppId:      easyAppId,
		AppOrderNo: orderno.OrderNo,
		OrderAmt:   amt,
		PayId:      "401",
		AccNo:      accountcheck.Account,
		AccName:    accountcheck.AccountName,
		BankName:   accountcheck.AccountBank,
		NotifyURL:  plateformdata.UrlCallBack,
		Sign:       thesign,
	}
	jsondata, _ := json.Marshal(data)
	resp, err := http.Post(plateformdata.UrlCall, "application/json", bytes.NewBuffer(jsondata))
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "respERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	calloutTime := time.Now().UTC().Format("2006-01-02 15:04:05.000")
	err = DB.Table(payrecord).Where("orderNo = ?", payrecordcode.OrderNo).Update("calloutTime", calloutTime).Error
	err = DB.Table(payrecord).Where("orderNo = ?", payrecordcode.OrderNo).Update("calloutData", string(jsondata)).Error
	err = DB.Table(payrecord).Where("orderNo = ?", payrecordcode.OrderNo).Update("orderStage", 2).Error
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "bodyERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	paygetRespBodyJson, _ := simplejson.NewJson(body)
	thereturndata, _ := json.Marshal(paygetRespBodyJson)
	code := paygetRespBodyJson.Get("code").MustString()
	//thedata := paygetRespBodyJson.Get("data").MustString()
	msg := paygetRespBodyJson.Get("msg").MustString()
	returnTime := time.Now().UTC().Format("2006-01-02 15:04:05")
	err = DB.Table(payrecord).Where("orderNo = ?", payrecordcode.OrderNo).Update("returnTime", returnTime).Error
	err = DB.Table(payrecord).Where("orderNo = ?", payrecordcode.OrderNo).Update("returnData", string(thereturndata)).Error
	err = DB.Table(payrecord).Where("orderNo = ?", payrecordcode.OrderNo).Update("orderStage", 3).Error
	if code != "0000" {
		response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: code}, Message: msg}
		ResponseWithJson(writer, http.StatusOK, response)
		paynotifyTime := time.Now().UTC().Format("2006-01-02 15:04:05")
		err = DB.Table(payrecord).Where("orderNo = ?", payrecordcode.OrderNo).Update("paynotifyTime", paynotifyTime).Error
		err = DB.Table(payrecord).Where("orderNo = ?", payrecordcode.OrderNo).Update("orderStage", 4).Error
		err = DB.Table(payrecord).Where("orderNo = ?", payrecordcode.OrderNo).Update("orderStatus", 1).Error
		return
	}
	OKorError := RecordUserCoin(uid, orderno.OrderNo)
	if OKorError == false {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: code}, Message: "RecordERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	paynotifyTime := time.Now().UTC().Format("2006-01-02 15:04:05")
	err = DB.Table(payrecord).Where("orderNo = ?", payrecordcode.OrderNo).Update("paynotifyTime", paynotifyTime).Error
	err = DB.Table(payrecord).Where("orderNo = ?", payrecordcode.OrderNo).Update("orderStage", 4).Error
	err = DB.Table(payrecord).Where("orderNo = ?", payrecordcode.OrderNo).Update("orderStatus", 1).Error
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: code}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}

// CallBackEasy
// GetApp CallBackEasy
// @Summary CallBackEasy
// @Description "出金回調"
// @Tags Payment
// @Content-Type application/json
// @Produce json
// @Param appOrderNo header string true "appOrderNo"
// @Param orderNo formData int true "orderNo"
// @Param orderTime formData string true "orderTime"
// @Param appId formData string true "appId"
// @Param orderAmt formData float64 true "orderAmt"
// @Param orderFee formData float64 true "orderFee"
// @Param orderStatus formData string true "orderStatus"
// @Param sign formData string true "sign"
// @Success 200 string string "SUCCESS"
// @Failure  201 string string "ERROR"
// @Router /CallBackEasy [post]
func CallBackEasy(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	thecallbackdata, _ := json.Marshal(getRespBodyJson)
	apporderno := getRespBodyJson.Get("appOrderNo").MustString()
	orderno := getRespBodyJson.Get("orderNo").MustString()
	ordertime := getRespBodyJson.Get("orderTime").MustString()
	appid := getRespBodyJson.Get("appId").MustString()
	orderamt := getRespBodyJson.Get("orderAmt").MustString()
	orderfee := getRespBodyJson.Get("orderFee").MustString()
	orderstatus := getRespBodyJson.Get("orderStatus").MustString()
	sign := getRespBodyJson.Get("sign").MustString()
	var payrecordcheck payRecords_channelcode
	_ = DB.Table("payRecords_easy").Select("orderNo,orderStatus").Where("orderNo = ?", apporderno).Scan(&payrecordcheck)
	if payrecordcheck.OrderNo == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "appOrderNoERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if payrecordcheck.OrderStatus >= 3 {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "The order is over"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	callbackTime := time.Now().UTC().Format("2006-01-02 15:04:05")
	_ = DB.Table("payRecords_easy").Where("orderNo = ?", apporderno).Update("callbackTime", callbackTime).Error
	_ = DB.Table("payRecords_easy").Where("orderNo = ?", apporderno).Update("callbackData", string(thecallbackdata)).Error
	_ = DB.Table("payRecords_easy").Where("orderNo = ?", apporderno).Update("orderStage", 5).Error
	_ = DB.Table("payRecords_easy").Where("orderNo = ?", apporderno).Update("orderStatus", 2).Error
	oamt, err := strconv.ParseFloat(orderamt, 64)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "orderAmtERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	_ = DB.Table("payRecords_easy").Select("amount").Where("orderNo = ?", apporderno).Scan(&payrecordcheck).Error
	if oamt != payrecordcheck.Amount {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "orderAmtERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	fee, err := strconv.ParseFloat(orderfee, 64)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "payAmtERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if fee < 0 {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "payAmt smaller than 0"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if orderno == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "orderNoERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	match, _ := regexp.MatchString("\\D", ordertime)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "orderTimeERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if ordertime == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "orderTimeERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if appid != "987654321" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "merIdERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if orderstatus != "00" && orderstatus != "01" && orderstatus != "02" && orderstatus != "99" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "orderStatusERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	thekey := "appId=" + appid + "&appOrderNo=" + apporderno + "&orderAmt=" + orderamt + "&orderfee=" + orderfee + "&orderNo=" + orderno + "&orderStatus=" + orderstatus + "&orderTime=" + ordertime + "&key=369d4621fddf90c2ee2b4f49"
	h := md5.New()
	io.WriteString(h, thekey)
	thesign := hex.EncodeToString(h.Sum(nil))
	thesign = strings.ToUpper(thesign)
	if thesign != sign {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: sign}, Message: "signERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	settileTime := time.Now().UTC().Format("2006-01-02 15:04:05")
	_ = DB.Table("payRecords_easy").Where("orderNo = ?", apporderno).Update("settleTime", settileTime).Error
	_ = DB.Table("payRecords_easy").Where("orderNo = ?", apporderno).Update("orderStage", 6).Error
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "SUCCESS"}
	ResponseWithJson(writer, http.StatusOK, response)
	recieveTime := time.Now().UTC().Format("2006-01-02 15:04:05")
	finishTime := time.Now().UTC().Format("2006-01-02 15:04:05")
	_ = DB.Table("payRecords_easy").Where("orderNo = ?", apporderno).Update("recieveTime", recieveTime).Error
	_ = DB.Table("payRecords_easy").Where("orderNo = ?", apporderno).Update("finishTime", finishTime).Error
	_ = DB.Table("payRecords_easy").Where("orderNo = ?", apporderno).Update("orderStage", 8).Error
	_ = DB.Table("payRecords_easy").Where("orderNo = ?", apporderno).Update("orderStatus", 3).Error
}

// AgencyPayOutOrder
// GetApp AgencyPayOutOrder
// @Summary AgencyPayOutOrder
// @Description "AgencyPayOutOrder"
// @Tags Payment
// @Content-Type application/json
// @Produce json
// @Param appId header int true "appId"
// @Param apporderno formData string true "apporderno"
// @Param sign formData string true "sign"
// @Success 200 string string "0000"
// @Failure  201 string string "ERROR"
// @Router /AgencyPayOutOrder [post]
func AgencyPayOutOrder(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	appid := getRespBodyJson.Get("appId").MustString()
	apporderno := getRespBodyJson.Get("appOrderNo").MustString()
	sign := getRespBodyJson.Get("sign").MustString()
	if appid != "987654321" {
		response := AgencyPayOutOrderRecall{Code: "X", Msg: "appIdError"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	var ordercheck payoutRecords_channelcode
	err := DB.Table("payRecords_easy").Where("orderNo = ?", apporderno).Scan(&ordercheck)
	if err != nil {
		response := AgencyPayOutOrderRecall{Code: "X", Msg: "appOrderNoError"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if ordercheck.OrderNo != apporderno {
		response := AgencyPayOutOrderRecall{Code: "X", Msg: "appOrderNo doesn't exisit"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	thekey := "appId=987654321&merOrderNo=" + apporderno + "&key=369d4621fddf90c2ee2b4f49"
	h := md5.New()
	io.WriteString(h, thekey)
	thesign := hex.EncodeToString(h.Sum(nil))
	thesign = strings.ToUpper(thesign)
	if thesign != sign {
		response := AgencyPayOutOrderRecall{Code: "X", Msg: "signError"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := AgencyPayOutOrderRecall{Code: "0000", Msg: "", OrderNo: ordercheck.OrderNo, AccNo: ordercheck.Account, Amt: strconv.FormatFloat(ordercheck.Amount, 'f', 2, 64)}
	ResponseWithJson(writer, http.StatusOK, response)
	return
}

// QueryPayWithdrawRules
// GetApp QueryPayWithdrawRules
// @Summary QueryPayWithdrawRules
// @Description "QueryPayWithdrawRules"
// @Tags Payment
// @Content-Type application/json
// @Produce json
// @Param country path int true "countrycode"
// @Success 200 {object} controller.userPayChannel "OK"
// @Failure  201 {object} controller.userPayChannel "ERROR"
// @Router /QueryPayWithdrawRules [post]
func QueryPayWithdrawRules(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	countrycode := getRespBodyJson.Get("countryCode").MustString()

	var paywithdrawrules []PayWithdrawRuleType
	err := DB.Table("payWithdrawRules").Where("countryCode = ?", countrycode).Scan(&paywithdrawrules).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: paywithdrawrules}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}

// NewPayWithdrawRules
// GetApp NewPayWithdrawRules
// @Summary NewPayWithdrawRules
// @Description "NewPayWithdrawRules"
// @Tags Payment
// @Content-Type application/json
// @Produce json
// @Param country header int true "countrycode"
// @Param betamount formData int true "betamount"
// @Param duration formData int ture "duration"
// @Param note formData string false "note"
// @Param enable formData int false "enable"
// @Success 200 {object} controller.PayWithdrawRuleType "OK"
// @Failure  201 {object} controller.PayWithdrawRuleTypeError "ERROR"
// @Router /NewPayWithdrawRules [post]
func NewPayWithdrawRules(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	countrycode := getRespBodyJson.Get("countryCode").MustString()
	betamount := getRespBodyJson.Get("betAmount").MustString()
	duration := getRespBodyJson.Get("duration").MustString()
	note := getRespBodyJson.Get("note").MustString()
	enable := getRespBodyJson.Get("enable").MustString()
	coco, err := strconv.Atoi(countrycode)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "countryCodeERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	bet, err := strconv.Atoi(betamount)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "betAmountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	dur, err := strconv.Atoi(duration)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "durationERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷enable是否照格式
	match, _ := regexp.MatchString("[^01]|[01]{2,}", enable)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "enableERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if enable == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "enableERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	ena, _ := strconv.Atoi(enable)

	newdata := PayWithdrawRuleType{
		CountryCode: coco,
		BetAmount:   bet,
		Duration:    dur,
		Note:        note,
		CreateTime:  time.Now().UTC().Format("2006-01-02 15:04:05"),
		Enable:      ena,
	}
	err = DB.Table("payWithdrawRules").Create(&newdata).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}

// ModifyPayWithdrawRules
// GetApp ModifyPayWithdrawRules
// @Summary ModifyPayWithdrawRules
// @Description "ModifyPayWithdrawRules"
// @Tags Payment
// @Content-Type application/json
// @Produce json
// @Param country header int true "countrycode"
// @Param betamount formData int true "betamount"
// @Param duration formData int ture "duration"
// @Param note formData string false "note"
// @Param enable formData int false "enable"
// @Success 200 {object} controller.PayWithdrawRuleType "OK"
// @Failure  201 {object} controller.PayWithdrawRuleTypeError "ERROR"
// @Router /ModifyPayWithdrawRules [post]
func ModifyPayWithdrawRules(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	countrycode := getRespBodyJson.Get("countryCode").MustString()
	betamount := getRespBodyJson.Get("betAmount").MustString()
	duration := getRespBodyJson.Get("duration").MustString()
	note := getRespBodyJson.Get("note").MustString()
	enable := getRespBodyJson.Get("enable").MustString()
	coco, err := strconv.Atoi(countrycode)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "countryCodeERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	bet, err := strconv.Atoi(betamount)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "betAmountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	dur, err := strconv.Atoi(duration)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "durationERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷enable是否照格式
	match, _ := regexp.MatchString("[^01]|[01]{2,}", enable)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "enableERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if enable == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "enableERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	ena, _ := strconv.Atoi(enable)

	err = DB.Table("payWithdrawRules").Where("countryCode = ?", coco).Updates(PayWithdrawRuleType{BetAmount: bet, Duration: dur, Note: note, UpdateTime: time.Now().UTC().Format("2006-01-02 15:04:05"), Enable: ena}).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}

func payWithdrawCheck(uid int) bool {
	var usercountry userq
	_ = DB.Table("users").Select("id,loginCountry").Where("id = ?", uid).Scan(&usercountry).Error
	if usercountry.Id == 0 {
		return false
	}
	var paywithdrawrule PayWithdrawRuleType
	_ = DB.Table("payWithdrawRules").Where("countryCode = ?", usercountry.LoginCountry).Scan(&paywithdrawrule).Error
	t := time.Now().UTC().Add(time.Hour * 24 * -1 * time.Duration(paywithdrawrule.Duration)).Format("2006-01-02 15:04:05")
	var result []UserCoinRecoderDataType
	var betamountsum float64
	_ = DB.Table("userCoinRecords").Where("uid = ? AND action = ? AND time > ?", uid, 1001, t).Scan(&result)
	for _, v := range result {
		betamountsum += v.Amount
	}
	fmt.Println(betamountsum)
	if int(betamountsum) < paywithdrawrule.BetAmount {
		return false
	}
	return true
}
func sortthekey(input []byte) string {

	var data map[string]interface{}
	err := json.Unmarshal(input, &data)
	if err != nil {
		return ""
	}
	keys := make([]string, 0, len(data))
	for k := range data {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	var params []string
	for _, v := range keys {
		params = append(params, fmt.Sprintf("%v=%v", v, data[v]))
	}

	query := strings.Join(params, "&")

	fmt.Println(query)
	return query
}
