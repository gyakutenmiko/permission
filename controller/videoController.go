package controller

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func VideoViewCounts(w http.ResponseWriter, r *http.Request) {

	var reqInfo VideoViewsTest

	err := json.NewDecoder(r.Body).Decode(&reqInfo)

	if err != nil {

		responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqFailCode, Data: nil, Msg: "something error!"})

		// 寫入回應內容
		_, err := w.Write(responseJSON)
		if err != nil {
			return
		}

	} else {

		// req info
		id := reqInfo.Id
		videoId := reqInfo.VideoId

		// redis 判斷邏輯
		err := AddViewsCount(id, videoId)

		if err != nil {
			// err	判斷
			errMsg := fmt.Sprintf("%s", err)

			// 重複請求 忽略
			if errMsg == "01" {

				responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqFailCode, Data: nil, Msg: "request so quick from same video id! count +0!"})

				// 寫入回應內容
				_, err := w.Write(responseJSON)
				if err != nil {
					return
				}
			} else {

				responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqFailCode, Data: nil, Msg: "something error!"})

				// 寫入回應內容
				_, err := w.Write(responseJSON)
				if err != nil {
					return
				}
			}

		} else {

			// 產生response

			// 產生response
			responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqSuccessCode, Data: nil, Msg: ""})

			// 寫入回應內容
			_, err := w.Write(responseJSON)
			if err != nil {
				return
			}

		}

	}
}

func VideoPlayCounts(w http.ResponseWriter, r *http.Request) {

	var reqInfo VideoViewsTest

	err := json.NewDecoder(r.Body).Decode(&reqInfo)

	if err != nil {

		responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqFailCode, Data: nil, Msg: "something error!"})

		// 寫入回應內容
		_, err := w.Write(responseJSON)
		if err != nil {
			return
		}

	} else {

		// req info
		id := reqInfo.Id
		videoId := reqInfo.VideoId

		// redis 判斷邏輯
		err := AddPlayCount(id, videoId)

		if err != nil {
			// err	判斷
			errMsg := fmt.Sprintf("%s", err)

			// 重複請求 忽略
			if errMsg == "01" {

				responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqFailCode, Data: nil, Msg: "request so quick from same video id! count +0!"})

				// 寫入回應內容
				_, err := w.Write(responseJSON)
				if err != nil {
					return
				}
			} else {

				responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqFailCode, Data: nil, Msg: "something error!"})

				// 寫入回應內容
				_, err := w.Write(responseJSON)
				if err != nil {
					return
				}
			}

		} else {

			// 產生response
			responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqSuccessCode, Data: nil, Msg: ""})

			// 寫入回應內容
			_, err := w.Write(responseJSON)
			if err != nil {
				return
			}

		}

	}
}
