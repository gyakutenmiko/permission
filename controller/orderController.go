package controller

import (
	"fmt"

	//"go/types"
	"io/ioutil"
	"net/http"
	"strconv"

	//"strings"
	"strings"
	"time"

	"sync"

	"github.com/bitly/go-simplejson"
)

func QueryOrder(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	userid := getRespBodyJson.Get("userId").MustString()
	uid, err := strconv.Atoi(userid)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "userIdERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	var orders []userOrderType
	err = DB.Table("userOrders").Where("userId = ?", uid).Scan(&orders).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: orders}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}
func UserOrder(writer http.ResponseWriter, request *http.Request) {
	m := new(sync.RWMutex)
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	userid := getRespBodyJson.Get("userId").MustString()
	productid := getRespBodyJson.Get("productId").MustString()
	uid, err := strconv.Atoi(userid)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "userIdERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	pid, err := strconv.Atoi(productid)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "productIdERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	m.RLock()
	var checkuser theuser
	err = DB.Table("users").Where("id = ?", uid).Scan(&checkuser).Error
	if err != nil {
		m.RUnlock()
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "userIdERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if checkuser.Account == "" {
		m.RUnlock()
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR:user doesn't exist"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	var checkproduct productType
	err = DB.Table("products").Where("id = ?", pid).Scan(&checkproduct).Error
	if err != nil {
		m.RUnlock()
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "productIdERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	amount := checkproduct.Price
	ordertime := time.Now().UTC()
	ordertimedate := ordertime.Format("2006-01-02 15:04:05")
	neworder := userOrderType{
		UserId:    uid,
		ProductId: pid,
		Amount:    amount,
		Time:      ordertimedate,
		Status:    0,
	}
	err = DB.Table("userOrders").Create(&neworder).Error
	if err != nil {
		m.RUnlock()
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	coin := checkuser.Coin
	if coin < amount {
		m.RUnlock()
		_ = DB.Table("userOrders").Where("userId = ? AND productId = ? AND time = ?", uid, pid, ordertimedate).Update("status", 2).Error
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR:coin doesn't enough"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	aftercoin := coin - amount
	_ = DB.Table("userOrders").Where("userId = ? AND productId = ? AND time = ?", uid, pid, ordertimedate).Update("status", 1)
	theUserId := fmt.Sprintf("%06d", uid)
	timeutc := strings.Replace(time.Now().UTC().Format("060102150405.000"), ".", "", 1)
	orderno := theUserId + "2001" + timeutc
	newcoinrecord := UserCoinRecoderDataType{
		Uid:       uid,
		Type:      2001,
		Action:    2001,
		OrderNo:   orderno,
		Coin:      coin,
		Amount:    float64(amount),
		AfterCoin: aftercoin,
		Time:      ordertimedate,
	}
	_ = DB.Table("userCoinRecords").Create(&newcoinrecord)
	_ = DB.Table("users").Where("id = ?", uid).Update("coin", aftercoin)
	expiretime := ordertime.Add(time.Hour * 24 * time.Duration(checkproduct.WatchDuration))
	expiretimedate := expiretime.Format("2006-01-02 15:04:05")
	newvideorecord := videoAuthRecordType{
		UserId:     uid,
		ProductId:  pid,
		OrderTime:  ordertimedate,
		ExpireTime: expiretimedate,
		Status:     1,
	}
	_ = DB.Table("videoAuthRecords").Create(&newvideorecord)
	m.RUnlock()
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response) //回傳
}

func QueryProduct(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	name := getRespBodyJson.Get("name").MustString()
	var products []productType
	err := DB.Table("products").Where("name LIKE '%" + name + "%'").Scan(&products).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: products}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}

func NewProduct(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	name := getRespBodyJson.Get("name").MustString()
	desc := getRespBodyJson.Get("desc").MustString()
	price := getRespBodyJson.Get("price").MustString()
	watchduration := getRespBodyJson.Get("watchDuration").MustString()
	priceint, err := strconv.Atoi(price)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "priceERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	durationday, err := strconv.Atoi(watchduration)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "priceERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	newproduct := productType{
		Name:          name,
		Desc:          desc,
		Price:         priceint,
		WatchDuration: durationday,
	}
	err = DB.Table("products").Create(&newproduct).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}

func ModifyProduct(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	id := getRespBodyJson.Get("id").MustString()
	name := getRespBodyJson.Get("name").MustString()
	desc := getRespBodyJson.Get("desc").MustString()
	price := getRespBodyJson.Get("price").MustString()
	watchduration := getRespBodyJson.Get("watchDuration").MustString()
	priceint, err := strconv.Atoi(price)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "priceERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	durationday, err := strconv.Atoi(watchduration)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "watchdurationERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	err = DB.Table("products").Where("id = " + id).Updates(productType{Name: name, Desc: desc, Price: priceint, WatchDuration: durationday}).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}
