package controller

import (
	"encoding/json"
	"errors"
	"fmt"
	"math/rand"
	"regexp"
	"strconv"
	"time"
	"unicode"

	//"github.com/forgoer/openssl"

	"crypto/sha256"
	"database/sql"
	"io"
	"io/ioutil"

	"github.com/bitly/go-simplejson"

	_ "github.com/go-sql-driver/mysql" //avoid golang mark mysql

	"log"
	"net/http"
	"net/smtp"
)

func ResponseWithJson(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	_, err := w.Write(response)
	if err != nil {
		return
	}
}

var DBGlobal *sql.DB

func init() {
	DBGlobal = newDBClient()
}
func newDBClient() *sql.DB {
	dbConnect, err := sql.Open(
		"mysql", // 因為只有這裡才用到這個`引數`，並沒有直接使用到了mysql.XXX 相關的函式或物件，會被認為沒有用到mysql這個依賴而 被go編譯器省略import
		"cmssa:Cms1q2w(O)P@tcp(43.198.7.55)/cms",
	)
	if err != nil {
		log.Fatalln(err)
	}

	err = dbConnect.Ping() //Ping() connect to DB
	if err != nil {
		log.Fatalln(err)
	}
	return dbConnect
}
func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

var AlphanumericSet = []rune{
	'0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
	'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
	'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
}

// UserQuery
// GetApp UserQuery
// @Summary UserQuery
// @Description "UserQuery"
// @Tags User
// @Content-Type application/json
// @Produce json
// @Param account header string false "account"
// @Param nickname header string false "nickname"
// @Success 200 {object} controller.userq "OK"
// @Failure  201 {object} controller.userq "ERROR"
// @Router /UserQuery [post]
func UserQuery(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	account := getRespBodyJson.Get("account").MustString()
	nickname := getRespBodyJson.Get("nickName").MustString()

	var retjs []userq

	err := DB.Table("users").Select("id,account,nickName,email,phone,createDatetime,IFNULL(icon,'') as icon,IFNULL(userStory,'') as userStory,coin,vipType,authStatus,IFNULL(loginDatetime,'') as loginDatetime,loginDayscount,loginCountry,IFNULL(birthday,'') as birthday,country,sex,language,roleId").Where("account LIKE '%" + account + "%' AND nickName LIKE '%" + nickname + "%'").Scan(&retjs).Error
	if err != nil {
		log.Println(err)
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response) //回傳
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: retjs}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response) //回傳
}

// UserNew
// GetApp UserNew
// @Summary UserNew
// @Description "AddNewUsers"
// @Tags User
// @Content-Type application/json
// @Produce json
// @Param account header string true "account"
// @Param nickname header string true "nickname"
// @Param email header string true "email"
// @Param pwd header string true "pwd"
// @Param phone header string true "phone"
// @Param loginCountry header int true "loginCountry"
// @Param country header int true "country"
// @Param language header int true "language"
// @Param roleId header int true "roleId"
// @Success 200 {object} controller.theuser "OK"
// @Failure  201 {object} controller.theuserError "ERROR"
// @Router /UserNew [post]
func UserNew(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	account := getRespBodyJson.Get("account").MustString()
	nickname := getRespBodyJson.Get("nickName").MustString()
	email := getRespBodyJson.Get("email").MustString()
	pwd := getRespBodyJson.Get("pwd").MustString()
	phone := getRespBodyJson.Get("phone").MustString()
	logincountry := getRespBodyJson.Get("loginCountry").MustString()
	country := getRespBodyJson.Get("country").MustString()
	language := getRespBodyJson.Get("language").MustString()
	roleid := getRespBodyJson.Get("roleId").MustString()
	lg, err := strconv.Atoi(logincountry)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "loginCountryERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	co, err := strconv.Atoi(country)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "countryERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	la, err := strconv.Atoi(language)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "languageERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	ri, err := strconv.Atoi(roleid)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "roleIdERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	timeutc := time.Now().UTC().Format("2006-01-02 15:04:05")

	//判斷account是否含特殊符號
	match, _ := regexp.MatchString("\\W", account)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "accountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if account == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "accountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷nick_name是否含特殊符號
	match, _ = regexp.MatchString("\\W", nickname)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "nickNameERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if nickname == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "nickNameERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷email是否照格式
	match, _ = regexp.MatchString("(\\w)[@](\\w+)(.com)", email)
	if match != true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "emailERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷password是否照格式
	err = VerifyPasswordRule(pwd, 6, 14)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "passwordERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷phone是否含數字以外的符號
	match, _ = regexp.MatchString("\\D", phone)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "phoneERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if phone == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "phoneERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}

	last := passwordhash(account, pwd)

	user := theuser{
		Account:        account,
		NickName:       nickname,
		Email:          email,
		Pwd:            last,
		Phone:          phone,
		CreateDatetime: timeutc,
		AuthStatus:     2,
		LoginCountry:   lg,
		Country:        co,
		Language:       la,
		RoleId:         ri,
	}
	if err = DB.Table("users").Create(&user).Error; err != nil {
		log.Println(err, "INSERT users SET account=?,nickName=?,email=?,pwd=?,phone=?,createDatetime=utc_timestamp,loginCountry=?,country=?,language=?,roleId=?", account, nickname, email, last, phone, logincountry, country, language, roleid)
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}

func RegisterEmailCheck(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	account := getRespBodyJson.Get("account").MustString()
	email := getRespBodyJson.Get("email").MustString()
	phone := getRespBodyJson.Get("phone").MustString()
	registertime := time.Now().Local()
	losttime := registertime.Add(time.Minute * 3)

	//判斷account是否含特殊符號
	match, _ := regexp.MatchString("\\W", account)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "accountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if account == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "accountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷email是否照格式
	match, _ = regexp.MatchString("(\\w)[@](\\w+)(.com)", email)
	if match != true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "emailERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷phone是否含數字以外的符號
	match, _ = regexp.MatchString("\\D", phone)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "phoneERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if phone == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "phoneERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}

	type accounttype struct {
		Account string
	}
	var accountcheck accounttype

	err := DB.Table("users").Select("account").Where("email = ? AND phone = ?", email, phone).Scan(&accountcheck).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "accountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if accountcheck.Account != account {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "accountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	code := registerHash(account, email, phone)
	err = redisClient.Set(account, code, time.Minute*3).Err()
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	send("anry@richclub.live", "<html><body><h1>"+account+"客戶您好:<br> 由於您於"+registertime.Format("2006/01/02 15:04")+"在CMS使用註冊功能,基於安全理由,註冊碼將於<br>"+losttime.Format("2006/01/02 15:04")+"失效,以下是您的註冊連結<br>http://172.0.0.1:30030/RegisterCheck?account="+account+"&code="+code+"<br>若有任何問題,歡迎至CMS客戶中心詢問或直接撥打專線02-xxx-xxx-xx會有專人為您服務</h1></body></html>")
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}
func registerHash(account string, email string, phone string) string {

	h := sha256.New()
	io.WriteString(h, phone)
	pho5 := fmt.Sprintf("%x", h.Sum(nil))
	salt1 := "PoMu0301"
	salt2 := "SeTu0808"
	salt3 := "NiJiGaKu"
	io.WriteString(h, salt1)
	io.WriteString(h, account)
	io.WriteString(h, salt2)
	io.WriteString(h, pho5)
	io.WriteString(h, salt3)
	io.WriteString(h, email)

	return fmt.Sprintf("%x", h.Sum(nil))
}
func RegisterCheck(writer http.ResponseWriter, request *http.Request) {
	account := request.URL.Query().Get("account")
	code := request.URL.Query().Get("code")
	var codecheck string
	type accounttype struct {
		Account string
	}
	var accountcheck accounttype

	err := DB.Table("users").Select("account").Where("account = ?", account).Scan(&accountcheck).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "accountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if accountcheck.Account != account {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "accountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	codecheck, _ = redisClient.Get(account).Result()
	if codecheck != code {
		remail := RestartRegisterMail(account)
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "codeERROR remail " + strconv.FormatBool(remail)}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	err = DB.Table("users").Where("account = ?", account).Update("authStatus", 1).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}
func RestartRegisterMail(account string) bool {
	registertime := time.Now().Local()
	losttime := registertime.Add(time.Minute * 3)
	type accounttype struct {
		Account string
		Email   string
		Phone   string
	}
	var accountcheck accounttype
	err := DB.Table("users").Select("account,email,phone").Where("account = ?", account).Scan(&accountcheck).Error
	if err != nil {
		return false
	}
	if accountcheck.Account != account {
		return false
	}
	code := registerHash(account, accountcheck.Email, accountcheck.Phone)
	err = redisClient.Set(account, code, time.Minute*3).Err()
	if err != nil {
		return false
	}
	send("anry@richclub.live", "<html><body><h1>"+account+"客戶您好:<br> 由於您於"+registertime.Format("2006/01/02 15:04")+"在CMS使用註冊功能,基於安全理由,註冊碼將於<br>"+losttime.Format("2006/01/02 15:04")+"失效,以下是您的註冊連結<br>http://172.0.0.1:30030/RegisterCheck?account="+account+"&code="+code+"<br>若有任何問題,歡迎至CMS客戶中心詢問或直接撥打專線02-xxx-xxx-xx會有專人為您服務</h1></body></html>")
	return true
}

// UserModify
// GetApp UserModify
// @Summary UserModify
// @Description "ModifyUser"
// @Tags User
// @Content-Type application/json
// @Produce json
// @Param id header int true "id"
// @Param nickname header string true "nickname"
// @Param email header string true "email"
// @Param pwd header string true "pwd"
// @Param phone header string true "phone"
// @Param icon header string false "icon"
// @Param user_story header string false "user_story"
// @Param coin header int true "coin"
// @Param vip_type header int true "vip_type"
// @Param role_id header int true "role_id"
// @Param auth_status header int true "auth_status"
// @Param country header int true "country"
// @Param sex header int true "sex"
// @Param language header int true "language"
// @Success 200 {object} controller.theuser "OK"
// @Failure  201 {object} controller.theuserError "ERROR"
// @Router /UserModify [post]
func UserModify(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	id := getRespBodyJson.Get("id").MustString()
	nickname := getRespBodyJson.Get("nickName").MustString()
	email := getRespBodyJson.Get("email").MustString()
	pwdold := getRespBodyJson.Get("pwdold").MustString()
	pwd := getRespBodyJson.Get("pwd").MustString()
	phone := getRespBodyJson.Get("phone").MustString()
	icon := getRespBodyJson.Get("icon").MustString()
	userstory := getRespBodyJson.Get("userStory").MustString()
	coin := getRespBodyJson.Get("coin").MustString()
	viptype := getRespBodyJson.Get("vipType").MustString()
	authstatus := getRespBodyJson.Get("authStatus").MustString()
	country := getRespBodyJson.Get("country").MustString()
	sex := getRespBodyJson.Get("sex").MustString()
	language := getRespBodyJson.Get("language").MustString()
	coi, err := strconv.Atoi(coin)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "coinERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	vip, err := strconv.Atoi(viptype)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "vipTypeERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	aut, err := strconv.Atoi(authstatus)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "authStatusERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	cou, err := strconv.Atoi(country)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "countryERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	se, err := strconv.Atoi(sex)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "sexERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	la, err := strconv.Atoi(language)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "languageERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷nick_name是否含特殊符號
	match, _ := regexp.MatchString("\\W", nickname)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "nickNameERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if nickname == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "nickNameERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷email是否照格式
	match, _ = regexp.MatchString("(\\w)[@](\\w+)(.com)", email)
	if match != true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "emailERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷phone是否含數字以外的符號
	match, _ = regexp.MatchString("\\D", phone)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "phoneERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if phone == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "phoneERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷icon是否照格式
	match, _ = regexp.MatchString("https://(\\w+)", icon)
	if match != true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "iconERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷vip_type是否照格式
	match, _ = regexp.MatchString("[^01]|[01]{2,}", viptype)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "vipTypeERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if viptype == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "vipTypeERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷auth_status是否照格式
	match, _ = regexp.MatchString("[^0-2]|[0-2]{2,}", authstatus)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "authStatusERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if authstatus == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "authStatusERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷sex是否照格式
	match, _ = regexp.MatchString("[^0-2]|[0-2]{2,}", sex)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "sexERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if sex == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "sexERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}

	//判斷是否打算更新密碼
	if pwdold != "" {
		//判斷password是否照格式
		err = VerifyPasswordRule(pwd, 6, 14)
		if err != nil {
			response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "passwordERROR"}
			ResponseWithJson(writer, http.StatusOK, response)
			return
		}

		type accounttype struct {
			Account string
		}
		var account accounttype
		err = DB.Table("users").Select("account").Where("id = ?", id).Scan(&account).Error
		if err != nil {
			response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "idERROR"}
			ResponseWithJson(writer, http.StatusOK, response)
			return
		}
		if account.Account == "" {
			response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "idERROR"}
			ResponseWithJson(writer, http.StatusOK, response)
			return
		}

		lastpasswordold := passwordhash(account.Account, pwdold)

		var accountcheck accounttype
		err = DB.Table("users").Select("account").Where("pwd = ?", lastpasswordold).Scan(&accountcheck).Error
		if err != nil {
			response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "oldPasswordERROR"}
			ResponseWithJson(writer, http.StatusOK, response)
			return
		}
		if accountcheck.Account == "" {
			response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "oldPasswordERROR"}
			ResponseWithJson(writer, http.StatusOK, response)
			return
		}

		lastpassword := passwordhash(account.Account, pwd)

		if err = DB.Table("users").Where("id = ?", id).Updates(theuser{NickName: nickname, Email: email, Pwd: lastpassword, Phone: phone, Icon: &icon, UserStory: &userstory, Coin: coi, VipType: vip, AuthStatus: aut, Country: cou, Sex: se, Language: la}).Error; err != nil {
			log.Println("UPDATE users SET nickName=?,email=?,pwd=?,phone=?,icon=?,userStory=?,coin=?,vipType=?,authStatus=?,country=?,sex=?,language=? WHERE id =?", nickname, email, pwd, phone, icon, userstory, coin, viptype, authstatus, country, sex, language, id)
			response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
			ResponseWithJson(writer, http.StatusOK, response)
			return
		}
		response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//若沒打算更密則依序更新其他
	if err = DB.Table("users").Where("id = ?", id).Updates(theuser{NickName: nickname, Email: email, Phone: phone, Icon: &icon, UserStory: &userstory, Coin: coi, VipType: vip, AuthStatus: aut, Country: cou, Sex: se, Language: la}).Error; err != nil {
		log.Println("UPDATE users SET nickName=?,email=?,phone=?,icon=?,userStory=?,coin=?,vipType=?,authStatus=?,country=?,sex=?,language=? WHERE id =?", nickname, email, phone, icon, userstory, coin, viptype, authstatus, country, sex, language, id)
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}

func VerifyPasswordRule(str string, minLen, maxLen int) error {
	var (
		isUpper   = false
		isLower   = false
		isNumber  = false
		isSpecial = false
	)

	if len(str) < minLen || len(str) > maxLen {
		return errors.New("The password must contain uppercase and lowercase letters, numbers or punctuation, and must be " + strconv.Itoa(minLen) + "-" + strconv.Itoa(maxLen) + " digits long. ")
	}

	for _, s := range str {
		switch {
		case unicode.IsUpper(s):
			isUpper = true
		case unicode.IsLower(s):
			isLower = true
		case unicode.IsNumber(s):
			isNumber = true
		case unicode.IsPunct(s) || unicode.IsSymbol(s):
			isSpecial = true
		default:
		}
	}

	if (isUpper && isLower) && (isNumber || isSpecial) {
		return nil
	}
	return errors.New("The password must contain uppercase and lowercase letters, numbers or punctuation, and must be " + strconv.Itoa(minLen) + "-" + strconv.Itoa(maxLen) + " digits long. ")
}
func passwordhash(account string, password string) string {

	h := sha256.New()
	io.WriteString(h, password)
	pwmd5 := fmt.Sprintf("%x", h.Sum(nil))
	salt1 := "PoMu0301"
	salt2 := "SeTu0808"
	io.WriteString(h, salt1)
	io.WriteString(h, account)
	io.WriteString(h, salt2)
	io.WriteString(h, pwmd5)

	return fmt.Sprintf("%x", h.Sum(nil))
}
func EmailCheck(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	account := getRespBodyJson.Get("account").MustString()
	email := getRespBodyJson.Get("email").MustString()
	forgettime := time.Now().Local()
	losttime := forgettime.Add(time.Minute * 3)

	//判斷account是否含特殊符號
	match, _ := regexp.MatchString("\\W", account)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "accountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if account == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "accountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	//判斷email是否照格式
	match, _ = regexp.MatchString("(\\w)[@](\\w+)(.com)", email)
	if match != true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "emailERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	type accounttype struct {
		Account string
	}
	var accountcheck accounttype

	err := DB.Table("users").Select("account").Where("email = ?", email).Scan(&accountcheck).Error
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: err.Error(), Info: ""}, Message: "emailERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if accountcheck.Account != account {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: err.Error(), Info: ""}, Message: "emailERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	authcode := GetAuthCode(6)
	authcoderune := []rune(authcode)
	err = redisClient.Set(account, authcode, time.Minute*3).Err()
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: err.Error(), Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	send("anry@richclub.live", "<html><body><h1>"+account+"客戶您好:<br> 由於您於"+forgettime.Format("2006/01/02 15:04")+"在CMS使用忘記密碼功能,基於安全理由,驗證碼將於<br>"+losttime.Format("2006/01/02 15:04")+"失效,以下是您的驗證碼<br><P style='display: inline-block; border-radius: 12px; background: #73AD21; width: 60px; height: 60px; font-size: 45px; text-align: center; margin: 5px; '>"+string(authcoderune[0])+"</P><P style='display: inline-block; border-radius: 12px; background: #73AD21; width: 60px; height: 60px; font-size: 45px; text-align: center; margin: 5px; '>"+string(authcoderune[1])+"</P><P style='display: inline-block; border-radius: 12px; background: #73AD21; width: 60px; height: 60px; font-size: 45px; text-align: center; margin: 5px; '>"+string(authcoderune[2])+"</P><P style='display: inline-block; border-radius: 12px; background: #73AD21; width: 60px; height: 60px; font-size: 45px; text-align: center; margin: 5px; '>"+string(authcoderune[3])+"</P><P style='display: inline-block; border-radius: 12px; background: #73AD21; width: 60px; height: 60px; font-size: 45px; text-align: center; margin: 5px; '>"+string(authcoderune[4])+"</P><P style='display: inline-block; border-radius: 12px; background: #73AD21; width: 60px; height: 60px; font-size: 45px; text-align: center; margin: 5px; '>"+string(authcoderune[5])+"</P><br>若有任何問題,歡迎至CMS客戶中心詢問或直接撥打專線02-xxx-xxx-xx會有專人為您服務</h1></body></html>")
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "" + email, Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}
func send(email string, body string) {
	from := "anry@richclub.live"
	pass := "anry0619!"
	to := email
	subject := "Subject: Test email from Go!\n"
	mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	/*msg := "From: " + from + "\n" +
	"To: " + to + "\n" +
	"Subject: Get AuthCode\n\n" +
	body*/
	msg := []byte(subject + mime + body)

	err := smtp.SendMail("smtp.gmail.com:587",
		smtp.PlainAuth("", from, pass, "smtp.gmail.com"),
		from, []string{to}, []byte(msg))

	if err != nil {
		fmt.Printf("smtp error: %s", err)
		return
	}

	fmt.Print("sent, visit " + email)
}

// / GetInvCodeByUID 獲取指定長度的邀請碼
func GetAuthCode(l int) string {
	seed := time.Now().UnixNano()
	r := rand.New(rand.NewSource(seed))
	var code []rune
	for i := 0; i < l; i++ {
		idx := r.Intn(len(AlphanumericSet))
		code = append(code, AlphanumericSet[idx])
	}
	return string(code)
}

func AuthcodePasswordCheck(writer http.ResponseWriter, request *http.Request) {
	getRespBody, _ := ioutil.ReadAll(request.Body)
	getRespBodyJson, _ := simplejson.NewJson(getRespBody)
	account := getRespBodyJson.Get("account").MustString()
	authcode := getRespBodyJson.Get("authcode").MustString()
	pwd := getRespBodyJson.Get("pwd").MustString()

	//判斷account是否含特殊符號
	match, _ := regexp.MatchString("\\W", account)
	if match == true {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "accountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	if account == "" {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "accountERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	var authcodecheck string
	authcodecheck, _ = redisClient.Get(account).Result()
	if authcodecheck != authcode {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "authcodeERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	err := VerifyPasswordRule(pwd, 6, 14)
	if err != nil {
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "passwordERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}

	lastpassword := passwordhash(account, pwd)

	if err = DB.Table("users").Where("account = ?", account).Update("pwd", lastpassword).Error; err != nil {
		log.Println("UPDATE users SET pwd=? WHERE account =?", pwd, account)
		response := ApiResponseCorrect{ResultCode: ApiReqFailCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "ERROR"}
		ResponseWithJson(writer, http.StatusOK, response)
		return
	}
	response := ApiResponseCorrect{ResultCode: ApiReqSuccessCode, ResultMessage: DataInfo{Code: 0, Msg: "", Info: ""}, Message: "OK"}
	ResponseWithJson(writer, http.StatusOK, response)
}
