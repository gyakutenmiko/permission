package controller

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
)

const (
	// ApiReqSuccessCode Api Response Status Code
	ApiReqSuccessCode = 200
	ApiReqFailCode    = 201
)

// JwtAuth middleware 確認 Jwt key
func JwtAuth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		// get token from request
		reqToken := r.Header.Get("Authorization")
		splitToken := strings.Split(reqToken, "Bearer ")
		tokenStr := splitToken[1]

		// 測試用token string 直接略過驗證
		if tokenStr == "api_test" {
			next.ServeHTTP(w, r)

		} else {

			if tokenStr == "" {

				responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqFailCode, Data: nil, Msg: "Missing Token!"})

				// 寫入回應內容
				_, err := w.Write(responseJSON)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				return

			} else {

				// 解析 token 內容
				token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {

					// 確認 token 加密方式
					if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
						return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
					}

					// 確認 token 簽名
					return []byte("CMSeyJhbGciOi"), nil
				})

				// 驗證簽名失敗
				if err != nil {
					responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqFailCode, Data: nil, Msg: "Illegal token key!"})

					// 寫入回應內容
					_, err := w.Write(responseJSON)
					if err != nil {
						http.Error(w, err.Error(), http.StatusInternalServerError)
						return
					}
					return
				}

				// 確認token有效
				if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
					exp := int64(claims["exp"].(float64))

					// 有效時間判斷
					if time.Now().Unix() > exp {

						responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqFailCode, Data: nil, Msg: "Token expired!"})

						// 寫入回應內容
						_, err := w.Write(responseJSON)
						if err != nil {
							http.Error(w, err.Error(), http.StatusInternalServerError)
							return
						}
						return
					}
				}
				next.ServeHTTP(w, r)
			}

		}
	})
}

// JSONContentTypeMiddleware middleware 確認 req格式是json
func JSONContentTypeMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		contentType := r.Header.Get("Content-Type")

		if contentType != "application/json" {

			// 產生response
			responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqFailCode, Data: nil, Msg: "Request type error!"})

			// 寫入回應內容
			_, err := w.Write(responseJSON)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

			return
		} else {
			next.ServeHTTP(w, r)
		}

	})
}

// LoggMakerMiddleware log 生成
func LoggMakerMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		lrw := &loggingResponseWriter{ResponseWriter: w, buf: &bytes.Buffer{}}

		// 忽略 swagger ui 生成html req
		if r.URL.Path == "/index.html" ||
			r.URL.Path == "/swagger-ui.css" ||
			r.URL.Path == "/swagger-ui-bundle.js" ||
			r.URL.Path == "/swagger-ui-standalone-preset.js" ||
			r.URL.Path == "/doc.json" ||
			r.URL.Path == "/favicon-32x32.png" {
			next.ServeHTTP(lrw, r)

		} else {

			// do
			next.ServeHTTP(lrw, r)

			// 紀錄response data
			resp := lrw.buf.String()

			jsonStruct := ApiResInit{}
			err := json.Unmarshal([]byte(resp), &jsonStruct)
			if err != nil {
				return
			}

			var requestBody interface{}

			//	空request判斷
			if r.Body != nil {
				err := json.NewDecoder(r.Body).Decode(&requestBody)
				if err != nil {
					// handle error
				}
			}
			// 寫入db
			DB.Create(&Logs{
				RequestIp:              r.RemoteAddr,
				RequestBody:            &requestBody,
				ResponseBody:           resp,
				Route:                  r.URL.Path,
				ResponseStatus:         jsonStruct.Ret,
				RequestDeliverDatetime: time.Now(),
			})

		}
	})
}

// 紀錄response 內容
func (lrw *loggingResponseWriter) Write(b []byte) (int, error) {
	lrw.buf.Write(b)
	return lrw.ResponseWriter.Write(b)
}

// SetResHeaderJson 將所有res header.Content-Type 設為 application/json
func SetResHeaderJson(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		next.ServeHTTP(w, r)
	})
}
