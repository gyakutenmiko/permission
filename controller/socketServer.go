package controller

import (
	"fmt"
	f "github.com/ambelovsky/gosf"
)

const (
	socketPort = 30031
)

func init() {
	// Listen on an endpoint
	f.OnConnect(func(client *f.Client, request *f.Request) {
		//log.Println("Client connected")
	})

	f.OnDisconnect(func(client *f.Client, request *f.Request) {
		//log.Println("Client disconnected")
	})

	f.Listen("join room", joinRoom)

	f.Listen("leave room", leaveRoom)

	// 向所有監聽“event_1”端點的客戶端廣播消息
	f.Listen("event1", allClientEvent1)

	// 向特定房間中收聽“event_2”端點的所有客戶端廣播消息：只要在room內 有listen "event_2"就會收到
	f.Listen("event2Room", event2Room)

	// 將響應廣播給房間“cast”中的所有客戶端，請求客戶端除外
	f.Listen("cast", cast)
}

func joinRoom(c *f.Client, r *f.Request) *f.Message {

	room := r.Message.Body["room"].(string)

	c.Join(room)
	c.Join("cast")

	// Construct response
	response := new(f.Message)
	response.Success = true
	response.Text = "join room: " + room + " success!"

	// Send response back to the client
	return response
}

func leaveRoom(c *f.Client, r *f.Request) *f.Message {
	room := r.Message.Body["room"].(string)
	c.Leave(room)
	c.Leave("cast")

	return nil
}

// 向所有監聽“event_1”端點的客戶端廣播消息
func allClientEvent1(_ *f.Client, r *f.Request) *f.Message {

	text := r.Message.Body["welcomeStr"]

	response := new(f.Message)
	response.Success = true
	response.Text = text.(string)

	f.Broadcast("", "event_1", response)

	return response
}

// 向特定房間中收聽“event_2”端點的所有客戶端廣播消息
func event2Room(_ *f.Client, r *f.Request) *f.Message {

	room := r.Message.Body["room"].(string)
	fmt.Println("向特定房間 " + room + " 中收聽 event_2 端點的所有客戶端廣播消息")

	response2 := new(f.Message)
	response2.Success = true
	response2.Text = "Room:test, emit:test"

	f.Broadcast(room, "event_2", response2)

	return response2
}

// 將響應廣播給房間“cast”中的所有客戶端，請求客戶端除外
func cast(c *f.Client, r *f.Request) *f.Message {

	room := r.Message.Body["room"].(string)

	response := new(f.Message)
	response.Success = true
	response.Text = "Broadcasts to one room: " + room + " !!"

	c.Broadcast("cast", "cast board", response)

	return response
}

func SocketServer() {
	// Start the server using a basic configuration
	f.Startup(map[string]interface{}{"port": socketPort})

}
