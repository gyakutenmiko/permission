package controller

import (
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/go-redis/redis"
)

var redisClient *redis.Client

func InitClient() (err error) {

	redisClient = redis.NewClient(&redis.Options{

		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	_, err = redisClient.Ping().Result()

	if err != nil {
		return err
	}
	return nil
}

// WriteTokenInRedis 將token寫入redis
func WriteTokenInRedis(id string, token string) error {
	var user UserInsertRedis

	user.Id = id
	user.Token = token

	err := redisClient.Set(user.Id, user.Token, 0).Err()
	if err != nil {
		return err
	}
	return nil
}

// AddViewsCount 接收id 寫入video/series 以及 score 用來判斷需不需要更新 validViews
func AddViewsCount(id int, videoId int) error {

	/*
		1.redis sort list 功能：可將element 賦予一個數值(score)
		2.利用請求者ＩＤ＋目標影片ＩＤ 生成 redis key
		3.score值為 請求時間點(timestamp)＋60秒後
		4.每次發起請求時 利用 redis.ZRemRangeByScore 清除 score 值 小於or等於現在的 key
		5.接著 確認這次請求資料 （步驟2）有無存在於redis內
			有> 時效未過 不+1
			無> 時效已過 這次請求 對於count數 +1
	*/

	var user VideoViews

	// request info
	user.Id = id
	user.VideoId = videoId

	// redis prefix
	redisPrefix := "_validView"

	// 生成 id_validView
	redisKey := fmt.Sprintf("%d%s", user.Id, redisPrefix)
	fmt.Println(redisKey)

	// now time stamp
	validTime := int(time.Now().Unix()) + 60
	nowTime := int(time.Now().Unix())
	fmt.Println("有效時間", validTime)
	fmt.Println("現在時間", nowTime)

	// str now 轉換為 int64
	strNow := strconv.FormatFloat(float64(nowTime), 'f', -1, 64)

	// 用 strNow 來移除key內過期資料
	redisClient.ZRemRangeByScore(redisKey, "-inf", strNow)

	// 判斷 videoId 是否存在於 key內
	redisCheck, _ := redisClient.ZScore(redisKey, strconv.Itoa(user.VideoId)).Result()

	// 判斷該影片ＩＤ是否存在於redis set
	if int(redisCheck) == 0.0 {

		fmt.Println("===== Insert redis! =====")

		// 不存在 寫入 現在時間+ 60秒
		redisClient.ZAdd(redisKey, redis.Z{Score: float64(validTime), Member: user.VideoId})

		// 影片views +1
		err := VideoViewsCountAdd(videoId)
		if err != nil {
			return err
		}
		fmt.Println("count data +1 !")

		return nil

	} else {
		// 存在 寫入 現在時間+ 60秒
		fmt.Println("video 尚未過期！")
		str := "1"
		return errors.New(str)
	}

}

// AddPlayCount 接收id 寫入video/series 以及 score 用來判斷需不需要更新 validPlay
func AddPlayCount(id int, videoId int) error {

	/*
		1.redis sort list 功能：可將element 賦予一個數值(score)
		2.利用請求者ＩＤ＋目標影片ＩＤ 生成 redis key
		3.score值為 請求時間點(timestamp)＋60秒後
		4.每次發起請求時 利用 redis.ZRemRangeByScore 清除 score值小於or等於現在的 key
		5.接著 確認這次請求資料 （步驟2）有無存在於redis內
			有> 時效未過 不+1
			無> 時效已過 這次請求 對於count數 +1
	*/

	var user VideoViews

	// request info
	user.Id = id
	user.VideoId = videoId

	// redis prefix
	redisPrefix := "_validPlay"
	// 生成 id_validView 用來當作redis key
	redisKey := fmt.Sprintf("%d%s", user.Id, redisPrefix)
	fmt.Println(redisKey)

	// now time stamp
	validTime := int(time.Now().Unix()) + 60
	nowTime := int(time.Now().Unix())
	fmt.Println("有效時間", validTime)
	fmt.Println("現在時間", nowTime)

	// str now 轉換為 int64
	strNow := strconv.FormatFloat(float64(nowTime), 'f', -1, 64)

	// 用 strNow 來移除key內過期資料
	redisClient.ZRemRangeByScore(redisKey, "-inf", strNow)

	// 判斷 videoId 是否存在於 key內
	redisCheck, _ := redisClient.ZScore(redisKey, strconv.Itoa(user.VideoId)).Result()

	// 判斷該影片ＩＤ是否存在於redis set
	if int(redisCheck) == 0.0 {

		fmt.Println("===== Insert redis! =====")

		// 不存在 寫入 現在時間+ 60秒
		redisClient.ZAdd(redisKey, redis.Z{Score: float64(validTime), Member: user.VideoId})

		// 影片views +1
		err := VideoPlayCountAdd(videoId)
		if err != nil {
			return err
		}
		fmt.Println("count data +1 !")

		return nil

	} else {
		// 存在 寫入 現在時間+ 60秒
		fmt.Println("video 尚未過期！")
		str := "1"
		return errors.New(str)
	}

}
