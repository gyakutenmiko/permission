package controller

import (
	"fmt"
)

/*

todo: server上 DB要改為AdminDB DB是localhost
需要的table

1.homepageAnnounceTag 首頁上方標籤功能欄位
2.homepageBannerTag 首頁內公告標籤
3.classId 首頁船內公告類別
4.logs  操作記錄功能
5.newsCard 首頁傳內公告卡片資訊
6.seriesEpisodeInfo 影集內容
7.videoInfo 影片內容
8.users 使用者資料

*/

func TableCheck() {

	CheckUsers()
	CheckHomePageAnnounceTag()
	CheckHomepageBannerTags()
	CheckNewsCardColumn()
	CheckClassText()
	CheckSeriesEpisodeInfo()
	CheckLogs()
	CheckVideoInfo()
	CheckPayRecordsGolden()
	CheckPayPlatforms()
	CheckPayChannels()
}

// CheckUsers 確認 cms.Users是否存在 存在的話 寫入一筆假資料 不存在的話 建立該table後寫入假資料
func CheckUsers() {

	// 判斷table 是否存在
	// 無：建立table
	// 有：跳過
	if !DB.HasTable(&Users{}) {
		fmt.Println("查無table:users...建立中!")
		DB.AutoMigrate(&Users{})
	}
	// table內資料存在確認
	var usersTableDataCount int

	DB.Model(&Users{}).Unscoped().Count(&usersTableDataCount)

	// db.users 無資料 生成一筆假資料
	if usersTableDataCount == 0 {
		// pwd 為 dev_account hash後的結果 直接輸入
		DB.Create(&Users{
			Account:  "dev_account",
			Pwd:      "496519deb5e9043b7f66d436ff68224dd563b5271be2f29863a06420d3221d29",
			NickName: "dev_1",
			Icon:     "www.dev.com.tw",
			Coin:     "500000",
			VipType:  "1",
			Language: "886"})

		fmt.Println("測試資料帳號密碼均為'dev_account'...")
	}
}

// CheckHomePageAnnounceTag 確認 cms.homepageAnnounceTag
func CheckHomePageAnnounceTag() {

	if !DB.HasTable(&HomePageAnnounceTag{}) {
		fmt.Println("查無table:HomePageAnnounceTag...建立中!")
		DB.AutoMigrate(&HomePageAnnounceTag{})
		fmt.Println("HomePageAnnounceTag...已建立!")
	} else {
		fmt.Println("HomePageAnnounceTag...已存在!")
	}
}

// CheckHomepageBannerTags 確認 cms.CheckHomepageBannerTags
func CheckHomepageBannerTags() {

	if !DB.HasTable(&HomepageBannerTags{}) {
		fmt.Println("查無table:HomepageBannerTags...建立中!")
		DB.AutoMigrate(&HomepageBannerTags{})
		fmt.Println("HomepageBannerTags...已建立!")
	} else {
		fmt.Println("HomepageBannerTags...已存在!")
	}
}

// CheckNewsCardColumn 確認 cms.NewsCardColumn
func CheckNewsCardColumn() {

	if !DB.HasTable(&NewsCardColumn{}) {
		fmt.Println("查無table:NewsCardColumn...建立中!")
		DB.AutoMigrate(&NewsCardColumn{})
		fmt.Println("NewsCardColumn...已建立!")
	} else {
		fmt.Println("NewsCardColumn...已存在!")
	}
}

// CheckClassText 確認 cms.ClassText
func CheckClassText() {

	if !DB.HasTable(&ClassText{}) {
		fmt.Println("查無table:ClassText...建立中!")
		DB.AutoMigrate(&ClassText{})
		fmt.Println("ClassText...已建立!")
	} else {
		fmt.Println("ClassText...已存在!")
	}
}

// CheckSeriesEpisodeInfo 確認 cms.ClassText
func CheckSeriesEpisodeInfo() {

	if !DB.HasTable(&ClassText{}) {
		fmt.Println("查無table:SeriesEpisodeInfo...建立中!")
		DB.AutoMigrate(&ClassText{})
		fmt.Println("SeriesEpisodeInfo...已建立!")
	} else {
		fmt.Println("SeriesEpisodeInfo...已存在!")
	}
}

// CheckLogs 確認 cms.ClassText
func CheckLogs() {

	if !DB.HasTable(&Logs{}) {
		fmt.Println("查無table:logs...建立中!")
		DB.AutoMigrate(&Logs{})
		fmt.Println("logs...已建立!")
	} else {
		fmt.Println("logs...已存在!")
	}
}

// CheckVideoInfo 確認 cms.payRecords_golden  payRecords_golden
func CheckVideoInfo() {

	if !DB.HasTable(&VideoInfoColumn{}) {
		fmt.Println("查無table:payRecords_golden...建立中!")
		DB.AutoMigrate(&VideoInfoColumn{})
		fmt.Println("payRecords_golden...已建立!")
	} else {
		fmt.Println("payRecords_golden...已存在!")
	}
}

// CheckPayRecordsGolden 確認 cms.ClassText
func CheckPayRecordsGolden() {

	if !DB.HasTable(&PayRecordsGolden{}) {
		fmt.Println("查無table:ClassText...建立中!")
		DB.AutoMigrate(&PayRecordsGolden{})
		fmt.Println("ClassText...已建立!")
	} else {
		fmt.Println("ClassText...已存在!")
	}
}

// CheckPayPlatforms 確認 cms.pyPlatforms
func CheckPayPlatforms() {

	if !DB.HasTable(&PayPlatforms{}) {
		fmt.Println("查無table:pyPlatforms...建立中!")
		DB.AutoMigrate(&PayPlatforms{})
		fmt.Println("pyPlatforms...已建立!")
	} else {
		fmt.Println("pyPlatforms...已存在!")
	}
}

// CheckPayChannels 確認 cms.payChannels
func CheckPayChannels() {

	if !DB.HasTable(&PayChannels{}) {
		fmt.Println("查無table:payChannels...建立中!")
		DB.AutoMigrate(&PayChannels{})
		fmt.Println("payChannels...已建立!")
	} else {
		fmt.Println("payChannels...已存在!")
	}
}
