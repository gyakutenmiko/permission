package controller

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// GetHomePageTab 上方功能欄標籤
// GetApp godoc
//	@Summary		拿到首頁上方藍色區塊標籤資料
//	@Description	拿到首頁上方藍色區塊標籤資料
//	@Description 	== Response DATA ==
//	@Description	1.response.data:jsons in list
//	@Description	== json:announceTagInfo ==
//	@Description	1.displayName:string, tag info text.
//	@Description	2.enable:int, only for backend.
//	@Description	3.hyperLink:string, hyper link string.
//	@Description	4.permissionLevel:int, check for different client roles.
//	@Description	5.tips:string, only for backend.
//	@Description	== json:index ==
//	@Description	1.int, for sort data.
//	@Tags			HomePage
//	@Content-Type	application/json
//	@Produce		json
//	@Param			Authorization	header		string										true	"Bearer api_test"	default	(Bearer api_test)
//	@Param			Content-Type	header		string										true	"application/json"	default	("application/json")
//	@Success		200				{object}	controller.GetHomePageTabSuccessInitExample	"Request Success!"
//	@Failure		201				{object}	controller.GetTestdataFailResExample		"wait for default!"
//	@Router			/homePage/Tab [post]
func GetHomePageTab(w http.ResponseWriter, _ *http.Request) {

	// db query
	tagInfoSlice := HomePageTabInfoGet()

	// 產生response
	responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqSuccessCode, Data: tagInfoSlice, Msg: ""})

	// 回傳response
	_, err := w.Write(responseJSON)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

// GetHomePageAnnounceTab 上方公告標籤
// GetApp godoc2
//	@Summary		拿到首頁公告中 灰底的標籤資料
//	@Description	拿到首頁公告中 灰底的標籤資料
//	@Description 	== Response DATA ==
//	@Description	1.response.data:jsons in list
//	@Description	== json:announceTagInfo ==
//	@Description	1.displayName:string, tag info text.
//	@Description	2.enable:int, only for backend.
//	@Description	3.hyperLink:string, hyper link string.
//	@Description	4.permissionLevel:int, check for different client roles.
//	@Description	5.tips:string, only for backend.
//	@Description	== json:index ==
//	@Description	1.int, for sort data.
//	@Tags			HomePage
//	@Content-Type	application/json
//	@Produce		json
//	@Param			Authorization	header		string									true	"Bearer api_test"	default	(Bearer api_test)
//	@Param			Content-Type	header		string									true	"application/json"	default	("application/json")
//	@Success		200				{object}	controller.GetHomePageAnnounceTabSuccessInitExample				"Request Success!"
//	@Failure		201				{object}	controller.GetTestdataFailResExample	"wait for default!"
//	@Router			/homePage/GetHomePageAnnounceTab [post]
func GetHomePageAnnounceTab(w http.ResponseWriter, _ *http.Request) {

	// db query
	AnnounceTagInfo := HomePageAnnounceTagGet()

	// 產生response
	responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqSuccessCode, Data: AnnounceTagInfo, Msg: ""})

	// 回傳response

	_, err := w.Write(responseJSON)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

// GetHomePageVideoRank 首頁影片排行
// GetApp godoc2
//	@Summary		拿到首頁"電影"排行 TOP 10
//	@Description	拿到首頁"電影"排行 TOP 10
//	@Tags			HomePage
//	@Content-Type	application/json
//	@Description 	== Response.data ==
//	@Description	1.response.data:jsons in list
//	@Description	== json:VideoBasicInfo ==
//	@Description	1.VideoBasicInfo.length:int , video time length.
//	@Description	2.VideoBasicInfo.stars:float, video star.
//	@Description	3.VideoBasicInfo.thumb:str, thumb http.
//	@Description	4.VideoBasicInfo.title:str, video title.
//	@Description	5.VideoBasicInfo.views:int, views count.
//	@Description	== json:rank ==
//	@Description	1.int, rank number from 1 to 10.
//	@Produce		json
//	@Param			Authorization	header		string									true	"測試用：Bearer api_test"	default(Bearer api_test)
//	@Param			Content-Type	header		string									true	"application/json"		default(application/json)
//	@Success		200				{object}	controller.GetHomePageVideoRankSuccessResponse				"Request Success!"
//	@Failure		201				{object}	controller.GetTestdataFailResExample	"Check error msg!"
//	@Router			/homePage/GetHomePageVideoRank [post]
func GetHomePageVideoRank(w http.ResponseWriter, _ *http.Request) {

	// db query
	VideoRank, err := VideoRank()

	// error handle
	if err != nil {

		responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqFailCode, Data: nil, Msg: fmt.Sprintf("%v", err)})

		// 回傳response
		_, err = w.Write(responseJSON)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	} else {

		jsonData, _ := json.Marshal(map[string]interface{}{
			"VideoRank": VideoRank,
		})

		// 產生response
		responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqSuccessCode, Data: jsonData, Msg: ""})

		// 回傳response
		_, err = w.Write(responseJSON)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
}

// GetHomePageSeriesRank 首頁影集排行
// GetApp godoc2
//	@Summary		拿到首頁"影集"排行 TOP 10
//	@Description	拿到首頁"影集"排行 TOP 10
//	@Tags			HomePage
//	@Description 	== Response.data ==
//	@Description	1.response.data:jsons in list
//	@Description	== json:SeriesBasicInfo ==
//	@Description	1.SeriesBasicInfo.length:int , series time length.
//	@Description	2.SeriesBasicInfo.stars:float, series star.
//	@Description	3.SeriesBasicInfo.thumb:str, thumb http.
//	@Description	4.SeriesBasicInfo.title:str, series title.
//	@Description	5.SeriesBasicInfo.views:int, views count.
//	@Description	== json:rank ==
//	@Description	1.int, rank number from 1 to 10.
//	@Content-Type	application/json
//	@Produce		json
//	@Param			Authorization	header		string									true	"測試用：Bearer api_test"	default(Bearer api_test)
//	@Param			Content-Type	header		string									true	"application/json"		default(application/json)
//	@Success		200				{object}	controller.GetHomePageSeriesRankSuccessResponse				"Request Success!"
//	@Failure		201				{object}	controller.GetTestdataFailResExample	"Check error msg!"
//	@Router			/homePage/GetHomePageSeriesRank [post]
func GetHomePageSeriesRank(w http.ResponseWriter, _ *http.Request) {

	// db query
	VideoRank, err := SeriesRank()

	// error handle
	if err != nil {

		responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqFailCode, Data: nil, Msg: fmt.Sprintf("%v", err)})

		// 回傳response
		_, err = w.Write(responseJSON)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	} else {

		jsonData, _ := json.Marshal(map[string]interface{}{
			"SeriesRank": VideoRank,
		})

		// 產生success response
		responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqSuccessCode, Data: jsonData, Msg: ""})

		// 回傳response
		_, err = w.Write(responseJSON)

		if err != nil {
			return
		}
	}
}

// GetClassTypeText 首頁公告標籤 下拉選單
// GetApp godoc2
//	@Summary		拿到首頁公告 標籤下拉選單籤內容
//	@Description	拿到首頁 標籤下拉選單籤內容
//	@Tags			HomePage
//	@Content-Type	application/json
//	@Description 	== Response.data ==
//	@Description 	1.response.data:json
//	@Description	== json:classTextList ==
//	@Description 	1.list, type string.
//	@Produce		json
//	@Param			Authorization	header		string									true	"測試用：Bearer api_test"	default(Bearer api_test)
//	@Param			Content-Type	header		string									true	"application/json"		default(application/json)
//	@Success		200				{object}	controller.NewsClassTypeSuccessResponse	"Request Success!"
//	@Failure		201				{object}	controller.GetTestdataFailResExample	"Check error msg!"
//	@Router			/homePage/GetClassTypeText [post]
func GetClassTypeText(w http.ResponseWriter, _ *http.Request) {

	// db query
	ClassTextList := GetNewsClassText()
	fmt.Print("GetClassTypeText here!")

	jsonData, _ := json.Marshal(map[string]interface{}{
		"ClassTextList": ClassTextList,
	})

	// 產生success response
	responseJSON, err := json.Marshal(ApiResInit{Ret: ApiReqSuccessCode, Data: jsonData, Msg: ""})

	// 回傳response
	_, err = w.Write(responseJSON)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

// GetAllTypeNewsCards 首頁 新聞卡片 全部
// GetApp godoc2
//	@Summary		首頁 新聞卡片 全部 Summary
//	@Description	拿到不分類的 首頁新聞卡片
//	@Description 	== Response.data ==
//	@Description	1.response.data:jsons in list
//	@Description	== json:cardInfo ==
//	@Description	1.classType:int, class type.
//	@Description	2.hyperLink:string, hyperlink string.
//	@Description	3.infoText:string, card info text.
//	@Description	4.subTitle:string, card subtitle.
//	@Description	5.thumb:string, card thumb.
//	@Description	6.title:string, card title.
//	@Description	== json:index ==
//	@Description	1.int, sort index, 1~n.
//	@Tags			HomePage
//	@Content-Type	application/json
//	@Produce		json
//	@Param			Authorization	header		string									true	"測試用：Bearer api_test"	default(Bearer api_test)
//	@Param			Content-Type	header		string									true	"application/json"		default(application/json)
//	@Success		200				{object}	controller.GetNewsCardSuccess	"Request Success!"
//	@Failure		201				{object}	controller.GetTestdataFailResExample	"Check error msg!"
//	@Router			/homePage/GetAllTypeNewsCards [post]
func GetAllTypeNewsCards(w http.ResponseWriter, _ *http.Request) {

	// db query
	CardsRank, err := GetAllTypeNewsCard()

	// error handle
	if err != nil {

		responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqFailCode, Data: nil, Msg: fmt.Sprintf("%v", err)})

		// 回傳response
		_, err = w.Write(responseJSON)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	} else {

		jsonData, _ := json.Marshal(map[string]interface{}{
			"CardsRank": CardsRank,
		})

		// 產生success response
		responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqSuccessCode, Data: jsonData, Msg: ""})

		// 回傳response
		_, err = w.Write(responseJSON)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
}

// GetTypeNewsCards 首頁 新聞卡片 指定類別
// GetApp godoc2
//	@Summary		首頁 新聞卡片 指定類別
//	@Description	首頁 新聞卡片 指定類別
//	@Description 	== Response.data ==
//	@Description	1.response.data:jsons in list
//	@Description	== json:cardInfo ==
//	@Description	1.classType:int, class type number.
//	@Description	2.hyperLink:string, hyperlink string.
//	@Description	3.infoText:string, card info text.
//	@Description	4.subTitle:string, card subtitle.
//	@Description	5.thumb:string, card thumb.
//	@Description	6.title:string, card title.
//	@Description	== json:index ==
//	@Description	1.int, sort index, 1~n.
//	@Tags			HomePage
//	@Content-Type	application/json
//	@Produce		json
//	@Param			Authorization	header		string									true	"測試用：Bearer api_test"	default(Bearer api_test)
//	@Param			Content-Type	header		string									true	"application/json"		default(application/json)
//	@Success		200				{object}	controller.GetNewsCardSuccess	"Request Success!"
//	@Failure		201				{object}	controller.GetTestdataFailResExample	"Check error msg!"
//	@Router			/homePage/GetTypeNewsCards [post]
func GetTypeNewsCards(w http.ResponseWriter, r *http.Request) {

	var typeId CardsReq

	err := json.NewDecoder(r.Body).Decode(&typeId)

	targetClassId := typeId.ClassId

	// db query
	CardsRank, err := GetTypeNewsCard(targetClassId)

	// error handle
	if err != nil {

		responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqFailCode, Data: nil, Msg: fmt.Sprintf("%v", err)})

		// 回傳response
		_, err = w.Write(responseJSON)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	} else {

		jsonData, _ := json.Marshal(map[string]interface{}{
			"CardsRank": CardsRank,
		})

		// 產生success response
		responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqSuccessCode, Data: jsonData, Msg: ""})

		// 回傳response
		_, err = w.Write(responseJSON)

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
}
