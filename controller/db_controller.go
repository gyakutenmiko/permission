package controller

import (
	"encoding/json"
	"log"
	"runtime"
	"sort"

	"strconv"
	"time"

	"github.com/jinzhu/gorm"
)

var DB *gorm.DB
var AdminDB *gorm.DB

func Init() error {
	var err error

	sysType := runtime.GOOS

	// mac os:darwin
	// server os:linux
	if sysType != "darwin" {

		// server sql port
		// 查詢用
		DB, err = gorm.Open("mysql", "cmsop:1q2w(O)P@tcp(43.198.7.55:3306)/cms?charset=utf8&parseTime=true")
		if err != nil {
			log.Println(err)
		}
		// 新建table用
		AdminDB, err = gorm.Open("mysql", "cmssa:Cms1q2w(O)P@tcp(43.198.7.55:3306)/cms?charset=utf8&parseTime=true")
	} else {

		// local sql port
		DB, err = gorm.Open("mysql", "root:aa552300@tcp(127.0.0.1:3306)/testdb?charset=utf8&parseTime=true")
	}

	// 確認所有必要table是否存在 存在的話 寫入一筆假資料 不存在的話 建立該table後寫入假資料
	TableCheck()

	if err != nil {
		return err
	}

	return nil
}

// db CRUD query

// GetPwdByAccount db 用帳號 撈取密碼\ID
func GetPwdByAccount(account string) (string, string, error) {
	var user Users

	err := DB.Unscoped().Table("users").Where("account = ?", account).First(&user).Error
	id := strconv.Itoa(user.ID)

	if err != nil {
		return "", "", err
	}
	return id, user.Pwd, nil
}

// GetAccountInfo 用帳號撈取 account登入用資料
func GetAccountInfo(account string) (Users, error) {
	var user Users

	err := DB.Table("users").Select("id, authToken, nickName, icon, coin, vipType, language").Where("account = ?", account).Scan(&user).Error

	if err != nil {
		return Users{}, err
	}
	return user, nil
}

// GetPwdByManagerAccount db 用帳號 撈取密碼\ID
func GetPwdByManagerAccount(account string) (string, string, error) {
	var manager Managers

	err := DB.Unscoped().Table("managers").Where("account = ?", account).First(&manager).Error
	id := strconv.Itoa(int(manager.ID))

	if err != nil {
		return "", "", err
	}
	return id, manager.Pwd, nil
}
func indexOf(array []string, el int) int {
	for i, value := range array {
		if value == strconv.Itoa(el) {
			return i
		}
	}
	return -1
}

// GetPermissionByManagerAccount db 用帳號 撈取權限
func GetPermissionByManagerAccount(account string) ([]string, error) {
	type Roles struct {
		Id int `gorm:"column:id"`
	}
	var rols []Roles
	var pers []Permissions

	var perid []string
	err := DB.Unscoped().Table("rolesManagersList A2").Select("A3.id").Joins("JOIN managers A1 ON A1.id = A2.managerId AND A1.account = ?", account).Joins("JOIN managerRoles A3 ON A3.id = A2.roleId").Scan(&rols).Error
	if err != nil {
		log.Println(err)
		return []string{}, err
	}
	for i := 0; i < len(rols); i++ {
		err = DB.Unscoped().Table("rolesPermissionsList A2").Select("A3.id").Joins("JOIN managerRoles A1 ON A1.id = A2.roleId AND A1.Id = ?", rols[i].Id).Joins("JOIN managerPermissions A3 ON A3.id = A2.permissionId").Scan(&pers).Error
		if err != nil {
			log.Println(err)
			return []string{}, err
		}
		for _, value := range pers {
			if indexOf(perid, value.Id) == -1 {
				perid = append(perid, strconv.Itoa(value.Id))
			}
		}
	}
	//排序
	sort.Slice(perid, func(n, m int) bool { return perid[n] < perid[m] })
	return perid, nil
}

// GetBanPermissionByManagerAccount db 用帳號 撈取禁用權限
func GetBanPermissionByManagerAccount(account string) ([]string, error) {
	var pers []Permissions

	var perid []string
	err := DB.Unscoped().Table("permissionsManagerBan A2").Select("A3.id").Joins("JOIN managers A1 ON A1.id = A2.managerId AND A1.account = ?", account).Joins("JOIN managerPermissions A3 ON A3.id = A2.permissionId").Scan(&pers).Error
	if err != nil {
		log.Println(err)
		return []string{}, err
	}
	for _, value := range pers {
		if indexOf(perid, value.Id) == -1 {
			perid = append(perid, strconv.Itoa(value.Id))
		}
	}
	//排序
	sort.Slice(perid, func(n, m int) bool { return perid[n] < perid[m] })
	return perid, nil
}

// GetManagerAccountInfo 用帳號撈取 account登入用資料
func GetManagerAccountInfo(account string) (Managers, error) {
	var manager Managers

	err := DB.Table("managers").Select("id, authToken, nickName, language").Where("account = ?", account).Scan(&manager).Error

	if err != nil {
		return Managers{}, err
	}
	return manager, nil
}

// UpdateAuthToken 更新 account.auth_token
func UpdateAuthToken(id string, token string) error {
	return DB.Table("users").Where("id = ?", id).Update("authToken", token).Error
}
func UpdateManagerAuthToken(id string, token string) error {
	return DB.Table("managers").Where("id = ?", id).Update("authToken", token).Error
}

// HomePageTabInfoGet 撈取cms.homepageBannerTags tag的資料
func HomePageTabInfoGet() []byte {
	var tagInfoSlice []HomepageBannerTags

	DB.Table("homePageBannerTags").Find(&tagInfoSlice)
	//DB.LogMode(true)
	jsonData, err := json.Marshal(tagInfoSlice)

	if err != nil {
		return nil
	}

	return jsonData

}

// HomePageAnnounceTagGet 撈取cms.HomePageAnnounceTagGet tag的資料
func HomePageAnnounceTagGet() []byte {
	var announceTagInfo []HomePageAnnounceTag

	DB.Table("HomepageAnnounceTag").Find(&announceTagInfo)
	//DB.LogMode(true)
	jsonData, err := json.Marshal(announceTagInfo)

	if err != nil {
		return nil
	}

	return jsonData

}

// VideoRank 撈取cms.VideoInfo
func VideoRank() (interface{}, error) {

	var TopRankVideo []TopRankVideo
	var RankResult VideoInfo
	var ErrorResult ApiFailResInit

	errMsg := DB.Table("videoInfo").Limit(10).Order("views DESC").Where("videoSeriesType= ?", 1).Find(&TopRankVideo).Error

	if errMsg != nil {

		return ErrorResult, errMsg
	}

	// 產生一個slice
	searchResult := make([]interface{}, len(TopRankVideo))

	for rank, column := range TopRankVideo {

		RankResult := RankResult

		RankResult.VideoInfo.Title = column.Title
		RankResult.VideoInfo.Stars = column.Stars
		RankResult.VideoInfo.Length = column.Length
		RankResult.VideoInfo.Views = column.Views
		RankResult.VideoInfo.Thumb = column.Thumb
		RankResult.Rank = rank + 1

		searchResult[rank] = RankResult

	}
	return searchResult, errMsg
}

// SeriesRank 撈取cms.VideoInfo
func SeriesRank() (interface{}, error) {

	var TopRankSeries []TopRankSeries
	var RankResult VideoInfo
	var ErrorResult ApiFailResInit

	errMsg := DB.Table("videoInfo").Limit(10).Order("validViews DESC").Where("videoSeriesType= ?", 2).Find(&TopRankSeries).Error

	if errMsg != nil {

		return ErrorResult, errMsg
	}
	// 產生一個slice
	searchResult := make([]interface{}, len(TopRankSeries))

	for rank, column := range TopRankSeries {

		RankResult.VideoInfo.Title = column.Title
		RankResult.VideoInfo.Stars = column.Stars
		RankResult.VideoInfo.Length = column.Length
		RankResult.VideoInfo.Views = column.Views
		RankResult.VideoInfo.Thumb = column.Thumb
		RankResult.Rank = rank + 1

		searchResult[rank] = RankResult

	}
	return searchResult, errMsg
}

// VideoViewsCountAdd 將影片、影集有效劉覽次數加一
func VideoViewsCountAdd(videoId int) error {

	return DB.Table("videoInfo").Where("id = ?", videoId).Update("validViews", gorm.Expr("validViews + ?", 1)).Error
}

// VideoPlayCountAdd 將影片、影集有效播放次數加一
func VideoPlayCountAdd(videoId int) error {

	return DB.Table("videoInfo").Where("id = ?", videoId).Update("validPlay", gorm.Expr("validPlay + ?", 1)).Error
}

// GetNewsClassText 公告頁面 下拉式選單 標籤內容
func GetNewsClassText() []string {
	var ClassTextList []ClassText

	if err := DB.Model(&ClassText{}).Select("className").Find(&ClassTextList).Error; err != nil {
		return nil
	}

	var classNames []string

	for _, classText := range ClassTextList {
		classNames = append(classNames, classText.ClassText)
	}

	return classNames
}

// GetAllTypeNewsCard 拿到全部 不分類別的news cards
func GetAllTypeNewsCard() (interface{}, error) {

	var NewsCardList []NewsCardColumn
	var NewsCardIndex NewsCardInfo

	now := time.Now()
	//sec := now.Unix()

	err := DB.Table("newsCard").
		Order("updateDatetime ASC").
		Where("enable= ? and enableDatetime <= ?", 1, now).Find(&NewsCardList).Error

	if err != nil {

		return NewsCardList, err
	}

	// 產生一個slice
	searchResult := make([]interface{}, 0)
	// 置頂資料
	topResults := make([]interface{}, 0)

	//fmt.Println(NewsCardList)

	for index, column := range NewsCardList {

		index += 1

		NewsCardIndex.CardsInfo.Title = column.Title
		NewsCardIndex.CardsInfo.SubTitle = column.SubTitle
		NewsCardIndex.CardsInfo.InfoText = column.InfoText
		NewsCardIndex.CardsInfo.HyperLink = column.HyperLink
		NewsCardIndex.CardsInfo.Thumb = column.Thumb
		NewsCardIndex.CardsInfo.ClassType = column.ClassType

		if column.OnTop == 1 {
			NewsCardIndex.Index = 1
			topResults = append(topResults, NewsCardIndex)
		} else {
			NewsCardIndex.Index = index
			searchResult = append(searchResult, NewsCardIndex)
		}

	}
	// 插入資料到 slice 前面
	searchResult = append(topResults, searchResult...)

	return searchResult, err

}

// GetTypeNewsCard 拿到某類別的news cards
func GetTypeNewsCard(typeId int) (interface{}, error) {

	var NewsCardList []NewsCardColumn
	var NewsCardIndex NewsCardInfo

	now := time.Now()
	//sec := now.Unix()

	err := DB.Table("newsCard").
		Order("updateDatetime ASC").
		Where("classType = ? and enable= ? and enableDatetime <= ?", typeId, 1, now).Find(&NewsCardList).Error

	if err != nil {
		return NewsCardList, err
	}

	// 產生一個slice
	searchResult := make([]interface{}, 0)
	// 置頂資料
	topResults := make([]interface{}, 0)

	//fmt.Println(NewsCardList)

	for index, column := range NewsCardList {
		index += 1

		NewsCardIndex.CardsInfo.Title = column.Title
		NewsCardIndex.CardsInfo.SubTitle = column.SubTitle
		NewsCardIndex.CardsInfo.InfoText = column.InfoText
		NewsCardIndex.CardsInfo.HyperLink = column.HyperLink
		NewsCardIndex.CardsInfo.Thumb = column.Thumb
		NewsCardIndex.CardsInfo.ClassType = column.ClassType

		if column.OnTop == 1 {
			NewsCardIndex.Index = 1
			topResults = append(topResults, NewsCardIndex)
		} else {
			NewsCardIndex.Index = index
			searchResult = append(searchResult, NewsCardIndex)
		}

	}
	// 插入資料到 slice 前面
	searchResult = append(topResults, searchResult...)

	return searchResult, err

}
