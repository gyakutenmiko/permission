package controller

import (
	"bytes"
	"encoding/json"
	"net/http"
	"time"
)

// ===============================
// swagger ui struct   == start ==
// ===============================

// todo 把struct濃縮在一起

// PerfectLoginRequestExample
// PerfectLogin swagger ui 前端 request init
type PerfectLoginRequestExample struct {
	Account string `json:"account" example:"dev_account"`
	Pwd     string `json:"pwd" example:"dev_account"`
}

// PerfectLoginSuccess
// PerfectLogin swagger ui 顯示 api response success init
type PerfectLoginSuccess struct {
	Ret  int                `json:"ret" example:"200"`
	Data PerfectLoginExData `json:"data"`
	Msg  string             `json:"msg" example:""`
}

// PerfectLoginExData
// PerfectLogin swagger ui 顯示 response success init
type PerfectLoginExData struct {
	Account  string `json:"account" example:"dev_account"`
	Coin     string `json:"coin" example:"1000"`
	Id       string `json:"id" example:"1"`
	Icon     string `json:"icon" example:"icon https"`
	Language string `json:"lang" example:"886"`
	Token    string `json:"token" example:"token string"`
	VipType  string `json:"vipType" example:"1"`
	Nickname string `json:"nickName" example:"dev_accountXD"`
}

// PerfectLoginFail
// PerfectLogin swagger ui 顯示 response fail init
type PerfectLoginFail struct {
	Ret  int    `json:"ret" example:"201"`
	Data string `json:"data" example:""`
	Msg  string `json:"msg" example:"Some Error msg!"`
}

// GetTestdataSuccessResExample
// GetTestdata swagger ui 顯示 response success init
type GetTestdataSuccessResExample struct {
	Ret  int               `json:"ret" example:"200"`
	Data GetTestDataExData `json:"data"`
	Msg  string            `json:"msg" example:""`
}

// GetTestDataExData
// GetTestdata swagger ui 顯示 response success init
type GetTestDataExData struct {
	Data string `json:"data" example:"20230206"`
	Id   string `json:"id" example:"10"`
}

// GetTestdataFailResExample
// GetTestdata swagger ui 顯示 response fail init
type GetTestdataFailResExample struct {
	Ret  int    `json:"ret" example:"201"`
	Data string `json:"data" example:""`
	Msg  string `json:"msg" example:"Some Error msg!"`
}

// GetNewsCardSuccess swagger ui 顯示 api response success init
type GetNewsCardSuccess struct {
	Ret  int               `json:"ret" example:"200"`
	Data []GetNewsCardData `json:"data"`
	Msg  string            `json:"msg" example:""`
}

// GetNewsCardData swagger ui
type GetNewsCardData struct {
	Index     int                 `json:"index" example:"1"`
	CardsInfo NewsCardExampleInfo `json:"cardInfo" `
}

// NewsCardExampleInfo swagger ui
type NewsCardExampleInfo struct {
	Title     string `json:"title" example:"News Title Example"`
	SubTitle  string `json:"subTitle" example:"Subtitle Example"`
	InfoText  string `json:"infoText" example:"Info Text Example"`
	HyperLink string `json:"hyperLink" example:"Hyper links"`
	Thumb     string `json:"thumb" example:"thumb http!"`
	ClassType int    `json:"classType" example:"1"`
	OnTop     int    `json:"-" example:"1"`
}

// ============ Home Page Swag UI struct ============

// GetHomePageTabSuccessInitExample GetHomePageTabSuccessInit response Example
type GetHomePageTabSuccessInitExample struct {
	Ret  int                            `json:"ret" example:"200"`
	Data []GetHomePageTabSuccessExample `json:"data"`
	Msg  string                         `json:"msg" example:""`
}

// GetHomePageTabSuccessExample swagger ui GetHomePageTabSuccessExample
type GetHomePageTabSuccessExample struct {
	Index   int                   `json:"index" example:"1"`
	TagInfo GetHomePageTabTagInfo `json:"tagInfo"`
}

// GetHomePageTabTagInfo  swagger ui GetHomePageTabSuccessExample.Data
type GetHomePageTabTagInfo struct {
	Tips            string `json:"tips" example:"Tips msg."`
	DisplayName     string `json:"displayName" example:"Display string."`
	HyperLink       string `json:"hyperLink" example:"hyperlink after click."`
	Enable          int    `json:"enable" example:"2"`
	PermissionLevel int    `json:"permissionLevel" example:"3"`
}

// GetHomePageAnnounceTabSuccessInitExample response Example
type GetHomePageAnnounceTabSuccessInitExample struct {
	Ret  int               `json:"ret" example:"200"`
	Data []AnnounceTabInfo `json:"data"`
	Msg  string            `json:"msg" example:""`
}

// AnnounceTabInfo swagger ui GetHomePageTabSuccessExample
type AnnounceTabInfo struct {
	Index   int                        `json:"index" example:"1"`
	TagInfo GetHomePageAnnounceTagInfo `json:"announceTagInfo"`
}

// GetHomePageAnnounceTagInfo  swagger ui GetHomePageTabSuccessExample.Data
type GetHomePageAnnounceTagInfo struct {
	Tips            string `json:"tips" example:"Tips msg."`
	DisplayName     string `json:"displayName" example:"Display string."`
	HyperLink       string `json:"hyperLink" example:"hyperlink after click."`
	Enable          int    `json:"enable" example:"2"`
	PermissionLevel int    `json:"permissionLevel" example:"3"`
}

// GetHomePageVideoRankSuccessResponse 首頁影片排行 請求成功 回傳格式範例 swagger ui用
type GetHomePageVideoRankSuccessResponse struct {
	Ret  int                   `json:"ret" description:"請求結果" example:"200"`
	Data HomePageVideoRankInfo `json:"data"`
	Msg  string                `json:"msg" example:""`
}

// HomePageVideoRankInfo 默認 response.data 內容
type HomePageVideoRankInfo struct {
	VideoRankBasicData []VideoRankBasicData `json:"videoRank"`
}

// VideoRankBasicData 每包電影資料內容 星等+其他資料
type VideoRankBasicData struct {
	Rank           int            `json:"rank" example:"1" `
	VideoBasicInfo VideoBasicInfo `json:"VideoBasicInfo"`
}

// VideoBasicInfo 其他資料內容
type VideoBasicInfo struct {
	Title  string  `json:"title" example:"movie_1" `
	Starts float32 `json:"stars" example:"3.2" `
	Length int     `json:"length" example:"231" `
	Views  int     `json:"views" example:"3000" `
	Thumb  string  `json:"thumb" example:"https://thumb.com.tw" `
}

// GetHomePageSeriesRankSuccessResponse 首頁影集排行 請求成功 回傳格式範例 swagger ui用
type GetHomePageSeriesRankSuccessResponse struct {
	Ret  int                    `json:"ret" example:"200"`
	Data HomePageSeriesRankInfo `json:"data"`
	Msg  string                 `json:"msg" example:""`
}

// HomePageSeriesRankInfo 默認 response.data 內容
type HomePageSeriesRankInfo struct {
	SeriesRankBasicData []SeriesRankBasicData `json:"seriesRank"`
}

// SeriesRankBasicData 每包影集資料內容 星等+其他資料
type SeriesRankBasicData struct {
	Rank            int             `json:"rank" example:"1" `
	SeriesBasicInfo SeriesBasicInfo `json:"SeriesBasicInfo"`
}

// SeriesBasicInfo 其他資料內容
type SeriesBasicInfo struct {
	Title  string  `json:"title" example:"movie_1" `
	Starts float32 `json:"stars" example:"3.2" `
	Length int     `json:"length" example:"231" `
	Views  int     `json:"views" example:"3000" `
	Thumb  string  `json:"thumb" example:"https://thumb.com.tw" `
}

// NewsClassTypeSuccessResponse 首頁公告 右上方下拉選單內容成功範例
type NewsClassTypeSuccessResponse struct {
	Ret  int           `json:"ret" example:"200"`
	Data NewsClassType `json:"data"`
	Msg  string        `json:"msg" example:""`
}

// NewsClassType 右上方下拉選單內容成功範例
type NewsClassType struct {
	ClassTextList []string `json:"classTextList" example:"公告,船內,系統"`
}

// ===============================
// Middleware struct   == start ==
// ===============================

// loggingResponseWriter init
type loggingResponseWriter struct {
	http.ResponseWriter
	statusCode int
	buf        *bytes.Buffer
}

// ========================
// Api struct   == start ==
// ========================

type ApiResInit struct {
	Ret  int             `json:"ret"`
	Data json.RawMessage `json:"data"`
	Msg  string          `json:"msg"`
}

type ApiFailResInit struct {
	Ret  int             `json:"ret"`
	Data json.RawMessage `json:"data"`
	Msg  string          `json:"msg"`
}

type JwtToken struct {
	Token string `json:"token"`
}

type AccountPwd struct {
	Account string `json:"account"`
	Pwd     string `json:"pwd"`
}

type Request struct {
	JSONData json.RawMessage `json:"jsonData"`
	FormData map[string]string
	Token    string `json:"token"`
}

type Account struct {
	Account string `json:"account"`
	Pwd     string `json:"pwd"`
}

type LoginReq struct {
	Account string `json:"account"`
	Pwd     string `json:"pwd"`
}

type HomePageTag struct {
	Id              int    `json:"id"`
	Tips            string `json:"tips"`
	DisplayName     string `json:"displayName"`
	Index           int    `json:"index"`
	HyperLink       string `json:"hyperLink"`
	Enable          int    `json:"enable"`
	PermissionLevel int    `json:"permissionLevel"`
}

type VideoViewsTest struct {
	Id      int `json:"id"`
	VideoId int `json:"videoId"`
}

type VideoPlayTest struct {
	Id      int `json:"id"`
	VideoId int `json:"videoId"`
}

// NewsClassTypeText 右上方下拉選單標籤內容
type NewsClassTypeText struct {
	ClassTypes []string `json:"classTypes"`
}

type CardsReq struct {
	ClassId int `json:"typeId"`
}

// ===============================
// Redis data struct   == start ==
// ===============================

type UserInsertRedis struct {
	Id    string `json:"id"`
	Token string `json:"token"`
}

// RedisKey 用於Z add
type RedisKey struct {
	Score  int
	Member interface{}
}

// VideoViews for video view count 用於Z add
type VideoViews struct {
	Id      int `json:"id"`
	VideoId int `json:"videoId"`
}

// =============================
// DB.table struct   == start ==
// =============================

// Users Users db.users 欄位
type Users struct {
	ID        int    `gorm:"primaryKey:autoIncrement"`
	Account   string `gorm:"column:account"`
	Pwd       string `gorm:"column:pwd"`
	AuthToken string `gorm:"column:authToken"`
	NickName  string `gorm:"column:nickName"`
	Icon      string `gorm:"column:icon"`
	Coin      string `gorm:"column:coin"`
	VipType   string `gorm:"column:vipType"`
	Language  string `gorm:"column:language"`
}

// HomepageBannerTags cms.homepageBannerTags 欄位
type HomepageBannerTags struct {
	ID              *uint      `gorm:"primaryKey:autoIncrement"`
	Tips            string     `gorm:"column:tips;size:100" json:"tips"`
	DisplayName     string     `gorm:"column:displayName;size:20" json:"displayName"`
	Index           int        `gorm:"column:index;type:int" json:"index"`
	HyperLink       string     `gorm:"column:hyperLink;size:100" json:"hyperLink"`
	Enable          int        `gorm:"column:enable;type:int" json:"enable"`
	PermissionLevel int        `gorm:"column:permissionLevel;type:int" json:"permissionLevel"`
	UpdateDatetime  *time.Time `gorm:"column:updateDatetime; type:timestamp" json:"updateDatetime"`
}

// TableName change 設定HomePageAnnounceTag對應資料表名稱為"HomePageAnnounceTag"
func (emp HomepageBannerTags) TableName() string {
	return "homePageBannerTags"
}

// HomePageAnnounceTag cms.HomePageAnnounceTag 欄位
type HomePageAnnounceTag struct {
	ID              *uint      `gorm:"primaryKey:autoIncrement"`
	Tips            string     `gorm:"column:tips;size:100" json:"tips"`
	DisplayName     string     `gorm:"column:displayName;size:20" json:"displayName"`
	Index           int        `gorm:"column:index;type:int" json:"index"`
	HyperLink       string     `gorm:"column:hyperLink;size:100" json:"hyperLink"`
	Enable          int        `gorm:"column:enable;type:int" json:"enable"`
	PermissionLevel int        `gorm:"column:permissionLevel;type:int" json:"permissionLevel"`
	UpdateDatetime  *time.Time `gorm:"column:updateDatetime; type:timestamp" json:"updateDatetime"`
}

// TableName change 設定HomePageAnnounceTag對應資料表名稱為"HomePageAnnounceTag"
func (emp HomePageAnnounceTag) TableName() string {
	return "HomePageAnnounceTag"
}

// TopRankVideo 首頁影片排行榜上需要的column
type TopRankVideo struct {
	Title  string `gorm:"column:title"`
	Stars  string `gorm:"column:stars"`
	Length int    `gorm:"column:length"`
	Views  int    `gorm:"column:views"`
	Thumb  string `gorm:"column:thumb"`
}

// TopRankSeries 首頁影集排行榜上需要的column
type TopRankSeries struct {
	Title  string `gorm:"column:title"`
	Stars  string `gorm:"column:stars"`
	Length int    `gorm:"column:length"`
	Views  int    `gorm:"column:views"`
	Thumb  string `gorm:"column:thumb"`
}

// VideoInfo cms.VideoInfo 排序
type VideoInfo struct {
	Rank      int              `json:"rank"`
	VideoInfo TopRankVideoList `json:"videoInfo"`
}

// TopRankVideoList for response
type TopRankVideoList struct {
	Title  string `json:"title"`
	Stars  string `json:"starts"`
	Length int    `json:"length"`
	Views  int    `json:"views"`
	Thumb  string `json:"thumb"`
}

// TableName change 設定Employee對應資料表名稱為"videoInfo"
func (emp VideoInfo) TableName() string {
	return "videoInfo"
}

// ClassText 標籤內容文字欄位
type ClassText struct {
	ID        *uint  `gorm:"primaryKey:autoIncrement"`
	ClassText string `gorm:"column:className; size:6" json:"classText"`
}

// TableName change 設定ClassText對應資料表名稱為"classId"
func (emp ClassText) TableName() string {
	return "classId"
}

// NewsCardInfo cms.NewsCard 排序
type NewsCardInfo struct {
	Index     int      `json:"index"`
	CardsInfo NewsCard `json:"cardsInfo"`
}

// NewsCard cms.newsCard json
type NewsCard struct {
	Title           string    `json:"title"`
	SubTitle        string    `json:"subTitle"`
	InfoText        string    `json:"infoText"`
	HyperLink       string    `json:"hyperLink"`
	Thumb           string    `json:"thumb"`
	ClassType       int       `json:"classType"`
	OnTop           int       `json:"-"`
	Enable          int       `json:"-"`
	UpdateDatetime  time.Time `json:"-"`
	EnableDatetime  time.Time `json:"-"`
	DisableDatetime time.Time `json:"-"`
	CensorStatus    int       `json:"-"`
	CensorId        int       `json:"-"`
	CensorTips      string    `json:"-"`
}

// TableName change 設定NewsCard對應資料表名稱為"newsCard"
func (emp NewsCard) TableName() string {
	return "newsCard"
}

// NewsCardColumn cms.newsCard  欄位
type NewsCardColumn struct {
	ID              *uint      `gorm:"primaryKey:autoIncrement"`
	Title           string     `gorm:"column:title;size:60" json:"title"`
	SubTitle        string     `gorm:"column:subTitle;size:60" json:"subTitle"`
	InfoText        string     `gorm:"column:infoText;type:longtext" json:"infoText"`
	HyperLink       string     `gorm:"column:hyperLink;size:45" json:"hyperLink"`
	Thumb           string     `gorm:"column:thumb;size:80" json:"thumb"`
	ClassType       int        `gorm:"column:classType;type:int" json:"classType"`
	OnTop           int        `gorm:"column:onTop;type:int" json:"onTop"`
	Enable          int        `gorm:"column:enable;type:int" json:"enable"`
	UpdateDatetime  *time.Time `gorm:"column:updateDatetime;type:timestamp" json:"updateDatetime"`
	EnableDatetime  *time.Time `gorm:"column:enableDatetime;type:timestamp" json:"enableDatetime"`
	DisableDatetime *time.Time `gorm:"column:disableDatetime;type:timestamp" json:"disableDatetime"`
	CensorStatus    int        `gorm:"column:censorStatus;type:int" json:"censorStatus"`
	CensorId        int        `gorm:"column:censorId;type:int" json:"censorId"`
	CensorTips      string     `gorm:"column:censorTips;size:45" json:"censorTips"`
}

// TableName change 設定NewsCard對應資料表名稱為"newsCard"
func (emp NewsCardColumn) TableName() string {
	return "newsCard"
}

type Logs struct {
	ID                     *uint       `gorm:"primaryKey:autoIncrement"`
	RequestIp              string      `gorm:"column:requestIp;size:45"`
	RequestBody            interface{} `gorm:"column:requestBody;type:longtext" json:"requestBody,omitempty"`
	Route                  string      `gorm:"column:route;size:45"`
	ResponseBody           string      `gorm:"column:responseBody;type:text"`
	ResponseStatus         interface{} `gorm:"column:responseStatus;type:int"`
	RequestDeliverDatetime time.Time   `gorm:"column:requestDeliverDatetime;type:timestamp"`
}

func (emp Logs) TableName() string {
	return "logs"
}

// SeriesEpisodeInfo 影集 table
type SeriesEpisodeInfo struct {
	ID      *uint `gorm:"primaryKey:autoIncrement"`
	MainId  int   `gorm:"column:mainId;type:int"`
	Season  int   `gorm:"column:season;type:int"`
	Episode int   `gorm:"column:episode;type:int"`
	Views   int   `gorm:"column:views;type:int"`
	Enable  int   `gorm:"column:title;type:int"`
}

func (emp SeriesEpisodeInfo) TableName() string {
	return "seriesEpisodeInfo"
}

type VideoInfoColumn struct {
	ID                  *uint  `gorm:"primaryKey:autoIncrement"`
	Title               string `gorm:"column:title;size:45"`
	Stars               int    `gorm:"column:stars;type:int"`
	Year                int    `gorm:"column:year;type:int"`
	Length              int    `gorm:"column:length;type:int"`
	ValidViews          int    `gorm:"column:validViews;type:int"`
	ValidPlay           int    `gorm:"column:validPlay;type:int"`
	Info                string `gorm:"column:info;size:300"`
	AgeLimit            int    `gorm:"column:ageLimit;type:int"`
	Country             string `gorm:"column:country;size:45"`
	Tag                 string `gorm:"column:tag;size:50"`
	Resolution          int    `gorm:"column:resolution;type:int"`
	Thumb               string `gorm:"column:thumb;size:45"`
	Enable              int    `gorm:"column:enable;type:int"`
	Subtitle            string `gorm:"column:subtitle;size:45"`
	NewestEpisode       int    `gorm:"column:newestEpisode;type:int"`
	NewestSeason        int    `gorm:"column:newestSeason;type:int"`
	VideoSeriesType     int    `gorm:"column:videoSeries;type:int"`
	SeriesEpisodeInfoId int    `gorm:"column:seriesEpisodeInfoId;type:int"`
}

func (emp VideoInfoColumn) TableName() string {
	return "videoInfo"
}

// ManagersColumn anry cms.managers
type ManagersColumn struct {
	ID             *uint     `gorm:"column:id"`
	Account        string    `gorm:"column:account"`
	NickName       string    `gorm:"column:nickname"`
	Email          string    `gorm:"column:email"`
	Pwd            string    `gorm:"column:pwd"`
	Phone          string    `gorm:"column:phone"`
	CreateDatetime time.Time `gorm:"column:createDatetime;type:datetime"`
	AuthStatus     int       `gorm:"column:authStatus;type:tinyint"`
	LoginCountry   string    `gorm:"column:loginCountry;type:tinyint"`
	LoginDatetime  time.Time `gorm:"column:loginDatetime;type:datetime"`
	Country        int       `gorm:"column:country;type:smallint"`
	Language       int       `gorm:"column:language;type:smallint"`
	AuthToken      string    `gorm:"column:authToken"`
}

func (emp ManagersColumn) TableName() string {
	return "managers"
}

// RolesManagersList anry cms.rolesManagersList
type RolesManagersList struct {
	ID        *uint `gorm:"column:id"`
	RoleId    int   `gorm:"column:roleId;type:int"`
	ManagerId int   `gorm:"column:managerId;type:int"`
	Enable    int   `gorm:"column:enable;type:tinyint"`
}

func (emp RolesManagersList) TableName() string {
	return "rolesManagersList"
}

// ManagerRoles anry cms.managerRoles
type ManagerRoles struct {
	ID       *uint  `gorm:"column:id"`
	RoleName string `gorm:"column:roleName"`
	Note     string `gorm:"column:note"`
	Enable   int    `gorm:"column:enable;type:tinyint"`
}

func (emp ManagerRoles) TableName() string {
	return "managerRoles"
}

// RolesPermissionsList anry cms.rolesPermissionsList
type RolesPermissionsList struct {
	ID           *uint `gorm:"column:id"`
	RoleId       int   `gorm:"column:roleId; type:int"`
	PermissionId int   `gorm:"column:permissionId;type:int"`
	Enable       int   `gorm:"column:enable;type:tinyint"`
}

func (emp RolesPermissionsList) TableName() string {
	return "rolesPermissionsList"
}

// ManagerPermissions anry cms.managerPermissions
type ManagerPermissions struct {
	ID           *uint  `gorm:"column:id"`
	Permission   string `gorm:"column:permission"`
	Level        int    `gorm:"column:level;type:int"`
	Sort         int    `gorm:"column:sort;type:int"`
	ParentId     int    `gorm:"column:parentId;type:int"`
	LinkFunction string `gorm:"column:linkFunction"`
	Enable       int    `gorm:"column:enable;type:tinyint"`
}

func (emp ManagerPermissions) TableName() string {
	return "managerPermissions"
}

// PermissionManagerBan anry cms.permissionManagerBan
type PermissionManagerBan struct {
	ID           *uint `gorm:"column:id"`
	PermissionId int   `gorm:"column:permissionId;type:id"`
	ManagerId    int   `gorm:"column:managerId;type:int"`
	Enable       int   `gorm:"column:enable;type:tinyint"`
}

func (emp PermissionManagerBan) TableName() string {
	return "permissionManagerBan"
}

// PayRecordsGolden anry cms.payRecords_golden
type PayRecordsGolden struct {
	OrderNo       string    `gorm:"column:orderNo"`
	Uid           *uint     `gorm:"column:uid"`
	OrderAction   int       `gorm:"column:orderAction"`
	Currency      int       `gorm:"column:currency"`
	Amount        int       `gorm:"column:amount"`
	CreateTime    time.Time `gorm:"column:createTime;type:TIMESTAMP"`
	CalloutTime   time.Time `gorm:"column:calloutTime;type:TIMESTAMP"`
	ReturnTime    time.Time `gorm:"column:returnTime;type:TIMESTAMP"`
	PayNotifyTime time.Time `gorm:"column:payNotifyTime;type:TIMESTAMP"`
	CallbackTime  time.Time `gorm:"column:callbackTime;type:TIMESTAMP"`
	SettleTime    time.Time `gorm:"column:settleTime;type:TIMESTAMP"`
	ReceiveTime   time.Time `gorm:"column:receiveTime;type:TIMESTAMP"`
	FinishTime    time.Time `gorm:"column:finishTime;type:TIMESTAMP"`
	OrderStage    int       `gorm:"column:orderStage;type:tinyint"`
	OrderStatus   int       `gorm:"column:orderStatus;type:tinyint"`
	CreateData    string    `gorm:"column:createData"`
	CalloutData   string    `gorm:"column:calloutData"`
	ReturnData    string    `gorm:"column:returnData"`
	CallbackData  string    `gorm:"column:callbackData"`
}

func (emp PayRecordsGolden) TableName() string {
	return "payRecords_golden"
}

// PayPlatforms anry cms.payPlatforms
type PayPlatforms struct {
	ID          *uint     `gorm:"column:id"`
	Name        string    `gorm:"column:name"`
	Code        string    `gorm:"column:code"`
	UrlCall     string    `gorm:"column:urlCall"`
	UrlQuery    string    `gorm:"column:urlQuery"`
	UrlCallback string    `gorm:"column:urlCallback"`
	Note        string    `gorm:"column:note"`
	CreateTime  time.Time `gorm:"column:createTime;type:timestamp"`
	UpdateTime  time.Time `gorm:"column:updateTime;type:timestamp"`
	Enable      int       `gorm:"column:enable;type:tinyint"`
}

func (emp PayPlatforms) TableName() string {
	return "payPlatforms"
}

// PayChannels anry cms.payChannels
type PayChannels struct {
	ID          *uint     `gorm:"column:id"`
	ChannelName string    `gorm:"column:channelName"`
	PlatFromId  int       `gorm:"column:platFromId"`
	PayType     int       `gorm:"column:payType"`
	CountryCode int       `gorm:"column:countryCode"`
	Currency    int       `gorm:"column:currency"`
	Amounts     string    `gorm:"column:amounts"`
	Note        string    `gorm:"column:note"`
	CreateTime  time.Time `gorm:"column:createTime;type:timestamp"`
	UpdateTime  time.Time `gorm:"column:updateTime;type:timestamp"`
	Enable      int       `gorm:"column:enable;type:tinyint"`
}

func (emp PayChannels) TableName() string {
	return "payChannels"
}
