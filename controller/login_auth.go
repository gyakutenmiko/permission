package controller

import (
	"encoding/json"
	"fmt"
	jwt "github.com/dgrijalva/jwt-go"
	"net/http"
	"strconv"
	"time"
)

// GetTestdata 測試驗證資料
// GetApp godoc
//	@Summary		用header帶入登入時拿到的token 並拿到一筆測試資料
//	@Description	"用token 拿到資料"
//	@Tags			GetTestdata
//	@Param			Authorization	header	string	true	"Insert your 登入 token"	default(<Add access token here>)
//	@Param			Content-Type	header	string	true	"default Content-Type"	default(application/json)
//	@Produce		json
//	@Success		200	{object}	controller.GetTestdataSuccessResExample	"Request finish!"
//	@Failure		201	{object}	controller.GetTestdataFailResExample	"Other error! Please Check Response.msg!"
//	@Router			/get_test_data [post]
func GetTestdata(w http.ResponseWriter, r *http.Request) {

	// req.Method 判斷
	if r.Method == http.MethodGet {
		// 產生response
		responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqFailCode, Data: nil, Msg: "request type error!"})

		// 寫入回應內容
		_, err := w.Write(responseJSON)
		if err != nil {
			return
		}

	} else {

		data := map[string]string{
			"date": "20230116",
			"id":   "10"}

		dataJSON, _ := json.Marshal(data)

		// 產生response
		responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqSuccessCode, Data: dataJSON, Msg: ""})

		// 設置回應狀態碼和回應內容類型
		w.WriteHeader(http.StatusOK)

		// 寫入回應內容
		_, err := w.Write(responseJSON)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
}

// PerfectLogin
// GetApp godoc
//	@Summary		登入驗證密碼並獲取token
//	@Description	"登入成功會給予token"
//	@Tags			PerfectLogin
//	@Content-Type	application/json
//	@Produce		json
//	@Param			request	body		controller.PerfectLoginRequestExample	true	"JSON檔案內容"
//	@Success		200		{object}	controller.PerfectLoginSuccess			"Request finish!"
//	@Failure		201		{object}	controller.PerfectLoginFail				"Other error! Please Check Response.msg!"
//	@Router			/PerfectLogin [post]
func PerfectLogin(w http.ResponseWriter, r *http.Request) {

	var loginReq LoginReq

	err := json.NewDecoder(r.Body).Decode(&loginReq)

	if r.Method != http.MethodPost || err != nil {
		// 回傳response
		responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqFailCode, Data: nil, Msg: "request type error!"})

		// 寫入回應內容
		_, err := w.Write(responseJSON)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

	} else {

		account := loginReq.Account
		pwd := loginReq.Pwd

		// 空白密碼帳號判斷
		if account == "" || pwd == "" {

			// 回傳response
			responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqFailCode, Data: nil, Msg: "Invalid account or password!"})

			// 寫入回應內容
			_, err := w.Write(responseJSON)
			if err != nil {
				http.Error(w, err.Error(), http.StatusBadRequest)
				return
			}
		}

		// 撈出驗證密碼
		id, accountPwd, _ := GetPwdByAccount(account)

		// hash後結果
		pwdHash := passwordhash(account, pwd)

		// req密碼與db內比對 若是正確 生成新 token + 有效時間
		if pwdHash == accountPwd {

			// create JWT token 有效期：一天
			token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
				"id":  id,
				"exp": time.Now().Add(time.Second * 1800).Unix(),
			})

			// 用JwtKey yaml 內 JwtKey 簽名
			tokenString, _ := token.SignedString([]byte("CMSeyJhbGciOi"))

			//撈出資料庫中帳號資料
			user, _ := GetAccountInfo(account)

			//寫入redis
			err = WriteTokenInRedis(id, tokenString)
			fmt.Print("Insert to redis! \n")

			if err != nil {

				// 回傳response
				responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqFailCode, Data: nil, Msg: fmt.Sprintf("Redis insert Fail! %s \n", err)})

				// 寫入回應內容
				_, err := w.Write(responseJSON)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
			}

			// token 寫入 response json
			data := map[string]string{
				"id":        strconv.Itoa(int(user.ID)),
				"nick_name": user.NickName,
				"icon":      user.Icon,
				"coin":      user.Coin,
				"vip_type":  user.VipType,
				"language":  user.Language,
				"account":   account,
				"token":     tokenString}

			dataJSON, _ := json.Marshal(data)

			// 產生response

			// 產生response
			responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqSuccessCode, Data: dataJSON, Msg: ""})

			// 更新進入db
			id := strconv.Itoa(int(user.ID))
			err = UpdateAuthToken(id, tokenString)

			// 回傳response
			_, err = w.Write(responseJSON)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}

		} else {

			// 回傳response
			responseJSON, _ := json.Marshal(ApiResInit{Ret: ApiReqFailCode, Data: nil, Msg: "Invalid account or password!"})

			// 寫入回應內容
			_, err := w.Write(responseJSON)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		}
	}

}
