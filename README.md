## Api Auth Pass

>新增測試用token , 請用下方string 帶入headers 可略過token驗證
>- Bearer api_test


## Commit info 
 
> - fix：修補 bug（bug fix)  
    >     feat：新增或修改功能（feature）

> - style：格式   
    >      不影響程式碼運行的變動，例如：white-space, formatting, missing semi colons

> - refactor：重構
    >     不是新增功能，也非修補 bug 的程式碼變動

> - perf：改善效能（improves performance）

> - test：增加測試（when adding missing tests）

> - chore：maintain
    >     不影響程式碼運行，建構程序或輔助工具的變動  例如修改 config、Grunt Task 任務管理工具

>- docs：文件（documentation）
