package router

import (
	"cms/controller"
	"net/http"

	httpSwagger "github.com/swaggo/http-swagger"

	"github.com/gorilla/mux"
)

func Init(r *mux.Router) {

	r.HandleFunc("/RegisterCheck", controller.RegisterCheck).Methods(http.MethodGet)
	r.HandleFunc("/ManagerRegisterCheck", controller.ManagerRegisterCheck).Methods(http.MethodGet)
	r.HandleFunc("/CallBackGolden", controller.CallBackGolden).Methods(http.MethodPost)
	r.HandleFunc("/CallBackEasy", controller.CallBackEasy).Methods(http.MethodPost)
	r.HandleFunc("/AgencyPayOutOrder", controller.AgencyPayOutOrder).Methods(http.MethodPost)

	// Login route 驗證帳號密碼 正確的話生成新token
	loginRouter := r.Methods(http.MethodPost).Subrouter()
	loginRouter.HandleFunc("/PerfectLogin", controller.PerfectLogin).Methods(http.MethodPost)
	loginRouter.HandleFunc("/ManagerPerfectLogin", controller.ManagerPerfectLogin).Methods(http.MethodPost)
	loginRouter.Use(controller.JSONContentTypeMiddleware)

	// Auth Post Route
	AuthRouter := r.Methods(http.MethodPost).Subrouter()

	// 驗證token 正確的話返回資料 若token快要過期 產生新的token
	r.PathPrefix("/").Handler(httpSwagger.WrapHandler)
	AuthRouter.Use(controller.JwtAuth)
	AuthRouter.Use(controller.JSONContentTypeMiddleware)

	// Jake homePage api
	homepage := AuthRouter.PathPrefix("/homePage").Subrouter()
	// 上方藍底區域標籤
	homepage.HandleFunc("/Tab", controller.GetHomePageTab).Methods(http.MethodPost)
	// 首頁內灰底標籤
	homepage.HandleFunc("/AnnounceTab", controller.GetHomePageAnnounceTab).Methods(http.MethodPost)
	// 首頁影片排行
	homepage.HandleFunc("/GetHomePageVideoRank", controller.GetHomePageVideoRank).Methods(http.MethodPost)
	// 首頁影集排行
	homepage.HandleFunc("/GetHomePageSeriesRank", controller.GetHomePageSeriesRank).Methods(http.MethodPost)
	// 公告 右上方 下拉選單內容
	homepage.HandleFunc("/GetClassTypeText", controller.GetClassTypeText).Methods(http.MethodPost)
	// 公告卡片 不分類
	homepage.HandleFunc("/GetAllTypeNewsCards", controller.GetAllTypeNewsCards).Methods(http.MethodPost)
	// 公告卡片 分類
	homepage.HandleFunc("/GetTypeNewsCards", controller.GetTypeNewsCards).Methods(http.MethodPost)

	// test middleware api
	AuthRouter.HandleFunc("/get_test_data", controller.GetTestdata).Methods(http.MethodPost)

	// test view count + play count api
	//todo:將這些功能整合進入middleware 用於請求影片資料時的api
	TestRouter := AuthRouter.Methods(http.MethodPost).Subrouter()
	TestRouter.HandleFunc("/VideoViewCounts", controller.VideoViewCounts).Methods(http.MethodPost)
	TestRouter.HandleFunc("/VideoPlayCounts", controller.VideoPlayCounts).Methods(http.MethodPost)

	// anry sql api
	//User api
	AuthRouter.HandleFunc("/UserQuery", controller.UserQuery).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/UserNew", controller.UserNew).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/UserModify", controller.UserModify).Methods(http.MethodPost)

	AuthRouter.HandleFunc("/EmailCheck", controller.EmailCheck).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/AuthcodePasswordCheck", controller.AuthcodePasswordCheck).Methods(http.MethodPost)

	AuthRouter.HandleFunc("/RegisterEmailCheck", controller.RegisterEmailCheck).Methods(http.MethodPost)

	//Manager api
	AuthRouter.HandleFunc("/QueryManager", controller.QueryManager).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/NewManager", controller.NewManager).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/ModifyManager", controller.ModifyManager).Methods(http.MethodPost)

	AuthRouter.HandleFunc("/ManagerEmailCheck", controller.ManagerEmailCheck).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/ManagerAuthcodePasswordCheck", controller.ManagerAuthcodePasswordCheck).Methods(http.MethodPost)

	AuthRouter.HandleFunc("/ManagerRegisterEmailCheck", controller.ManagerRegisterEmailCheck).Methods(http.MethodPost)

	//Role api
	AuthRouter.HandleFunc("/QueryRole", controller.QueryRole).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/NewRole", controller.NewRole).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/ModifyRole", controller.ModifyRole).Methods(http.MethodPost)

	//Role_manager api
	AuthRouter.HandleFunc("/QueryManagerRole", controller.QueryManagerRole).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/NewManagerRole", controller.NewManagerRole).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/OnOffManagerRole", controller.OnOffManagerRole).Methods(http.MethodPost)

	//Permission api
	AuthRouter.HandleFunc("/QueryPermission", controller.QueryPermission).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/NewPermission", controller.NewPermission).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/ModifyPermission", controller.ModifyPermission).Methods(http.MethodPost)

	//Role_permission api
	AuthRouter.HandleFunc("/QueryRolePermission", controller.QueryRolePermission).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/NewRolePermission", controller.NewRolePermission).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/OnOffRolePermission", controller.OnOffRolePermission).Methods(http.MethodPost)

	//Permission_ban api
	AuthRouter.HandleFunc("/QueryBanPermission", controller.QueryBanPermission).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/NewBanPermission", controller.NewBanPermission).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/OnOffBanPermission", controller.OnOffBanPermission).Methods(http.MethodPost)

	//payment api
	AuthRouter.HandleFunc("/QueryPayInChannels", controller.QueryPayInChannels).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/CallPayIn", controller.CallPayIn).Methods(http.MethodPost)

	AuthRouter.HandleFunc("/QueryPayChannel", controller.QueryPayChannel).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/NewPayChannel", controller.NewPayChannel).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/ModifyPayChannel", controller.ModifyPayChannel).Methods(http.MethodPost)

	AuthRouter.HandleFunc("/QueryPayPlatform", controller.QueryPayPlatform).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/NewPayPlatform", controller.NewPayPlatform).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/ModifyPayPlatform", controller.ModifyPayPlatform).Methods(http.MethodPost)

	AuthRouter.HandleFunc("/QueryPayOutAccount", controller.QueryPayOutAccount).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/NewPayOutAccount", controller.NewPayOutAccount).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/ModifyPayOutAccount", controller.ModifyPayOutAccount).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/DeletePayOutAccount", controller.DeletePayOutAccount).Methods(http.MethodPost)

	AuthRouter.HandleFunc("/QueryPayOutChannel", controller.QueryPayOutChannel).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/CallPayOut", controller.CallPayOut).Methods(http.MethodPost)

	AuthRouter.HandleFunc("/QueryPayWithdrawRules", controller.QueryPayWithdrawRules).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/NewPayWithdrawRules", controller.NewPayWithdrawRules).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/ModifyPayWithdrawRules", controller.ModifyPayWithdrawRules).Methods(http.MethodPost)

	AuthRouter.HandleFunc("/QueryOrder", controller.QueryOrder).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/UserOrder", controller.UserOrder).Methods(http.MethodPost)

	AuthRouter.HandleFunc("/QueryProduct", controller.QueryProduct).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/NewProduct", controller.NewProduct).Methods(http.MethodPost)
	AuthRouter.HandleFunc("/ModifyProduct", controller.ModifyProduct).Methods(http.MethodPost)
}
